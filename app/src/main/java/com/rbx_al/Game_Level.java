package com.rbx_al;

import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class Game_Level extends Activity {

	public MediaPlayer click,backsound;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_game_level);
		backsound=MediaPlayer.create(getBaseContext(), R.raw.backsound_game_home);
		click=MediaPlayer.create(getBaseContext(), R.raw.klik);
		backsound.start();font();hiden();hiden3();hiden4();hiden5();hiden6();
		hiden7();hiden8();hiden9();
		animasi();
		backsound.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				backsound.start();
			}
		});
	}
	public void klik(View v){
		if(v==findViewById(R.id.btn_game_level1_play)){
			startActivity(new Intent(Game_Level.this, Game_Play.class));
		}else if(v==findViewById(R.id.btn_game_level2_play)){
			startActivity(new Intent(Game_Level.this, Game_Play_lev2.class));
		}else if(v==findViewById(R.id.btn_game_level3_play)){
			startActivity(new Intent(Game_Level.this, Game_Play_lev3.class));
		}else if(v==findViewById(R.id.btn_game_level4_play)){
			startActivity(new Intent(Game_Level.this, Game_Play_lev4.class));
		}else if(v==findViewById(R.id.btn_game_level5_play)){
			startActivity(new Intent(Game_Level.this, Game_Play_lev5.class));
		}else if(v==findViewById(R.id.btn_game_level6_play)){
			startActivity(new Intent(Game_Level.this, Puzzle1.class));
		}else if(v==findViewById(R.id.btn_game_level7_play)){
			startActivity(new Intent(Game_Level.this, Puzzle2.class));
		}else if(v==findViewById(R.id.btn_game_level8_play)){
			startActivity(new Intent(Game_Level.this, Puzzle3.class));
		}else if(v==findViewById(R.id.btn_game_level9_play)){
			startActivity(new Intent(Game_Level.this, Puzzle4.class));
		}
		backsound.pause();click.start();
	}
	public void font(){
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 face1=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		TextView judul=(TextView)findViewById(R.id.judul_level),
				 judullevel1=(TextView)findViewById(R.id.judul_game_level1),
				 judullevel2=(TextView)findViewById(R.id.judul_game_level2),
				 judullevel3=(TextView)findViewById(R.id.judul_game_level3),
				 judullevel4=(TextView)findViewById(R.id.judul_game_level4),
				 judullevel5=(TextView)findViewById(R.id.judul_game_level5),
				 judullevel6=(TextView)findViewById(R.id.judul_game_level6),
				 judullevel7=(TextView)findViewById(R.id.judul_game_level7),
				 judullevel8=(TextView)findViewById(R.id.judul_game_level8),
				 judullevel9=(TextView)findViewById(R.id.judul_game_level9);
		Button kembali=(Button)findViewById(R.id.btn_game_level_kembali);
		judul.setTypeface(face1);judullevel1.setTypeface(face);judullevel2.setTypeface(face);
		judullevel3.setTypeface(face);judullevel4.setTypeface(face);judullevel5.setTypeface(face);
		judullevel6.setTypeface(face);judullevel7.setTypeface(face);judullevel8.setTypeface(face);
		judullevel9.setTypeface(face);
		kembali.setTypeface(face);
	}
	public void hiden(){
		ImageButton img2=(ImageButton)findViewById(R.id.btn_game_level2_play),img3=(ImageButton)findViewById(R.id.btn_game_level3_play),
				    img4=(ImageButton)findViewById(R.id.btn_game_level4_play),img5=(ImageButton)findViewById(R.id.btn_game_level5_play);
		img3.setVisibility(View.GONE);img4.setVisibility(View.GONE);img5.setVisibility(View.GONE);
		int standard1=70;
		View bg2=findViewById(R.id.level2_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", "");
		if(nilai_level1.isEmpty()||Integer.parseInt(nilai_level1)<standard1){
			img2.setVisibility(View.GONE);
			bg2.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img2.setVisibility(View.VISIBLE);
			bg2.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	
	public void hiden3(){
		ImageButton img3=(ImageButton)findViewById(R.id.btn_game_level3_play);
		img3.setVisibility(View.GONE);
		int standard2=150;
		View bg3=findViewById(R.id.level3_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)<standard2){
			img3.setVisibility(View.GONE);
			bg3.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img3.setVisibility(View.VISIBLE);
			bg3.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void hiden4(){
		ImageButton img4=(ImageButton)findViewById(R.id.btn_game_level4_play);
		img4.setVisibility(View.GONE);
		int standard2=230;
		View bg4=findViewById(R.id.level4_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)<standard2){
			img4.setVisibility(View.GONE);
			bg4.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img4.setVisibility(View.VISIBLE);
			bg4.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	
	public void hiden5(){
		ImageButton img5=(ImageButton)findViewById(R.id.btn_game_level5_play);
		img5.setVisibility(View.GONE);
		int standard2=320;
		View bg5=findViewById(R.id.level5_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", ""),
			   nilai_level4=sp.getString("level4", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||nilai_level4.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)+Integer.parseInt(nilai_level4)<standard2){
			img5.setVisibility(View.GONE);
			bg5.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img5.setVisibility(View.VISIBLE);
			bg5.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void hiden6(){
		ImageButton img6=(ImageButton)findViewById(R.id.btn_game_level6_play);
		img6.setVisibility(View.GONE);
		int standard2=400;
		View bg6=findViewById(R.id.level6_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", ""),
			   nilai_level4=sp.getString("level4", ""),
			   nilai_level5=sp.getString("level5", "");;
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||nilai_level4.isEmpty()||nilai_level5.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)+Integer.parseInt(nilai_level4)+Integer.parseInt(nilai_level5)<standard2){
			img6.setVisibility(View.GONE);
			bg6.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img6.setVisibility(View.VISIBLE);
			bg6.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void hiden7(){
		ImageButton img7=(ImageButton)findViewById(R.id.btn_game_level7_play);
		img7.setVisibility(View.GONE);
		int standard2=470;
		View bg7=findViewById(R.id.level7_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", ""),
			   nilai_level4=sp.getString("level4", ""),
			   nilai_level5=sp.getString("level5", ""),
			   nilai_level6=sp.getString("nilai_puzzle1", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||nilai_level4.isEmpty()||nilai_level5.isEmpty()||nilai_level6.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)+Integer.parseInt(nilai_level4)+
				Integer.parseInt(nilai_level5)+Integer.parseInt(nilai_level6)<standard2){
			img7.setVisibility(View.GONE);
			bg7.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img7.setVisibility(View.VISIBLE);
			bg7.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void hiden8(){
		ImageButton img8=(ImageButton)findViewById(R.id.btn_game_level8_play);
		img8.setVisibility(View.GONE);
		int standard2=450;
		View bg8=findViewById(R.id.level8_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", ""),
			   nilai_level4=sp.getString("level4", ""),
			   nilai_level5=sp.getString("level5", ""),
			   nilai_level6=sp.getString("nilai_puzzle1", ""),
			   nilai_level7=sp.getString("nilai_puzzle2", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||nilai_level4.isEmpty()||nilai_level5.isEmpty()||nilai_level6.isEmpty()||nilai_level7.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)+Integer.parseInt(nilai_level4)+
				Integer.parseInt(nilai_level5)+Integer.parseInt(nilai_level6)+Integer.parseInt(nilai_level7)<standard2){
			img8.setVisibility(View.GONE);
			bg8.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img8.setVisibility(View.VISIBLE);
			bg8.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void hiden9(){
		ImageButton img9=(ImageButton)findViewById(R.id.btn_game_level9_play);
		img9.setVisibility(View.GONE);
		int standard2=530;
		View bg9=findViewById(R.id.level9_lay);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilai_level1=sp.getString("level1", ""),
			   nilai_level2=sp.getString("level2", ""),
			   nilai_level3=sp.getString("level3", ""),
			   nilai_level4=sp.getString("level4", ""),
			   nilai_level5=sp.getString("level5", ""),
			   nilai_level6=sp.getString("nilai_puzzle1", ""),
			   nilai_level7=sp.getString("nilai_puzzle2", ""),
			   nilai_level8=sp.getString("nilai_puzzle3", "");
		if(nilai_level1.isEmpty()||nilai_level2.isEmpty()||nilai_level3.isEmpty()||nilai_level4.isEmpty()||nilai_level5.isEmpty()||nilai_level6.isEmpty()||nilai_level7.isEmpty()||nilai_level8.isEmpty()||Integer.parseInt(nilai_level1)+Integer.parseInt(nilai_level2)+Integer.parseInt(nilai_level3)+Integer.parseInt(nilai_level4)+
				Integer.parseInt(nilai_level5)+Integer.parseInt(nilai_level6)+Integer.parseInt(nilai_level7)+Integer.parseInt(nilai_level8)<standard2){
			img9.setVisibility(View.GONE);
			bg9.setBackgroundResource(R.drawable.game_level_bg2);
		}else{
			img9.setVisibility(View.VISIBLE);
			bg9.setBackgroundResource(R.drawable.game_level_bg);
		}
	}
	public void animasi(){
		ImageButton img2=(ImageButton)findViewById(R.id.btn_game_level2_play),img3=(ImageButton)findViewById(R.id.btn_game_level3_play),
					img4=(ImageButton)findViewById(R.id.btn_game_level4_play),img5=(ImageButton)findViewById(R.id.btn_game_level5_play),
					img1=(ImageButton)findViewById(R.id.btn_game_level1_play),img6=(ImageButton)findViewById(R.id.btn_game_level6_play);
		Animation anim=AnimationUtils.loadAnimation(Game_Level.this, R.anim.animasi_game_level_btn_play);
		img1.setAnimation(anim);img2.setAnimation(anim);img3.setAnimation(anim);img4.setAnimation(anim);img5.setAnimation(anim);img6.setAnimation(anim);
	}
	public void onBackPressed(){
		startActivity(new Intent(Game_Level.this, Game_Home.class));
		backsound.pause();
	}
	public void keluar(View v){
		startActivity(new Intent(Game_Level.this, Game_Home.class));
		backsound.pause();click.start();
	}
}
