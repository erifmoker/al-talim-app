package com.rbx_al;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class Puzzle1_nilai extends Activity {

	TextView nilai;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_puzzle1_nilai);
		nilai=(TextView)findViewById(R.id.puzlle_nilai_nilai);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		String nilainya = null;
		font();
		if(sp.getString("puzzle", "").equals("1")){
			nilainya=sp.getString("nilai_puzzle1","");
		}else if(sp.getString("puzzle", "").equals("2")){
			nilainya=sp.getString("nilai_puzzle2","");
		}else if(sp.getString("puzzle", "").equals("3")){
			nilainya=sp.getString("nilai_puzzle3","");
		}else if(sp.getString("puzzle", "").equals("4")){
			nilainya=sp.getString("nilai_puzzle4","");
		}
		nilai.setText(nilainya);
	}

	public void onBackPressed(){
		
	}
	
	public void ulangi(View v){
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		if(sp.getString("puzzle", "").equals("1")){
			startActivity(new Intent(Puzzle1_nilai.this, Puzzle1.class));
		}else if(sp.getString("puzzle", "").equals("2")){
			startActivity(new Intent(Puzzle1_nilai.this, Puzzle2.class));
		}else if(sp.getString("puzzle", "").equals("3")){
			startActivity(new Intent(Puzzle1_nilai.this, Puzzle3.class));
		}else if(sp.getString("puzzle", "").equals("4")){
			startActivity(new Intent(Puzzle1_nilai.this, Puzzle4.class));
		}
	}
	public void daftar(View v){
		startActivity(new Intent(Puzzle1_nilai.this, Game_Level.class));
	}
	public void font(){
		TextView label=(TextView)findViewById(R.id.puzlle_nilai_text_nilai),
				 nilai=(TextView)findViewById(R.id.puzlle_nilai_nilai);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf");
		label.setTypeface(face);nilai.setTypeface(face);
		}
}

