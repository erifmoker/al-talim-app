package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.Window;

public class Intro extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_intro);
		MediaPlayer salam=MediaPlayer.create(this, R.raw.basmalah_anak);
		salam.start();buat_centang_waktu();
		intronya();
	}
	
	public void intronya(){
		new CountDownTimer(6000,1000) {
			public void onTick(long millisUntilFinished) {
			}
			public void onFinish() {
				startActivity(new Intent(Intro.this, Home.class));
				System.exit(0);
			}
		}.start();
	}
	public void buat_centang_waktu(){
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		Editor edit=sp.edit();
		edit.commit();
	}
}
