package com.rbx_al;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

public class Game_Play extends Activity {

	public Button jawab1, jawab2, jawab3, jawab4;
	public View wadah_soal;
	public TextView txWaktu, txPertanyaan, txkategori, Pancingan,tdk_terjwb;
	private Penghitung timer = new Penghitung(20000, 1000);
	private keputusan keputusannya = new keputusan(2500, 1000);
	int urut = 0, nilai_benar = 0, nilai_salah = 0, benar=0, tdk_trjwb=0;
	public Database_Game isi_game = new Database_Game();
	public MediaPlayer ganti_soal, btn_benar, btn_salah, backsound;

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_game_play);
		pertanyaan1();font();
		ganti_soal = MediaPlayer.create(getBaseContext(), R.raw.game_ganti_soal);
		btn_benar = MediaPlayer.create(getBaseContext(), R.raw.game_btn_benar);
		btn_salah = MediaPlayer.create(getBaseContext(), R.raw.game_btn_salah);
		timer.start();TextView txNilai = (TextView) findViewById(R.id.nilaiNilai);
		tdk_terjwb=(TextView)findViewById(R.id.game_tdk_terjawab);
		txNilai.setVisibility(View.GONE);tdk_terjwb.setVisibility(View.GONE);
		ganti_soal.start();
		backsound = MediaPlayer.create(this, R.raw.backsound_game_play);
		backsound.setVolume(50, 50);
		backsound.start();
		jawab1 = (Button) findViewById(R.id.jawab1);
		jawab2 = (Button) findViewById(R.id.jawab2);
		jawab3 = (Button) findViewById(R.id.jawab3);
		jawab4 = (Button) findViewById(R.id.jawab4);
		txWaktu = (TextView) findViewById(R.id.wadah_waktu);
		txkategori = (TextView) findViewById(R.id.wudlu_isi_tx_judul_bacaan);
		txPertanyaan = (TextView) findViewById(R.id.txSoal);
		wadah_soal = findViewById(R.id.wadah_Soal);
		Pancingan = (TextView) findViewById(R.id.pancingan);
		Pancingan.setAnimation(AnimationUtils.loadAnimation(getBaseContext(),
				R.anim.animasi_btn_benar));
		animasi_komponen();
		backsound.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				backsound.start();
			}
		});
	}

	public void jawaban(View v) {
		if (v == findViewById(R.id.jawab1)) {
			if (Arrays.asList(isi_game.jawaban_benar).contains(jawab1.getText().toString())) {
				jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.jawab2)){
			if (Arrays.asList(isi_game.jawaban_benar).contains(jawab2.getText().toString())) {
				jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.jawab3)){
			if (Arrays.asList(isi_game.jawaban_benar).contains(jawab3.getText().toString())) {
				jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.jawab4)){
			if (Arrays.asList(isi_game.jawaban_benar).contains(jawab4.getText().toString())) {
				jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}
		NonAktifkan_Tombol();timer.cancel();
		keputusannya.start();backsound.pause();
	}

	public void animasi_komponen() {
		jawab1 = (Button) findViewById(R.id.jawab1);
		jawab2 = (Button) findViewById(R.id.jawab2);
		jawab3 = (Button) findViewById(R.id.jawab3);
		jawab4 = (Button) findViewById(R.id.jawab4);
		txWaktu = (TextView) findViewById(R.id.wadah_waktu);
		wadah_soal = findViewById(R.id.wadah_Soal);
		wadah_soal.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_soal));
		txWaktu.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_soal));
		jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_btn_kiri));
		jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_btn_kiri));
		jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_btn_kanan));
		jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
				R.anim.animasi_btn_kanan));
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	public class Penghitung extends CountDownTimer {
		public Penghitung(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}
		public void onFinish() {
			timer.cancel();ganti_pertanyaan();timer.start();
			tdk_trjwb++;
		}
		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@SuppressLint("NewApi")
		@Override
		public void onTick(long millisUntilFinished) {
			txWaktu = (TextView) findViewById(R.id.wadah_waktu);
			long milis = millisUntilFinished;
			String hms = String.format(
					"%02d",
					TimeUnit.MILLISECONDS.toSeconds(milis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(milis)));
			txWaktu.setText(hms);
		}
	}

	public void pertanyaan1() {
		txkategori = (TextView) findViewById(R.id.wudlu_isi_tx_judul_bacaan);
		txPertanyaan = (TextView) findViewById(R.id.txSoal);
		jawab1 = (Button) findViewById(R.id.jawab1);
		jawab2 = (Button) findViewById(R.id.jawab2);
		jawab3 = (Button) findViewById(R.id.jawab3);
		jawab4 = (Button) findViewById(R.id.jawab4);
		txkategori.setText(isi_game.kategori[urut]);
		txPertanyaan.setText(isi_game.pertanyaan[urut]);
		jawab1.setText(isi_game.opsi[urut]);
		jawab2.setText(isi_game.opsi[1]);
		jawab3.setText(isi_game.opsi[2]);
		jawab4.setText(isi_game.opsi[3]);
	}

	public void ganti_pertanyaan() {
		if (txPertanyaan.getText().toString().equals(isi_game.pertanyaan[urut])) {
		}
		urut++;
		if (urut < isi_game.pertanyaan.length) {
			txkategori.setText(isi_game.kategori[urut]);
			txPertanyaan.setText(isi_game.pertanyaan[urut]);
			jawab1.setText(isi_game.opsi[(urut * 4)]);
			jawab2.setText(isi_game.opsi[(urut * 4) + 1]);
			jawab3.setText(isi_game.opsi[(urut * 4) + 2]);
			jawab4.setText(isi_game.opsi[(urut * 4) + 3]);
			wadah_soal.setAnimation(AnimationUtils.loadAnimation(
					Game_Play.this, R.anim.animasi_soal));
			txWaktu.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
					R.anim.animasi_soal));
			jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
					R.anim.animasi_btn_kiri));
			jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
					R.anim.animasi_btn_kiri));
			jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
					R.anim.animasi_btn_kanan));
			jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play.this,
					R.anim.animasi_btn_kanan));
			Aktifkan_Tombol();
			timer.start();
			ganti_soal.start();
			TextView txNilai = (TextView) findViewById(R.id.nilaiNilai),txNilaiBenar = (TextView) findViewById(R.id.game_play_tx_Benar),
					txSalah = (TextView) findViewById(R.id.game_play_tx_Salah);
			txNilai.setText(""+nilai_benar);txNilaiBenar.setText("Benar : "+benar);
			txSalah.setText("Salah : "+nilai_salah);
			backsound.setVolume(50, 50);backsound.start();
		} else {
			keputusannya.cancel();
			timer.cancel();
			startActivity(new Intent(Game_Play.this, Game_Nilai.class));
			overridePendingTransition(R.animator.animation1, R.animator.animation2);
			data_nilai();
			if (backsound.isPlaying()) {
				backsound.pause();
			}
		}
	}

	public class keputusan extends CountDownTimer {
		public keputusan(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}
		public void onFinish() {
			timer.cancel();
			ganti_pertanyaan();
		}
		public void onTick(long millisUntilFinished) {
		}
	}

	public void NonAktifkan_Tombol() {
		jawab1.setClickable(false);
		jawab2.setClickable(false);
		jawab3.setClickable(false);
		jawab4.setClickable(false);
	}

	public void Aktifkan_Tombol() {
		jawab1.setClickable(true);
		jawab2.setClickable(true);
		jawab3.setClickable(true);
		jawab4.setClickable(true);
	}
	public void onBackPressed(){
		AlertDialog.Builder ab = new AlertDialog.Builder(this);
		timer.cancel();
		ab.setCancelable(false).setTitle("Akhiri Permainan ?")
		.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				backsound.pause();
				timer.cancel();
				startActivity(new Intent(Game_Play.this, Game_Level.class));
			}
			}).setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					dialog.cancel();
					timer.start();}
				}).show();
	}
	public void font(){
		jawab1 = (Button) findViewById(R.id.jawab1);jawab2 = (Button) findViewById(R.id.jawab2);
		jawab3 = (Button) findViewById(R.id.jawab3);jawab4 = (Button) findViewById(R.id.jawab4);
		txWaktu = (TextView) findViewById(R.id.wadah_waktu);txkategori = (TextView) findViewById(R.id.wudlu_isi_tx_judul_bacaan);
		txPertanyaan = (TextView) findViewById(R.id.txSoal);
		TextView txNilaiBenar = (TextView) findViewById(R.id.game_play_tx_Benar),
				txSalah = (TextView) findViewById(R.id.game_play_tx_Salah);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		jawab1.setTypeface(face);jawab2.setTypeface(face);jawab3.setTypeface(face);jawab4.setTypeface(face);
		txWaktu.setTypeface(face);txPertanyaan.setTypeface(face);txWaktu.setTypeface(face);
		txkategori.setTypeface(face);txNilaiBenar.setTypeface(face);txSalah.setTypeface(face);
	}
	public void data_nilai(){
		TextView txNilai = (TextView) findViewById(R.id.nilaiNilai),
				 txSalah = (TextView) findViewById(R.id.game_play_tx_Salah),
			     txBenar = (TextView) findViewById(R.id.game_play_tx_Benar),
			     tdk_terjwb=(TextView)findViewById(R.id.game_tdk_terjawab);;
		txBenar.setText("Benar : "+benar);
		txSalah.setText("Salah : "+nilai_salah);
		tdk_terjwb.setText("Tidak Terjawab : "+tdk_trjwb);
		txNilai.setText(""+nilai_benar);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		Editor edit=sp.edit();
		edit.putString("level1", txNilai.getText().toString());
		edit.putString("salah_level1", txSalah.getText().toString());
		edit.putString("tdk_trjwb", tdk_terjwb.getText().toString());
		edit.putString("benar_level1", txBenar.getText().toString());
		edit.commit();
	}
}
