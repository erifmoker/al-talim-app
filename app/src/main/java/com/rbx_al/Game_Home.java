package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class Game_Home extends Activity {
	private MediaPlayer klik;
	public Button btnMulai,btnKeluar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_game_home);
		klik=MediaPlayer.create(this, R.raw.klik);
		font();
		btnMulai=(Button)findViewById(R.id.btn_game_Mulai);
		btnKeluar=(Button)findViewById(R.id.btn_game_home_keluar);
		btnMulai.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(Game_Home.this, Game_Level.class));
				klik.start();
			}
		});
		btnKeluar.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(Game_Home.this, Home.class));
				klik.start();
			}
		});
	}
	public void onBackPressed(){
		klik.start();
		Toast.makeText(getBaseContext(), "Tekan Tombol Keluar", 500).show();
	}
	public void font(){
		Typeface face=Typeface.createFromAsset(getAssets(), "font/kids.ttf");
		Button mulai=(Button)findViewById(R.id.btn_game_Mulai),keluar=(Button)findViewById(R.id.btn_game_home_keluar);
		mulai.setTypeface(face);keluar.setTypeface(face);
	}
}
