package com.rbx_al;

import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

public class Sholat extends Activity {
	private MediaPlayer mp,efek;
	private TextView judul,sholat1,sholat2,sholat3,sholat4,sholat5;
	private Button kembali;
	public Database_sholat database=new Database_sholat();
	private View list1,list2,list3,list4,list5;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_sholat);
		mp=MediaPlayer.create(this, R.raw.klik);
		efek=MediaPlayer.create(this, R.raw.effect_power_up);
		efek.start();
		font();animasi();
	}

	public void Sebelumnya(View v){
		mp.start();
		finish();
	}
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}
	public void onResume(){
		super.onResume();
		animasi();efek.start();
	}
	public void font(){
		judul=(TextView)findViewById(R.id.judul_sholat);
		kembali=(Button)findViewById(R.id.sholat_detail_btn_kembali);
		sholat1=(TextView)findViewById(R.id.tx_sholat1);
		sholat2=(TextView)findViewById(R.id.tx_sholat2);
		sholat3=(TextView)findViewById(R.id.tx_sholat3);
		sholat4=(TextView)findViewById(R.id.tx_sholat4);
		sholat5=(TextView)findViewById(R.id.tx_sholat5);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);kembali.setTypeface(face2);sholat1.setTypeface(face2);
		sholat2.setTypeface(face2);sholat3.setTypeface(face2);sholat4.setTypeface(face2);sholat5.setTypeface(face2);
	}
	
	public void Button(View v){
		Intent pindah=new Intent(Sholat.this, Sholat_detail.class);
		if(v==findViewById(R.id.sholat_img_btn1)){
			pindah.putExtra("judul_niat", database.judul_subuh[0]);
			pindah.putExtra("arab", database.bacaan_subuh[0]);
			pindah.putExtra("arti", database.arti_subuh[0]);
			pindah.putExtra("cara_baca", database.cara_baca_subuh[0]);
			pindah.putExtra("gambar_niat", database.gambar_subuh[0]);
			pindah.putExtra("audio_sholat", database.audio_subuh[0]);
		}else if(v==findViewById(R.id.sholat_img_btn2)){
			pindah.putExtra("judul_niat", database.judul_dhuhur[0]);
			pindah.putExtra("arab", database.bacaan_dhuhur[0]);
			pindah.putExtra("arti", database.arti_dhuhur[0]);
			pindah.putExtra("cara_baca", database.cara_baca_dhuhur[0]);
			pindah.putExtra("gambar_niat", database.gambar_dhuhur[0]);
			pindah.putExtra("audio_sholat", database.audio_dhuhur[0]);
		}else if(v==findViewById(R.id.sholat_img_btn3)){
			pindah.putExtra("judul_niat", database.judul_azhar[0]);
			pindah.putExtra("arab", database.bacaan_azhar[0]);
			pindah.putExtra("arti", database.arti_azhar[0]);
			pindah.putExtra("cara_baca", database.cara_baca_azhar[0]);
			pindah.putExtra("gambar_niat", database.gambar_azhar[0]);
			pindah.putExtra("audio_sholat", database.audio_azhar[0]);
		}else if(v==findViewById(R.id.sholat_img_btn4)){
			pindah.putExtra("judul_niat", database.judul_maghrib[0]);
			pindah.putExtra("arab", database.bacaan_maghrib[0]);
			pindah.putExtra("arti", database.arti_maghrib[0]);
			pindah.putExtra("cara_baca", database.cara_baca_maghrib[0]);
			pindah.putExtra("gambar_niat", database.gambar_maghrib[0]);
			pindah.putExtra("audio_sholat", database.audio_maghrib[0]);
		}else if(v==findViewById(R.id.sholat_img_btn5)){
			pindah.putExtra("judul_niat", database.judul_isya[0]);
			pindah.putExtra("arab", database.bacaan_isya[0]);
			pindah.putExtra("arti", database.arti_isya[0]);
			pindah.putExtra("cara_baca", database.cara_baca_isya[0]);
			pindah.putExtra("gambar_niat", database.gambar_isya[0]);
			pindah.putExtra("audio_sholat", database.audio_isya[0]);
		}
		mp.start();
		startActivity(pindah);
	}
	public void animasi(){
		list1=findViewById(R.id.lay_sholat_list1);list2=findViewById(R.id.lay_sholat_list2);
		list3=findViewById(R.id.lay_sholat_list3);list4=findViewById(R.id.lay_sholat_list4);
		list5=findViewById(R.id.lay_sholat_list5);
		list1.setAnimation(AnimationUtils.loadAnimation(Sholat.this, R.anim.sholat_list1));
		list2.setAnimation(AnimationUtils.loadAnimation(Sholat.this, R.anim.sholat_list2));
		list3.setAnimation(AnimationUtils.loadAnimation(Sholat.this, R.anim.sholat_list3));
		list4.setAnimation(AnimationUtils.loadAnimation(Sholat.this, R.anim.sholat_list4));
		list5.setAnimation(AnimationUtils.loadAnimation(Sholat.this, R.anim.sholat_list5));
	}
}
