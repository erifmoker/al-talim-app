package com.rbx_al;

public class Database_rukun {
	
	String[] judul_islam={
		"1. Syahadat",
		"2. Sholat",
		"3. Zakat",
		"4. Puasa",
		"5. Naik Haji"
	};
	String[] islam={
			"Mengucap Dua Kalimat Syahadat\n\n" +
			"شَهِدَ اللّهُ أَنَّهُ لاَ إِلَـهَ إِلاَّ هُوَ وَالْمَلاَئِكَةُ وَأُوْلُواْ الْعِلْمِ قَآئِمَاً بِالْقِسْطِ لاَ إِلَـهَ إِلاَّ هُوَ الْعَزِيزُ الْحَكِيمُ\n\n" +
			"Lafadznya: Asyhadu an-Laa Ilaaha Illallah wa Asyhadu an-na Muhammadarrasuulullah.\n\n" +
			"Artinya: Aku besaksi Tiada Tuhan yang berhak disembah selain Allah dan aku bersaksi bahwa Muhammad adalah Utusan Allah.\n\n" +
			"Syahadat (Bahasa Arab: الشهادة asy-syahādah ) merupakan asas dan dasar dari lima rukun Islam dan merupakan ruh, inti dan landasan seluruh ajaran Islam. Syahadat berasal dari kata bahasa Arab yaitu syahida (شهد), yang artinya ia telah menyaksikan. Kalimat itu dalam syariat Islam adalah sebuah pernyataan kepercayaan dalam keesaan Tuhan (Allah) dan Nabi Muhammad sebagai RasulNya. Kalimat inilah yang harus diikrarkan pertama kali ketika seseorang memeluk agama Islam.",
			"Sebagai ummat Islam kita diwajibkan mendirikan Sholat sehari semalam 5 waktu, mulai dari Sholat Subuh, Zuhur, Ashar, Magrib dan I’sya. Sholat memeliki kedudukan agung dalam Islam, hal ini bisa kita lihat dari keutamaan sholat tersebut seperti berikut:\n" +
			"a) Shalat adalah kewajiban paling utama setelah dua kalimat syahadat dan merupakan salah satu rukun islam.\n" +
			"b) Shalat merupakan pembeda antara muslim dan kafir.\n" +
			"c) Shalat adalah tiang agama dan agama seseorang tidak tegak kecuali dengan menegakkan shalat.\n" +
			"d) Amalan yang pertama kali akan dihisab pada hari kiamat.\n" +
			"Nabi shallallahu ‘alaihi wa sallam bersabda, “Sesungguhnya amal hamba yang pertama kali akan dihisab pada hari kiamat adalah shalatnya. Apabila shalatnya baik, dia akan mendapatkan keberuntungan dan keselamatan. Apabila shalatnya rusak, dia akan menyesal dan merugi. Jika ada yang kurang dari shalat wajibnya, Allah Tabaroka wa Ta’ala  mengatakan,’Lihatlah apakah pada hamba tersebut memiliki amalan shalat sunnah?’ Maka shalat sunnah tersebut akan menyempurnakan shalat wajibnya yang kurang. Begitu juga amalan lainnya seperti itu.”  Dalam riwayat lainnya, ”Kemudian zakat akan (diperhitungkan) seperti itu. Kemudian amalan lainnya akan dihisab seperti itu pula.” (HR Abu Daud)",
			"Dilihat dari segi bahasa, kata zakat berasal dari kata zaka (bentuk mashdar), yang mempunyai arti: berkah,tumbuh,bersih,suci dan baik. Zakat menurut istilah (syara’) artinya sesuatu yang hukumnya wajib diberikan dari sekumpulan harta benda tertentu, menurut sifat dan ukuran tertentu kepada golongan tertentu yang berhak menerimanya dengan syarat tertentu pula.\n" +
			"Allah telah memerintahkan setiap muslim yang memilki harta mencapai nisab untuk mengeluarkan zakat hartanya setiap tahun. Ia berikan kepada yang berhak menerima dari kalangan fakir serta selain mereka yang zakat boleh diserahkan kepada mereka sebagaimana telah diterangkan dalam Al Qur’an.\n\n" +
			"وَأَقِيمُواْ الصَّلاَةَ وَآتُواْ الزَّكَاةَ وَارْكَعُواْ مَعَ الرَّاكِعِينَ\n" +
			"Dan dirikanlah shalat, tunaikanlah zakat dan ruku’lah beserta orang-orang yang ruku‘.\n\n" +
			"خُذْ مِنْ أَمْوَالِهِمْ صَدَقَةً تُطَهِّرُهُمْ وَتُزَكِّيهِم بِهَا وَصَلِّ عَلَيْهِمْ\n" +
			"Allah SWT berfiman: Ambillah zakat dari sebagian harta mereka, dengan zakat itu kamu membersihkan dan mensucikan mereka",
			"Menurut syariat agama Islam, puasa ramadhan artinya menahan diri dari makan dan minum serta segala perbuatan yang bisa membatalkan puasa, mulai dari terbit fajar hinggalah terbenam matahari, dengan syarat tertentu, untuk meningkatkan ketakwaan seorang Muslim.\n\n" +
			"يَا أَيُّهَا الَّذِينَ آمَنُواْ كُتِبَ عَلَيْكُمُ الصِّيَامُ كَمَا كُتِبَ عَلَى الَّذِينَ مِن قَبْلِكُمْ لَعَلَّكُمْ تَتَّقُونَ\n" +
			"Hai orang-orang yang beriman, diwajibkan atas kamu berpuasa sebagaimana diwajibkan atas orang-orang sebelum kamu agar kamu bertakwa\n" +
			"Allah berfirman :\n\n" +
			"وَكُلُواْ وَاشْرَبُواْ حَتَّى يَتَبَيَّنَ لَكُمُ الْخَيْطُ الأَبْيَضُ مِنَ الْخَيْطِ الأَسْوَدِ مِنَ الْفَجْرِ ثُمَّ أَتِمُّواْ الصِّيَامَ إِلَى الَّليْلِ\n" +
			"Dan makan minumlah hingga terang bagimu benang putih dari benang hitam, yaitu fajar. ‎Kemudian sempurnakanlah puasa itu sampai (datang) malam. (QS al Baqarah: 187)‎",
			"Untuk rukun Islam yg kelima ini, Allah mewajibkan setiap hambanya untuk Haji ke Baitullah Mekkah sekali seumur hidup.\n" +
			"Pengertian Haji adalah berkunjung ke Baitullah Mekkah untuk melakukan tawaf, Sa’i, Wukuf di arafah dan melakukan amalan-amalan yang lain dalam waktu tertentu untuk mendapatkan keridhaan Allah SWT.\n" +
			"Adapun syarat-syarat Haji ada 5 perkara yaitu, Islam, Baligh, Berakal Sehat, Merdeka dan Mampu."
	};
	String[] judul_iman={
			"Iman Kepada Allah SWT",
			"Iman Kepada Malaikat",
			"Iman Kepada Kitab Allah",
			"Iman Kepada Rosul Allah",
			"Iman Kepada Hari Kiamat",
			"Iman Adanya Qadha dan Qadar"
	};
	String[] detail_iman={
			"Rukun Iman itu mempunyai 6 (enam) pilar dan yang pertama adalah imam / mempercayai Allah SWT tuhan sang pencipta alam itu ada serta menyakini bahwa tiada tuhan selain Allah karena orang yg beriman, percaya dan menyembah kpd Allah akan mendapatkan ketenangan dan ketentraman jiwa yg muncul dari hati secara ikhlas. Seperti Firman Alloh SWT yang berbunyi :\n" +
			"Adapun Orang2 yg beriman kepada Alloh dan berpedang teguh kepada agamanya (Islam) niscaya Allah akan memasukkan mereka ke dlm rahmat yg besar dari’nya (Surga) dan limpahan karunia’nya dan menunjuki mereka kpd jalan yg lurus untuk sampai kepada’nya (QS. An-Nisa : 175)",
			"Adapun Rukun Iman yg kedua adalah meyakini adanya Malaikat – Malaikate Gusti Alloh dan meyakini bahwa Alloh memiliki Malaikat2 yg diciptakan dari cahaya dan serta mereka (Para Malaikat) adlh hamba Alloh yg dimuliakan karena apapun yg Alloh perintahkan kpd mereka maka mereka akan langsung melaksanakannya.",
			"Yang mempunyai pengertian bahwa kita sebagai seorang Muslim harus meyakini bahwa Alloh memiliki kitab – kitab yg diturunkan kpd Nabi dan Rosul (Sebelum Al-qur’an) yg benar – benar merupakan kalam / Firman’nya dan ia (Kitabnya) adlah cahaya dan petunjuk serta apa yg terkandung didlmnya adlah suatu kebenaran. Adapun kitab – kitab Gusti Alloh yg disebutkan namanya oleh Alloh antara lain Kitab Taurat, Kitab Injil, Zabur dan Al – Qur’an, Kitab Al-Qur’an sendiri merupakan tolak ukur kebenaran dari kitab – kitab terdahulu.",
			"Rukun Iman yg ke empat adalah menyakini adanya para Rosul – Rosul atau Nabi utusan Alloh yang diberi wahyu oleh Alloh dan ditugaskan untuk menyampaikan pesan atau wahyu tersebut kpd hamba-hambanya yg intinya untuk memberikan jalan yg lurus yang dibenarkan oleh Alloh SWT. Sedangkan untuk Rosul atau Nabi merupakan sosok seseorang yg sdh ditinggikan derajatnya oleh Alloh dan Rosul atau Nabi yg diutus oleh Alloh berjumlah 25 orang.",
			"Rukun Iman yang kelima adlh menyakini bahwa hari akhir / kiamat itu ada dan bakal terjadi di dunia ini karena kita mengetahui sendiri bahwa kehidupan ini tidaklah kekal dan suatu saat pasti akan berakhir. Adapun kita sebagai umat muslim harus percaya bahwa urunan setelah dunia ini hancur beserta dg semua isinya akan di bangkitkan kembali semua umat manusia didlm kubur, lalu dikumpulkannya lagi di padang mahsyar, kemudian di hitungnya semua alam perbuatan manusia di dunia (Hisab), lalu ditimbangkan semua amal perbuatannya tersebut untuk mengetahui lebih banyak mana amal baik maupun amal buruknya, kemudian sampai kpd pembalasan di masukanya di dlm surga atau di neraka.",
			"Rukun Iman yang terakhir adalah mempercayi dg adanya Qadha dan Qadar, Qadha sendiri mempunyai pengertian kehendak atau ketetapan hukum Alloh terhadap segala sesuatu.. Sedangkan Qadar adalh ukuran atau ketentuan Alloh SWT terhadap segala sesuatu."
	};
}
