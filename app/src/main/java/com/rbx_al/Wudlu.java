package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class Wudlu extends Activity {
	private MediaPlayer mp;
	private TextView judul,air,taya;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_wudlu);
		font();
		mp=MediaPlayer.create(this, R.raw.klik);
	}

	public void wudlu1(View v){
		startActivity(new Intent(Wudlu.this, Wudlu1.class));
		mp.start();
	}
	
	public void Tayamum1(View v){
		startActivity(new Intent(Wudlu.this, Tayamum1.class));
		mp.start();
	}
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}
	public void font(){
		judul=(TextView)findViewById(R.id.judul_wudlu);
		air=(TextView)findViewById(R.id.wudu_air);
		taya=(TextView)findViewById(R.id.wd_taya);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);air.setTypeface(face2);taya.setTypeface(face2);
	}

}
