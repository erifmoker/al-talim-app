package com.rbx_al;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Coba_Next extends Activity {

	private Button next_brktnya;
	private TextView jdl; Database ambil=new Database();
	int pos=0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_coba__next);
		
		next_brktnya=(Button)findViewById(R.id.NEXT);
		jdl=(TextView)findViewById(R.id.JUDUL_DOA);
		jdl.setText(ambil.doa[0]);
		next_brktnya.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(jdl.getText().toString().equals(ambil.doa[pos])){
					pos++;
				}else if(jdl.getText().toString().equals(ambil.doa.length)){
					pos--;
				}
				if(pos<ambil.doa.length){
					jdl.setText(ambil.doa[pos]);
				}else if(pos==ambil.doa.length){
					jdl.setText("HOp");
				}
			}
		});
		
	}
}
