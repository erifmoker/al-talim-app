package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.Window;
import android.widget.TextView;

public class Rukun_Islam_detail extends Activity {

	private TextView isi_penjelasan,judul;
	private String ambil_penjelasan,ambil_judul;
	public Bundle extras;
	MediaPlayer mp,ding;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_rukun__islam_detail);
		isi_penjelasan=(TextView)findViewById(R.id.rukun_islam_detail_isi);
		judul=(TextView)findViewById(R.id.rukun_islam_detail_judul);
		ding=MediaPlayer.create(this, R.raw.effect_ding);
		mp=MediaPlayer.create(this, R.raw.klik);
		font();
		extras=getIntent().getExtras();
		if(extras !=null){
			ambil_penjelasan=extras.getString("penjelasan");
			ambil_judul=extras.getString("judul");
		}
		judul.setText(ambil_judul);
		isi_penjelasan.setText(ambil_penjelasan);
		
	}

	public void font(){
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf");
		isi_penjelasan.setTypeface(face);judul.setTypeface(face2);
	}
	public void onBackPressed(){
		super.onBackPressed();
		mp.start();
	}
}
