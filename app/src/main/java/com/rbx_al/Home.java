package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

@SuppressLint("NewApi")
public class Home extends Activity {

	public MediaPlayer mp, menuSound;
	public Button btn_Doa, btn_sholat, btn_wudlu, btn_Game, btn_Nabi,
			btn_rukun;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_home);
		mp = MediaPlayer.create(this, R.raw.klik);
		menuSound=MediaPlayer.create(getBaseContext(), R.raw.home_menu);
		animasi_button();
	}

	public void onBackPressed() {
		mp.start();
		AlertDialog.Builder ab = new AlertDialog.Builder(this);
		ab.setCancelable(false)
				.setTitle("Tutup Aplikasi ?")
				.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						finishAffinity();
					}
				})
				.setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.cancel();
							}
						}).show();
	}

	public void onResume() {
		super.onResume();
		animasi_button();
	}

	public void animasi_button() {
		btn_Doa = (Button) findViewById(R.id.doa);
		btn_sholat = (Button) findViewById(R.id.sholat);
		btn_wudlu = (Button) findViewById(R.id.wudlu);
		btn_Game = (Button) findViewById(R.id.game);
		btn_Nabi = (Button) findViewById(R.id.kisah);
		btn_rukun = (Button) findViewById(R.id.rukun);
		Animation animasi_BTN_nabi = AnimationUtils.loadAnimation(Home.this,
				R.anim.slide_nabi), animasi_BTN_rukun = AnimationUtils
				.loadAnimation(Home.this, R.anim.slide_rukun), animasi_BTN_doa = AnimationUtils
				.loadAnimation(Home.this, R.anim.slide_doa), animasi_BTN_sholat = AnimationUtils
				.loadAnimation(Home.this, R.anim.slide_sholat), animasi_BTN_wudlu = AnimationUtils
				.loadAnimation(Home.this, R.anim.slide_wudlu), animasi_BTN_game = AnimationUtils
				.loadAnimation(Home.this, R.anim.slide_game);
		btn_Doa.setAnimation(animasi_BTN_doa);
		btn_sholat.setAnimation(animasi_BTN_sholat);
		btn_wudlu.setAnimation(animasi_BTN_wudlu);
		btn_Game.setAnimation(animasi_BTN_game);
		btn_Nabi.setAnimation(animasi_BTN_nabi);
		btn_rukun.setAnimation(animasi_BTN_rukun);
		menuSound.start();
	}

	public void ClickButtonMenu(View v) {
		if (v == findViewById(R.id.doa)) {
			startActivity(new Intent(Home.this, Doa.class));
		} else if (v == findViewById(R.id.sholat)) {
			startActivity(new Intent(Home.this, Sholat.class));
		} else if (v == findViewById(R.id.wudlu)) {
			startActivity(new Intent(Home.this, Wudlu.class));
		} else if (v == findViewById(R.id.game)) {
			startActivity(new Intent(Home.this, Game_Home.class));
		} else if (v == findViewById(R.id.kisah)) {
			startActivity(new Intent(Home.this, Nabi_Home.class));
		} else if (v == findViewById(R.id.rukun)) {
			startActivity(new Intent(Home.this, Rukun.class));
		}
		mp.start();
		overridePendingTransition(R.animator.animation1, R.animator.animation2);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.alarm:
			mp.start();
			startActivity(new Intent(Home.this, AlarmUtama.class));
			break;
		case R.id.help:
			mp.start();
			break;
		case R.id.exit:
			onBackPressed();
			break;
		}
		return false;
	}
}
