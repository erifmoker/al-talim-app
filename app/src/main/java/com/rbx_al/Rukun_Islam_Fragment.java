package com.rbx_al;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class Rukun_Islam_Fragment extends Fragment{

	private TextView judul,judul1,judul2,judul3,judul4,judul5;
	private View rukun1,rukun2,rukun3,rukun4,rukun5;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v= inflater.inflate(R.layout.layout_rukun_islam, container,false);
		judul=(TextView)v.findViewById(R.id.judul_rukun_islam);
		judul1=(TextView)v.findViewById(R.id.rukun_islam_tx1);judul2=(TextView)v.findViewById(R.id.rukun_islam_tx2);
		judul3=(TextView)v.findViewById(R.id.rukun_islam_tx3);judul4=(TextView)v.findViewById(R.id.rukun_islam_tx4);
		judul5=(TextView)v.findViewById(R.id.rukun_islam_tx5);
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "font/smart_kid.otf"),
				 face2=Typeface.createFromAsset(getActivity().getAssets(), "font/buka_puasa.ttf");
		judul.setTypeface(face2);judul1.setTypeface(face);judul2.setTypeface(face);
		judul3.setTypeface(face);judul4.setTypeface(face);judul5.setTypeface(face);
		rukun1=v.findViewById(R.id.rukunIslam1);rukun2=v.findViewById(R.id.rukunIslam2);
		rukun3=v.findViewById(R.id.rukunIslam3);rukun4=v.findViewById(R.id.rukunIslam4);
		rukun5=v.findViewById(R.id.rukunIslam5);
		rukun1.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list1));
		rukun2.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list2));
		rukun3.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list3));
		rukun4.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list4));
		rukun5.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list5));
		return v;
	}
}
