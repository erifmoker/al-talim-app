package com.rbx_al;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class Rukun_Iman_Fragment extends Fragment{

	TextView judul,judul_1,judul_2,judul_3,judul_4,judul_5,judul_6,
			 kepada1,kepada2,kepada3,kepada4,kepada5,kepada6,
			 no1,no2,no3,no4,no5,no6;
	private View rukun1,rukun2,rukun3,rukun4,rukun5,rukun6;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
		View v= inflater.inflate(R.layout.layout_rukun_iman, container,false);
		judul=(TextView)v.findViewById(R.id.judul_rukun_iman);judul_1=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_1);
		judul_2=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_2);judul_3=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_3);
		judul_4=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_4);judul_5=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_5);
		judul_6=(TextView)v.findViewById(R.id.rukun_iman_tx_Judul_Iman_6);
		kepada1=(TextView)v.findViewById(R.id.rukun_iman_kepada1);kepada2=(TextView)v.findViewById(R.id.rukun_iman_kepada2);
		kepada3=(TextView)v.findViewById(R.id.rukun_iman_kepada3);kepada4=(TextView)v.findViewById(R.id.rukun_iman_kepada4);
		kepada5=(TextView)v.findViewById(R.id.rukun_iman_kepada5);kepada6=(TextView)v.findViewById(R.id.rukun_iman_kepada6);
		no1=(TextView)v.findViewById(R.id.rukun_iman_no1);no2=(TextView)v.findViewById(R.id.rukun_iman_no2);
		no3=(TextView)v.findViewById(R.id.rukun_iman_no3);no4=(TextView)v.findViewById(R.id.rukun_iman_no4);
		no5=(TextView)v.findViewById(R.id.rukun_iman_no5);no6=(TextView)v.findViewById(R.id.rukun_iman_no6);
		Typeface face=Typeface.createFromAsset(getActivity().getAssets(), "font/smart_kid.otf"),
				 face2=Typeface.createFromAsset(getActivity().getAssets(), "font/buka_puasa.ttf");
		judul.setTypeface(face2);judul_1.setTypeface(face);judul_2.setTypeface(face);judul_3.setTypeface(face);
		judul_4.setTypeface(face);judul_5.setTypeface(face);judul_6.setTypeface(face);
		kepada1.setTypeface(face);kepada2.setTypeface(face);kepada3.setTypeface(face);kepada4.setTypeface(face);
		kepada5.setTypeface(face);kepada6.setTypeface(face);
		no1.setTypeface(face);no2.setTypeface(face);no3.setTypeface(face);no4.setTypeface(face);
		no5.setTypeface(face);no6.setTypeface(face);
		
		rukun1=v.findViewById(R.id.rukunIman1);rukun2=v.findViewById(R.id.rukunIman2);
		rukun3=v.findViewById(R.id.rukunIman3);rukun4=v.findViewById(R.id.rukunIman4);
		rukun5=v.findViewById(R.id.rukunIman5);rukun6=v.findViewById(R.id.rukunIman6);
		rukun1.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list1));
		rukun2.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list2));
		rukun3.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list3));
		rukun4.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list4));
		rukun5.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list5));
		rukun6.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rukun_list6));
		return v;
	}
}
