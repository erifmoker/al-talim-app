package com.rbx_al;

import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Isi_doa extends Activity {

	String judulnya,bacanya,artinya,arabnya;
	int gambarnya,audionya,urut=0;
	TextView judul,arab,baca,arti,tx_pancingan;
	Button img;
	MediaPlayer mp,klik,efek1,efek2,efek3;
	Button play,back,next;
	Database ambil=new Database();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_doa_isi);
		klik=MediaPlayer.create(this, R.raw.klik);
		font();tx_pancingan=(TextView)findViewById(R.id.tx_isi_doa_pancingan);
		tx_pancingan.setAnimation(AnimationUtils.loadAnimation(Isi_doa.this, R.anim.animasi_btn_rukun_pancingan));
		efek1=MediaPlayer.create(this, R.raw.effect_fast_swoosh_down);
		efek2=MediaPlayer.create(this, R.raw.effect_fast_swish);
		efek3=MediaPlayer.create(this, R.raw.effect_released);
		efek1.start();efek3.start();
		Bundle extras=getIntent().getExtras();
		if(extras != null){
			judulnya=extras.getString("judule");
			gambarnya=extras.getInt("gambare");
			arabnya=extras.getString("arabe");
			audionya=extras.getInt("audione");
			bacanya=extras.getString("bacane");
			artinya=extras.getString("artine");
		}
		judul=(TextView) findViewById(R.id.judulnyaa);
		arab=(TextView) findViewById(R.id.sholat_detail_arab);
		baca=(TextView) findViewById(R.id.sholat_detail_bacaan);
		arti=(TextView) findViewById(R.id.sholat_detail_arti);
		
		judul.setText(judulnya);
		arab.setText(arabnya);
		baca.setText(bacanya);
		arti.setText(artinya);
		img=(Button) findViewById(R.id.gambar);
		img.setBackgroundResource(gambarnya);
		mp=MediaPlayer.create(this, audionya);
		anim_button();
		play=(Button) findViewById(R.id.btnplay);
		play.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(mp.isPlaying()){
					play.setBackgroundResource(R.drawable.playnya);
					mp.pause();
				}else{
					play.setBackgroundResource(R.drawable.pausenya);
					mp.start();
				}
			}
		});
		
		mp.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				play.setBackgroundResource(R.drawable.playnya);
			}
		});
	next=(Button)findViewById(R.id.btnnext);
	back=(Button)findViewById(R.id.btnback);
	next.setOnClickListener(new OnClickListener() {
		public void onClick(View v) {
			anim_button();
			mp.stop();
			klik.start();
			play.setBackgroundResource(R.drawable.playnya);
			if(judul.getText().toString().equals(ambil.doa)){	
				}urut++;
			if(urut<ambil.doa.length){
				judul.setText(ambil.doa[urut]);
				arab.setText(ambil.arab[urut]);
				baca.setText(ambil.baca[urut]);
				arti.setText(ambil.arti[urut]);
				img.setBackgroundResource(ambil.gambar_besar[urut]);
				zoom();efek3.start();efek2.start();
				mp=MediaPlayer.create(getBaseContext(), ambil.audio[urut]);
			}
		}
	});
	back.setOnClickListener(new OnClickListener() {
		public void onClick(View v) {
			mp.stop();
			klik.start();
			finish();
			}
	});
	}
	
	public void onBackPressed(){
		mp.stop();
		klik.start();
		finish();
	}
	
	public void onPause(){
		super.onPause();
		play.setBackgroundResource(R.drawable.playnya);
		mp.pause();
	}
	
	public void font(){
		TextView txt=(TextView)findViewById(R.id.judulnyaa),baca=(TextView)findViewById(R.id.sholat_detail_bacaan),
				 arti=(TextView)findViewById(R.id.sholat_detail_arti),tanda_baca=(TextView)findViewById(R.id.sholat_detail_cara_baca),
				 tanda_arti=(TextView)findViewById(R.id.sholat_detail_tanda_arti);
		Typeface face= Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 facekids= Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		txt.setTypeface(face);baca.setTypeface(facekids);arti.setTypeface(facekids);
		tanda_baca.setTypeface(facekids); tanda_arti.setTypeface(facekids);
	}
	public void zoom(){
		img=(Button) findViewById(R.id.gambar);
		Animation animasi_zoom=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.mengecil);
		img.setAnimation(animasi_zoom);
	}
	public void anim_button(){
		View layout_bacaan=findViewById(R.id.lay_bacaan_sholat);
		back=(Button) findViewById(R.id.btnback);
		next=(Button)findViewById(R.id.btnnext);
		play=(Button) findViewById(R.id.btnplay);
		img=(Button) findViewById(R.id.gambar);
		Animation animasi_kiri=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.animasi_btn_back),
				  animasi_kanan=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.animasi_btn_next),
				  animasi_bawah=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.animasi_btn_play),
				  animasi_mengecil=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.mengecil),
						  animasi_bacaan=AnimationUtils.loadAnimation(Isi_doa.this, R.anim.slide_sholat);
		back.setAnimation(animasi_kiri);play.setAnimation(animasi_bawah);next.setAnimation(animasi_kanan);
		img.setAnimation(animasi_mengecil);
		layout_bacaan.setAnimation(animasi_bacaan);
	}
	}
