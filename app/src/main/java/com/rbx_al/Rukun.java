package com.rbx_al;

import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class Rukun extends FragmentActivity {

	public Button btn_Rukun_Islam,btn_Rukun_Iman;
	public TextView pancingan;
	public Fragment newFragment;
	private MediaPlayer mp,efek1,efek2,wer;
	private View ganti;
	private Database_rukun database=new Database_rukun();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_rukun);font();
		pancingan=(TextView)findViewById(R.id.tx_Pilih_Salah_satu);
		pancingan.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_pancingan));
		efek1=MediaPlayer.create(this, R.raw.effect_fast_swish);
		efek2=MediaPlayer.create(this, R.raw.effect_fast_swoosh_down);
		wer=MediaPlayer.create(this, R.raw.effect_power_up);
		btn_Rukun_Islam=(Button)findViewById(R.id.btn_Rukun_Islam);
		btn_Rukun_Iman=(Button)findViewById(R.id.btn_Rukun_Iman);
		mp=MediaPlayer.create(getBaseContext(), R.raw.klik);
		ganti=findViewById(R.id.layout_ganti);
	}
	public void pilihFragment(View v){
		if(v==findViewById(R.id.btn_Rukun_Islam)){
			newFragment=new Rukun_Islam_Fragment();
			final TextView tx=(TextView)findViewById(R.id.tx_pilih);
			new CountDownTimer(1500,1000) {
				public void onTick(long millisUntilFinished) {
					btn_Rukun_Islam.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_islam));
					btn_Rukun_Iman.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_iman));
					tx.setText("");efek1.start();
					btn_Rukun_Islam.setClickable(false);
					if(btn_Rukun_Iman.isShown()){
					}else{
						ganti.setAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.animasi_rukun_bawah_exit));
					}
				}
				public void onFinish() {
					FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
					ft.replace(R.id.layout_ganti, newFragment);
					ft.addToBackStack(null);ft.commit();wer.start();
					btn_Rukun_Islam.setVisibility(View.GONE);
					if(btn_Rukun_Iman.isShown()){
						btn_Rukun_Iman.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_keluar));
					}else{
						btn_Rukun_Iman.setVisibility(View.VISIBLE);efek2.start();wer.start();
						btn_Rukun_Iman.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_keluar));
						btn_Rukun_Iman.setClickable(true);
						ganti.setAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.animasi_rukun_bawah));
					}
				}
			}.start();
			mp.start();
		}else if(v==findViewById(R.id.btn_Rukun_Iman)){
			newFragment=new Rukun_Iman_Fragment();
			final TextView tx=(TextView)findViewById(R.id.tx_pilih);
			new CountDownTimer(1500,1000) {
				public void onTick(long millisUntilFinished) {
					btn_Rukun_Iman.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_iman));
					btn_Rukun_Islam.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_islam));
					btn_Rukun_Iman.setClickable(false);
					tx.setText("");efek1.start();
					if(btn_Rukun_Islam.isShown()){
					}else{
						ganti.setAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.animasi_rukun_atas_exit));
					}
				}
				public void onFinish() {
					FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
					ft.replace(R.id.layout_ganti, newFragment);
					ft.addToBackStack(null);ft.commit();wer.start();
					btn_Rukun_Iman.setVisibility(View.GONE);
					if(btn_Rukun_Islam.isShown()){
						btn_Rukun_Islam.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_keluar1));
					}else{
						btn_Rukun_Islam.setVisibility(View.VISIBLE);efek2.start();wer.start();
						btn_Rukun_Islam.setAnimation(AnimationUtils.loadAnimation(Rukun.this, R.anim.animasi_btn_rukun_keluar1));
						btn_Rukun_Islam.setClickable(true);
						ganti.setAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.animasi_rukun_atas));
					}
				}
			}.start();
			mp.start();
		}
	}
	public void onBackPressed(){
		mp.start();
		startActivity(new Intent(Rukun.this, Home.class));
	}
	public void font(){
		Typeface header=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		TextView tx=(TextView)findViewById(R.id.tx_pilih),
				 btn1=(TextView)findViewById(R.id.btn_Rukun_Islam),
				 btn2=(TextView)findViewById(R.id.btn_Rukun_Iman);
		tx.setTypeface(header);btn1.setTypeface(header);btn2.setTypeface(header);
	}
	
	public void klik_islam(View v){
		Intent pindah=new Intent(Rukun.this, Rukun_Islam_detail.class);
		if(v==findViewById(R.id.rukun_islam_klik1)){
			pindah.putExtra("judul", database.judul_islam[0]);
			pindah.putExtra("penjelasan", database.islam[0]);
		}else if(v==findViewById(R.id.rukun_islam_klik2)){
			pindah.putExtra("judul", database.judul_islam[1]);
			pindah.putExtra("penjelasan", database.islam[1]);
		}else if(v==findViewById(R.id.rukun_islam_klik3)){
			pindah.putExtra("judul", database.judul_islam[2]);
			pindah.putExtra("penjelasan", database.islam[2]);
		}else if(v==findViewById(R.id.rukun_islam_klik4)){
			pindah.putExtra("judul", database.judul_islam[3]);
			pindah.putExtra("penjelasan", database.islam[3]);
		}else if(v==findViewById(R.id.rukun_islam_klik5)){
			pindah.putExtra("judul", database.judul_islam[4]);
			pindah.putExtra("penjelasan", database.islam[4]);
		}
		startActivity(pindah);mp.start();
	}
	
	public void klik_iman(View v){
		Intent pindah=new Intent(Rukun.this, Rukun_Islam_detail.class);
		if(v==findViewById(R.id.rukun_iman_klik1)){
			pindah.putExtra("judul", database.judul_iman[0]);
			pindah.putExtra("penjelasan", database.detail_iman[0]);
		}else if(v==findViewById(R.id.rukun_iman_klik2)){
			pindah.putExtra("judul", database.judul_iman[1]);
			pindah.putExtra("penjelasan", database.detail_iman[1]);
		}else if(v==findViewById(R.id.rukun_iman_klik3)){
			pindah.putExtra("judul", database.judul_iman[2]);
			pindah.putExtra("penjelasan", database.detail_iman[2]);
		}else if(v==findViewById(R.id.rukun_iman_klik4)){
			pindah.putExtra("judul", database.judul_iman[3]);
			pindah.putExtra("penjelasan", database.detail_iman[3]);
		}else if(v==findViewById(R.id.rukun_iman_klik5)){
			pindah.putExtra("judul", database.judul_iman[4]);
			pindah.putExtra("penjelasan", database.detail_iman[4]);
		}else if(v==findViewById(R.id.rukun_iman_klik6)){
			pindah.putExtra("judul", database.judul_iman[5]);
			pindah.putExtra("penjelasan", database.detail_iman[5]);
		}
		startActivity(pindah);mp.start();
	}
}
