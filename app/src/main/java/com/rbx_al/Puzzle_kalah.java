package com.rbx_al;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

public class Puzzle_kalah extends Activity {

	TextView judul_gagal;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_puzzle_kalah);
		font();
	}

	public void onBackPressed(){
		
	}
	
	public void ulangi(View v){
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		if(sp.getString("puzzle", "").equals("1")){
			startActivity(new Intent(Puzzle_kalah.this, Puzzle1.class));
		}else if(sp.getString("puzzle", "").equals("2")){
			startActivity(new Intent(Puzzle_kalah.this, Puzzle2.class));
		}else if(sp.getString("puzzle", "").equals("3")){
			startActivity(new Intent(Puzzle_kalah.this, Puzzle3.class));
		}else if(sp.getString("puzzle", "").equals("4")){
			startActivity(new Intent(Puzzle_kalah.this, Puzzle4.class));
		}
	}
	public void daftar(View v){
		startActivity(new Intent(Puzzle_kalah.this, Game_Level.class));
	}
	
	public void font(){
		judul_gagal=(TextView)findViewById(R.id.puzzle_kalah_txt_gagal);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul_gagal.setTypeface(face);
	}

}
