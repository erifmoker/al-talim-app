package com.rbx_al;

public class Database_Game {
	
	//Level 1
	String[] kategori={
			"Pengetahuan Do'a",
			"Pengetahuan Sholat",
			"Pengetahuan Nabi",
			"Pengetahuan Sholat",
			"Pengetahuan Wudlu",
			"Pengetahuan Sholat",
			"Rukun Islam & Iman",
			"Pengetahuan Nabi",
			"Pengetahuan Do'a",
			"Rukun Islam & Iman"
	};
	String[] pertanyaan={
			"Bacaan yang dibaca ketika sebelum melakukan sesuatu ialah bacaan ?",
			"Sholat shubuh terdiri dari ..... rakaat",
			"Siapakah Nabi yang pertama kali turun ke bumi ?",
			"Sholat maghrib terdiri dari ...... raka'at",
			"Apabila tidak ada air untuk bersuci, maka kita bersuci dengan cara ?",
			"Sholat azhar terdiri dari ...... raka'at",
			"Ada berapakah jumlah rukun Iman ?",
			"Berapakah jumlah Rasul Allah SWT ?",
			"Sebelum makan, kita harus membaca do'a ... ?",
			"Ada berapakah jumlah rukun Islam ?"
			
	};
	String[] jawaban_benar={
			"Do'a",
			"2 Rakaat",
			"Adam A.S.",
			"3 Raka'at",
			"Tayamum",
			"4 Rokaat",
			"Enam",
			"25 Rasul",
			"Akan makan",
			"Ada 5"
	};
	String[] opsi={
			"Do'a","Sholat","Lagu","Novel",
			"1 Rakaat","2 Rakaat","3 Rakaat","4 Rakaat",
			"Musa A.S.","Isa A.S.","Daud A.S.","Adam A.S.",
			"1 Raka'at","2 Raka'at","3 Raka'at","4 Raka'at",
			"Mandi","Tayamum","Berkumur","Minum",
			"1 Rokaat","2 Rokaat","3 Rokaat","4 Rokaat",
			"Satu","Tiga","Enam","Empat",
			"25 Rasul","20 Rasul","15 Rasul","21 Rasul",
			"Setelah makan","Akan makan","Akan tidur","Masuk WC",
			"Ada 6","Ada 7","Ada 5","Ada 4"
	};
	
	//Level 2
		String[] kategori_lev2={
				"Pengetahuan Do'a",
				"Pengetahuan Sholat",
				"Pengetahuan Nabi",
				"Pengetahuan Sholat",
				"Pengetahuan Wudlu",
				"Pengetahuan Sholat",
				"Rukun Islam & Iman",
				"Pengetahuan Nabi",
				"Pengetahuan Do'a",
				"Rukun Islam & Iman"
		};
		String[] pertanyaan_lev2={
				"اَلْحَمْدُ ِللهِ الَّذِىْ اَحْيَانَا بَعْدَمَآ اَمَاتَنَا وَاِلَيْهِ النُّشُوْرُ\nBacaan diatas adalah bacaan do'a ?",
				"Sholat yang dilakukan 2 raka'at adalah sholat ?",
				"Siapakah nama Nabi yang terakhir ?",
				"Sholat fardlu atau yang wajib dikerjakan ada .... waktu",
				"Tayamum merupakan cara bersuci menggunakan ?",
				"Sholat fardlu apabila dijumlahkan seluruhnya ada .... rakaat",
				"Percaya pada Qada' dan Qadar merupakan rukun iman yang ke ?",
				"Nabi Idris A.S adalah termasuk nabi yang ke ?",
				"اَللهُمَّ افْتَحْ لِىْ اَبْوَابَ رَحْمَتِكَ\ndo'a di atas merupakan do'a ... ?",
				"Syahadat merupakan rukun Islam yang ?"
				
		};
		String[] jawaban_benar_lev2={
				"Bangun tidur",
				"Subuh",
				"Muhammad SAW",
				"5 waktu",
				"Debu suci",
				"17 Rokaat",
				"Enam",
				"Dua",
				"Masuk masjid",
				"Pertama"
		};
		String[] opsi_lev2={
				"Akan makan","Masuk WC","Masuk Masjid","Bangun tidur",
				"Subuh","Azhar","Maghrib","Dhuhur",
				"Musa A.S.","Muhammad SAW","Daud A.S.","Adam A.S.",
				"1 waktu","2 waktu","3 waktu","5 waktu",
				"Udara","Debu","Debu suci","Air",
				"10 Rokaat","12 Rokaat","13 Rokaat","17 Rokaat",
				"Satu","Tiga","Enam","Empat",
				"Dua","Tiga","Lima","Tujuh",
				"Akan makan","Masuk WC","Masuk masjid","Akan Tidur",
				"Kedua","Pertama","Ketiga","Kelima"
		};
		
		//Level 3
				String[] kategori_lev3={
						"Pengetahuan Do'a",
						"Pengetahuan Sholat",
						"Pengetahuan Nabi",
						"Pengetahuan Sholat",
						"Pengetahuan Wudlu",
						"Pengetahuan Sholat",
						"Rukun Islam & Iman",
						"Pengetahuan Nabi",
						"Pengetahuan Do'a",
						"Rukun Islam & Iman"
				};
				int[] gbr_pertanyaan_lev3={
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_sholat_ruku,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_wd3,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
				};
				String[] pertanyaan_lev3={
						"اَللهُمَّ اِنّىْ اَعُوْذُبِكَ مِنَ الْخُبُثِ وَالْخَبَآئِثِ\nBacaan diatas adalah bacaan do'a ?",
						"Gambar diatas adalah gerakan sholat pada saat ?",
						"Nabi Adam A.S. dihukum dan diturunkan ke bumi karena memakan buah ?",
						"الله اكبر\nBacaan diatas dibaca ketika ?",
						"Setelah melakukan gerakan diatas lalu dilanjutkan dengan ?",
						"سُبْحَانَ رَبِّيَ\nLanjutan bacaan sujud diatas adalah ?",
						"Percaya pada Hari Kiamat merupakan rukun iman yang ke ?",
						"Nabi yang bisa berbicara dengan hewan adalah Nabi ?",
						"اَلْحَمْدُ ِللهِ\ndo'a di atas merupakan bagian do'a ... ?",
						"Rukun Islam yang ketiga adalah ?"
						
				};
				String[] jawaban_benar_lev3={
						"Masuk WC",
						"ruku'",
						"Quldi",
						"Takbiratul\nIkhram",
						"Memebasuh\ntangan",
						"اْلأَعْلَى",
						"Lima",
						"Isa A.S",
						"Bersin",
						"Zakat"
				};
				String[] opsi_lev3={
						"Akan makan","Masuk WC","Masuk Masjid","Bangun tidur",
						"sujud","takbir","ruku'","salam",
						"Apel","Tomat","Semangka","Quldi",
						"Niat","Wudlu","I'tidal","Takbiratul\nIkhram",
						"Memebasuh\nkaki","Memebasuh\nkepala","Memebasuh\ntangan","Memebasuh\nmuka",
						"اْسُبْحَانَ","الْحَمْدُ","الْعَظِيْمِ","اْلأَعْلَى",
						"Lima","Tiga","Enam","Empat",
						"Isa A.S","Musa A.S","Luth A.S","Yunus A.S",
						"akan makan","Bersin","masuk wc","bangun tidur",
						"Zakat","Puasa","Syahadat","Sholat"
				};
				
				//Level 4
				String[] kategori_lev4={
						"Pengetahuan Do'a",
						"Pengetahuan Sholat",
						"Pengetahuan Nabi",
						"Pengetahuan Sholat",
						"Pengetahuan Wudlu",
						"Pengetahuan Sholat",
						"Rukun Islam & Iman",
						"Pengetahuan Nabi",
						"Pengetahuan Do'a",
						"Rukun Islam & Iman"
				};
				int[] gbr_pertanyaan_lev4={
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_wd4,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
				};
				String[] pertanyaan_lev4={
						"بِسْمِ اللهِ تَوَكَّلْتُ عَلَى اللهِ لاَحَوْلَ وَلاَقُوَّةَ اِلاَّ بِاللهِ\nBacaan diatas adalah bacaan do'a ?",
						"Amalan yang pertama kali dihisab pada hari kiamat adalah ?",
						"Kitab apakah yang diterima nabi oleh Musa A.S. ?",
						"Membaca do'a Qunut merupakan bagian dari sholat ?",
						"Membasuh bagian-bagian tertentu dalam wudlu dilakukan sebanyak ?",
						"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ\nBacaan diatas adalah bacaan ketika ?",
						"Ruh dari rukun islam adalah ?",
						"Nabi yang membangun ka'bah di kota mekkah adalah nabi ?",
						"اَلْحَمْدُ ِللهِ الَّذِىْ اَطْعَمَنَا وَسَقَانَا وَجَعَلَنَا مُسْلِمِيْنَ\nDo'a di atas merupakan  do'a ... ?",
						"Manakah yang bukan syarat haji ?"
						
				};
				String[] jawaban_benar_lev4={
						"Keluar Rumah",
						"Sholat",
						"Taurat",
						"Subuh",
						"3 Kali",
						"I'tidal",
						"Syahadat",
						"Ibrahim A.S",
						"Setelah\nMakan",
						"Telah Umroh"
				};
				String[] opsi_lev4={
						"Akan makan","Masuk WC","Keluar Rumah","Bangun tidur",
						"Sholat","Zakat","Puasa'","Haji",
						"Al-Qur'an","Taurat","Injil","Zabur",
						"Subuh","Dhuhur","Sunnah","Azhar",
						"1 Kali","2 Kali","4 Kali","3 Kali",
						"Salam","Ruku'","Sujud","I'tidal",
						"sholat","zakat","Syahadat","haji",
						"Ibrahim A.S","Musa A.S","Luth A.S","Yunus A.S",
						"Sebelum\nMakan","Setelah\nMakan","Masuk WC","Bangun Tidur",
						"Merdeka","Mampu","Telah Umroh","Islam"
				};
				
				//Level 5
				String[] kategori_lev5={
						"Pengetahuan Do'a",
						"Pengetahuan Sholat",
						"Pengetahuan Nabi",
						"Pengetahuan Sholat",
						"Pengetahuan Wudlu",
						"Pengetahuan Sholat",
						"Rukun Islam & Iman",
						"Pengetahuan Nabi",
						"Pengetahuan Nabia",
						"Rukun Islam & Iman"
				};
				int[] gbr_pertanyaan_lev5={
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_sholat_iktidal,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_wd6,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
						R.drawable.pert_gbr_kosong,
				};
				String[] pertanyaan_lev5={
						"Ya Allah, bukakanlah bagiku pintu-pintu rahmat-Mu adalah arti dari do'a ?",
						"إِنِّي وَجَّهْتُ adalah bacaan dari ?",
						"Ke kota manakah Nabi Hud A.S. pindah ?",
						"Kiblat orang muslim adalah ?",
						"Bagian yang dibasuh setelah melakukan gerakan diatas adalah ?",
						"فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد\nBacaan diatas adalah bagian dari ?",
						"Pembeda antara orang muslim dengan orang kafir adalah ?",
						"Siapakah Nabi  yang dibawa ke surga tanpa kematian ?",
						"Adzab yang diterima kaum Nabi Sholeh yang membunuh unta betina adalah ?",
						"Yang bukan arti dari zakat adalah ?"
						
				};
				String[] jawaban_benar_lev5={
						"Masuk Masjid",
						"Iftitah",
						"hadramaut",
						"Ka'bah",
						"Kaki",
						"Tasyahud\nAkhir",
						"sholat",
						"Idris A.S",
						"Petir dan\nGempa",
						"Pemberian"
				};
				String[] opsi_lev5={
						"Masuk Masjid","Masuk WC","Keluar Rumah","Bangun tidur",
						"Takbir","Iftitah","I'tidal","Ruku'",
						"mekkah","madinah","yerussalem","hadramaut",
						"Arab","Ka'bah","Barat","Mekkah",
						"Kaki","Tangan","Muka","Kepala",
						"Fatikhah","Diantara 2\nSujud","Tasyahud\nAkhir","Tasyahud\nAwal",
						"sholat","zakat","Syahadat","haji",
						"Isa A.S","Musa A.S","Idris A.S","Sulaiman A.S",
						"Tenggelam\nke laut","Badai","Penyakit","Petir dan\nGempa",
						"Bersih","Pemberian","Tumbuh","Suci"
				};
}
