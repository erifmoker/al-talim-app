package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

public class Game_Nilai_lev4 extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_game__nilai_lev4);
		tampil_nilai();font();
	}
	public void tampil_nilai(){
		TextView nilai=(TextView) findViewById(R.id.game_nilai_lev4_nilai_nilai),
				 salah_benar=(TextView) findViewById(R.id.game_nilai_lev4_salah_benar),
				 ucapan=(TextView)findViewById(R.id.game_nilai_lev4_tx_ucapan);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		nilai.setText(sp.getString("level4", ""));
		salah_benar.setText(sp.getString("salah_level4", "")+"\n"+sp.getString("benar_level4", "")+"\n"+sp.getString("tdk_terjawab", ""));
		nilai.setAnimation(AnimationUtils.loadAnimation(Game_Nilai_lev4.this, R.anim.jrot));
		String nilainya=nilai.getText().toString();
		if(Integer.parseInt(nilainya)>80){
			ucapan.setText("Alhamdulillah");
		}else{
			ucapan.setText("Belajar Lagi Ya !");
		}
		MediaPlayer mp=MediaPlayer.create(Game_Nilai_lev4.this, R.raw.boom);
		mp.start();
	}
	public void ulangi(View v){
		startActivity(new Intent(Game_Nilai_lev4.this, Game_Play_lev4.class));
	}
	public void daftar(View v){
		startActivity(new Intent(Game_Nilai_lev4.this, Game_Level.class));
	}
	public void onBackPressed(){
		Toast.makeText(getBaseContext(), "Pilih level atau ulangi permainan", 500).show();
	}
	public void font(){
		TextView label=(TextView)findViewById(R.id.game_nilai_lev4_tx_label_nilai),
				 nilai=(TextView)findViewById(R.id.game_nilai_lev4_nilai_nilai),
				 ucapan=(TextView)findViewById(R.id.game_nilai_lev4_tx_ucapan),
				 salah_benar=(TextView)findViewById(R.id.game_nilai_lev4_salah_benar);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf");
		label.setTypeface(face);nilai.setTypeface(face);ucapan.setTypeface(face);salah_benar.setTypeface(face);
	}
}
