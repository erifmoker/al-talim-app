package com.rbx_al;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Sholat_detail extends Activity {

	public String judulnya,arabnya,bacanya,artinya;
	public int gambarnya;
	public TextView judul,arab,baca,arti,wadah_baca,wadah_arti,judul_header;
	public ImageView gambar;
	public MediaPlayer mp,bacaan;
	public int urutan=0,audio;
	public Button berikut,sebelum,play;
	Database_sholat ambil=new Database_sholat();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_sholat_detail);
		judul=(TextView)findViewById(R.id.sholat_detail_judul_sholat);
		arab=(TextView)findViewById(R.id.sholat_detail_arab);
		baca=(TextView)findViewById(R.id.sholat_detail_bacaan);
		arti=(TextView)findViewById(R.id.sholat_detail_arti);
		gambar=(ImageView)findViewById(R.id.sholat_detail_image);
		berikut=(Button)findViewById(R.id.sholat_detail_btn_berikutnya);
		sebelum=(Button)findViewById(R.id.sholat_detail_btn_sebelumnya);
		play=(Button)findViewById(R.id.sholat_detail_btn_play);
		sebelum.setVisibility(View.GONE);
		mp=MediaPlayer.create(getBaseContext(), R.raw.klik);
		Bundle extras=getIntent().getExtras();font();
		if(extras != null){
			judulnya=extras.getString("judul_niat");
			gambarnya=extras.getInt("gambar_niat");
			arabnya=extras.getString("arab");
			bacanya=extras.getString("cara_baca");
			artinya=extras.getString("arti");
			audio=extras.getInt("audio_sholat");
		}
		judul.setText(judulnya);gambar.setImageResource(gambarnya);arab.setText(arabnya);
		baca.setText(bacanya);arti.setText(artinya);bacaan=MediaPlayer.create(this, audio);
		
		bacaan.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				play.setBackgroundResource(R.drawable.playnya);
			}
		});
		
	}
	public void font(){
		judul=(TextView)findViewById(R.id.sholat_detail_judul_sholat);
		baca=(TextView)findViewById(R.id.sholat_detail_bacaan);
		arti=(TextView)findViewById(R.id.sholat_detail_arti);
		wadah_baca=(TextView)findViewById(R.id.sholat_detail_cara_baca);
		wadah_arti=(TextView)findViewById(R.id.sholat_detail_tanda_arti);
		judul_header=(TextView)findViewById(R.id.sholat_detail_judul);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf");
		judul.setTypeface(face);baca.setTypeface(face);arti.setTypeface(face);wadah_baca.setTypeface(face);
		wadah_arti.setTypeface(face);judul_header.setTypeface(face2);
	}
	public void Button(View v){
		Database_sholat database=new Database_sholat();
		String ambil_judul=judul.getText().toString();
		if(v==findViewById(R.id.sholat_detail_btn_kembali)){
			finish();mp.start();bacaan.pause();
		}else if(v==findViewById(R.id.sholat_detail_btn_play)){
			if(bacaan.isPlaying()){
				play.setBackgroundResource(R.drawable.playnya);
				bacaan.pause();
			}else{
				play.setBackgroundResource(R.drawable.pausenya);
				bacaan.start();
			}
			bacaan.setOnCompletionListener(new OnCompletionListener() {
				public void onCompletion(MediaPlayer mp) {
					play.setBackgroundResource(R.drawable.playnya);
				}
			});
		}
		else if(v==findViewById(R.id.sholat_detail_btn_sebelumnya)){
			bacaan.pause();play.setBackgroundResource(R.drawable.playnya);
			if(ambil_judul.equals(database.judul_subuh[urutan])){
				if(ambil_judul.equals(database.judul_subuh[1])){
					sebelum.setVisibility(View.GONE);
					bacaan.pause();
					bacaan.start();
				}
				urutan--;
				if(urutan<database.judul_subuh.length){
					judul.setText(database.judul_subuh[urutan]);
					arab.setText(database.bacaan_subuh[urutan]);
					baca.setText(database.cara_baca_subuh[urutan]);
					arti.setText(database.arti_subuh[urutan]);
					gambar.setImageResource(database.gambar_subuh[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_subuh[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_dhuhur[urutan])){
				if(ambil_judul.equals(database.judul_dhuhur[1])){
					sebelum.setVisibility(View.GONE);
				}
				urutan--;
				if(urutan<database.judul_dhuhur.length){
					judul.setText(database.judul_dhuhur[urutan]);
					arab.setText(database.bacaan_dhuhur[urutan]);
					baca.setText(database.cara_baca_dhuhur[urutan]);
					arti.setText(database.arti_dhuhur[urutan]);
					gambar.setImageResource(database.gambar_dhuhur[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_dhuhur[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_azhar[urutan])){
				if(ambil_judul.equals(database.judul_azhar[1])){
					sebelum.setVisibility(View.GONE);
				}
				urutan--;
				if(urutan<database.judul_azhar.length){
					judul.setText(database.judul_azhar[urutan]);
					arab.setText(database.bacaan_azhar[urutan]);
					baca.setText(database.cara_baca_azhar[urutan]);
					arti.setText(database.arti_azhar[urutan]);
					gambar.setImageResource(database.gambar_azhar[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_azhar[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_maghrib[urutan])){
				if(ambil_judul.equals(database.judul_maghrib[1])){
					sebelum.setVisibility(View.GONE);
				}
				urutan--;
				if(urutan<database.judul_maghrib.length){
					judul.setText(database.judul_maghrib[urutan]);
					arab.setText(database.bacaan_maghrib[urutan]);
					baca.setText(database.cara_baca_maghrib[urutan]);
					arti.setText(database.arti_maghrib[urutan]);
					gambar.setImageResource(database.gambar_maghrib[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_maghrib[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_isya[urutan])){
				if(ambil_judul.equals(database.judul_isya[1])){
					sebelum.setVisibility(View.GONE);
				}
				urutan--;
				if(urutan<database.judul_isya.length){
					judul.setText(database.judul_isya[urutan]);
					arab.setText(database.bacaan_isya[urutan]);
					baca.setText(database.cara_baca_isya[urutan]);
					arti.setText(database.arti_isya[urutan]);
					gambar.setImageResource(database.gambar_isya[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_isya[urutan]);
				}
			}
			berikut.setVisibility(View.VISIBLE);mp.start();
		}else if(v==findViewById(R.id.sholat_detail_btn_berikutnya)){
			bacaan.pause();play.setBackgroundResource(R.drawable.playnya);
			if(ambil_judul.equals(database.judul_subuh[urutan])){
				if(ambil_judul.equals(database.judul_subuh[10])){
					berikut.setVisibility(View.GONE);
				}
				urutan++;
				if(urutan<database.judul_subuh.length){
					judul.setText(database.judul_subuh[urutan]);
					arab.setText(database.bacaan_subuh[urutan]);
					baca.setText(database.cara_baca_subuh[urutan]);
					arti.setText(database.arti_subuh[urutan]);
					gambar.setImageResource(database.gambar_subuh[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_subuh[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_dhuhur[urutan])){
				if(ambil_judul.equals(database.judul_dhuhur[10])){
					berikut.setVisibility(View.GONE);
				}
				urutan++;
				if(urutan<database.judul_dhuhur.length){
					judul.setText(database.judul_dhuhur[urutan]);
					arab.setText(database.bacaan_dhuhur[urutan]);
					baca.setText(database.cara_baca_dhuhur[urutan]);
					arti.setText(database.arti_dhuhur[urutan]);
					gambar.setImageResource(database.gambar_dhuhur[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_dhuhur[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_azhar[urutan])){
				if(ambil_judul.equals(database.judul_azhar[10])){
					berikut.setVisibility(View.GONE);
				}
				urutan++;
				if(urutan<database.judul_azhar.length){
					judul.setText(database.judul_azhar[urutan]);
					arab.setText(database.bacaan_azhar[urutan]);
					baca.setText(database.cara_baca_azhar[urutan]);
					arti.setText(database.arti_azhar[urutan]);
					gambar.setImageResource(database.gambar_azhar[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_azhar[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_maghrib[urutan])){
				if(ambil_judul.equals(database.judul_maghrib[10])){
					berikut.setVisibility(View.GONE);
				}
				urutan++;
				if(urutan<database.judul_maghrib.length){
					judul.setText(database.judul_maghrib[urutan]);
					arab.setText(database.bacaan_maghrib[urutan]);
					baca.setText(database.cara_baca_maghrib[urutan]);
					arti.setText(database.arti_maghrib[urutan]);
					gambar.setImageResource(database.gambar_maghrib[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_maghrib[urutan]);
				}
			}
			if(ambil_judul.equals(database.judul_isya[urutan])){
				if(ambil_judul.equals(database.judul_isya[10])){
					berikut.setVisibility(View.GONE);
				}
				urutan++;
				if(urutan<database.judul_isya.length){
					judul.setText(database.judul_isya[urutan]);
					arab.setText(database.bacaan_isya[urutan]);
					baca.setText(database.cara_baca_isya[urutan]);
					arti.setText(database.arti_isya[urutan]);
					gambar.setImageResource(database.gambar_isya[urutan]);
					bacaan=MediaPlayer.create(getBaseContext(), ambil.audio_isya[urutan]);
				}
			}
			sebelum.setVisibility(View.VISIBLE);mp.start();
		}
	}

}
