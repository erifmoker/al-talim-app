package com.rbx_al;

import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Tayamum1 extends Activity {
	private MediaPlayer mp;
	private TextView judul, judul_kategori, keterangan, arab,wadah_baca , cara_baca, wadah_arti, arti;
	public ImageView gambar;
	int urutan=0;
	private Database_tayamum database=new Database_tayamum();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_tayamum1);
		font();muncul_pertama();
		mp=MediaPlayer.create(this, R.raw.klik);
		judul_kategori=(TextView)findViewById(R.id.tayamum_judul_ty);
		keterangan=(TextView)findViewById(R.id.tayamum_isi_keterangan);
		arab=(TextView)findViewById(R.id.tayamum_detail_arab);
		wadah_baca=(TextView)findViewById(R.id.tayamum_detail_cara_baca);
		cara_baca=(TextView)findViewById(R.id.tayamum_detail_bacaan);
		wadah_arti=(TextView)findViewById(R.id.tayamum_detail_tanda_arti);
		arti=(TextView)findViewById(R.id.tayamum_detail_arti);
	}
	
	public void Kembali_tayamum(View v){
		mp.start();
		finish();
	}
	
	public void next_tayamum(View v){
		Button sebelum=(Button)findViewById(R.id.tayamum_btn_sebelumnya);
		gambar=(ImageView)findViewById(R.id.tayamum_image);
		if(judul_kategori.getText().toString().equals(database.judul_tayamum)){
		}urutan++;
		if(urutan<database.judul_tayamum.length){
			sebelum.setVisibility(View.VISIBLE);
			judul_kategori.setText(database.judul_tayamum[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan_tayamum[urutan]);
			arab.setText(database.arab_tayamum[urutan]);
			cara_baca.setText(database.cara_baca_tayamum[urutan]);
			arti.setText(database.arti_tayamum[urutan]);
			gambar.setImageResource(database.gambar_tayamum[urutan]);
		}if(judul_kategori.getText().toString().equals(database.judul_tayamum[5])){
			Button lanjut=(Button)findViewById(R.id.tayamum_btn_berikutnya);
			judul_kategori.setText(database.judul_tayamum[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan_tayamum[urutan]);
			arab.setText(database.arab_tayamum[urutan]);
			cara_baca.setText(database.cara_baca_tayamum[urutan]);
			arti.setText(database.arti_tayamum[urutan]);
			gambar.setImageResource(database.gambar_tayamum[urutan]);
			lanjut.setVisibility(View.GONE);
		}
			mp.start();
		}

	public void sebelumnya_tayamum(View v){
		urutan--;
		if(judul_kategori.getText().toString().equals(database.judul_tayamum[1])){
			Button sebelum=(Button)findViewById(R.id.tayamum_btn_sebelumnya);
			judul_kategori.setText(database.judul_tayamum[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan_tayamum[urutan]);
			arab.setText(database.arab_tayamum[urutan]);
			cara_baca.setText(database.cara_baca_tayamum[urutan]);
			arti.setText(database.arti_tayamum[urutan]);
			gambar.setImageResource(database.gambar_tayamum[urutan]);
			sebelum.setVisibility(View.GONE);
		}
		else{
			Button lanjut=(Button)findViewById(R.id.tayamum_btn_berikutnya);
			judul_kategori.setText(database.judul_tayamum[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan_tayamum[urutan]);
			arab.setText(database.arab_tayamum[urutan]);
			cara_baca.setText(database.cara_baca_tayamum[urutan]);
			arti.setText(database.arti_tayamum[urutan]);
			gambar.setImageResource(database.gambar_tayamum[urutan]);
			lanjut.setVisibility(View.VISIBLE);
		}
		mp.start();
	}
	
	public void muncul_pertama(){
		Button sebelum=(Button)findViewById(R.id.tayamum_btn_sebelumnya);
		sebelum.setVisibility(View.GONE);
		gambar=(ImageView)findViewById(R.id.tayamum_image);
		judul_kategori.setText(database.judul_tayamum[urutan]);
		keterangan.setText("Keterangan :\n"+database.keterangan_tayamum[urutan]);
		arab.setText(database.arab_tayamum[urutan]);
		cara_baca.setText(database.cara_baca_tayamum[urutan]);
		arti.setText(database.arti_tayamum[urutan]);
		gambar.setImageResource(database.gambar_tayamum[urutan]);
	}
	
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}
	public void font(){
		judul_kategori=(TextView)findViewById(R.id.tayamum_judul_ty);
		keterangan=(TextView)findViewById(R.id.tayamum_isi_keterangan);
		arab=(TextView)findViewById(R.id.tayamum_detail_arab);
		wadah_baca=(TextView)findViewById(R.id.tayamum_detail_cara_baca);
		cara_baca=(TextView)findViewById(R.id.tayamum_detail_bacaan);
		wadah_arti=(TextView)findViewById(R.id.tayamum_detail_tanda_arti);
		arti=(TextView)findViewById(R.id.tayamum_detail_arti);
		judul=(TextView)findViewById(R.id.tayamum_header);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);judul.setTypeface(face);judul_kategori.setTypeface(face2);keterangan.setTypeface(face2);
		wadah_baca.setTypeface(face2);cara_baca.setTypeface(face2);wadah_arti.setTypeface(face2);arti.setTypeface(face2);
	}
}
