package com.rbx_al;

public class Database_tayamum {
String[] keterangan_tayamum={
		"Petama adalah niat tayamum [niat dalam hati],",
		"Setelah niat tayamum, mula-mula meletakkan dua belah tangan di atas debu untuk diusapkan ke muka",
		"Debu yang ada di tangan ditiup dulu, kemudian selanjutnya mengusap muka dengan debu, dengan dua kali usapan",
		"Urutan cara tayamum yang ke empat adalah mengusap dua belah tangan sampai pergelangan tangan dengan debu sebanyak dua kali usapan",
		"Memindahkan debu kepada anggota yang diusap.",
		"Dilakukan secara berturut-turut atau tertib, berurutan dari urutan pertama hingga urutan terakhir dari tata cara tayamum."
	},judul_tayamum={
		"Membaca Niat Tayamum",
		"Mengambil Debu",
		"Mengusap Muka",
		"Mengusap Tangan",
		"Memindahkan Debu",
		"Berurutan"
	},
	arab_tayamum={
		"نويت التّيمّم لإستباحة الصّلاة فرضالله تعالى",
		"_","_","_","_","_"
	},
	cara_baca_tayamum={
		"Nawaitut tayammuma li-istibaahatish shalaati far-dlan lillaahi ta’aala",
		"_","_","_","_","_",
	},
	arti_tayamum={
		"aku niat betayammum untuk dapat mengerjakan shalat; fardlu karena Allah.",
		"_","_","_","_","_",
	};
	int[] gambar_tayamum={
		R.drawable.ty2,
		R.drawable.ty1,
		R.drawable.ty3,
		R.drawable.ty4,
		R.drawable.ty8,
		R.drawable.ty7,
	};
}
