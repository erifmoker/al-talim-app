package com.rbx_al;

import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Wudlu1 extends Activity {
	private MediaPlayer mp;
	private TextView judul, judul_kategori, keterangan, arab,wadah_baca , cara_baca, wadah_arti, arti;
	public ImageView gambar;
	int urutan=0;
	
	public Database_wudlu database=new Database_wudlu();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_wudlu1);
		font();muncul_pertama();
		mp=MediaPlayer.create(this, R.raw.klik);
		judul_kategori=(TextView)findViewById(R.id.sholat_detail_judul_sholat);
		keterangan=(TextView)findViewById(R.id.wudlu_isi_keterangan);
		arab=(TextView)findViewById(R.id.sholat_detail_arab);
		wadah_baca=(TextView)findViewById(R.id.sholat_detail_cara_baca);
		cara_baca=(TextView)findViewById(R.id.sholat_detail_bacaan);
		wadah_arti=(TextView)findViewById(R.id.sholat_detail_tanda_arti);
		arti=(TextView)findViewById(R.id.sholat_detail_arti);
	}

	public void Kembali(View v){
		mp.start();
		finish();
	}
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}
	public void font(){
		judul=(TextView)findViewById(R.id.judul_wd_air);
		judul_kategori=(TextView)findViewById(R.id.sholat_detail_judul_sholat);
		keterangan=(TextView)findViewById(R.id.wudlu_isi_keterangan);
		arab=(TextView)findViewById(R.id.sholat_detail_arab);
		wadah_baca=(TextView)findViewById(R.id.sholat_detail_cara_baca);
		cara_baca=(TextView)findViewById(R.id.sholat_detail_bacaan);
		wadah_arti=(TextView)findViewById(R.id.sholat_detail_tanda_arti);
		arti=(TextView)findViewById(R.id.sholat_detail_arti);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);judul_kategori.setTypeface(face2);keterangan.setTypeface(face2);
		wadah_baca.setTypeface(face2);cara_baca.setTypeface(face2);wadah_arti.setTypeface(face2);arti.setTypeface(face2);
	}

	public void muncul_pertama(){
		Button sebelum=(Button)findViewById(R.id.sholat_detail_btn_sebelumnya);
		sebelum.setVisibility(View.GONE);
		gambar=(ImageView)findViewById(R.id.sholat_detail_image);
		judul_kategori.setText(database.judul[urutan]);
		keterangan.setText("Keterangan :\n"+database.keterangan[urutan]);
		arab.setText(database.arab[urutan]);
		cara_baca.setText(database.cara_baca[urutan]);
		arti.setText(database.arti[urutan]);
		gambar.setImageResource(database.gambar[urutan]);
	}
	
	public void next(View v){
		Button sebelum=(Button)findViewById(R.id.sholat_detail_btn_sebelumnya);
		if(judul_kategori.getText().toString().equals(database.judul)){
		}urutan++;
		if(urutan<database.judul.length){
			sebelum.setVisibility(View.VISIBLE);
			judul_kategori.setText(database.judul[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan[urutan]);
			arab.setText(database.arab[urutan]);
			cara_baca.setText(database.cara_baca[urutan]);
			arti.setText(database.arti[urutan]);
			gambar.setImageResource(database.gambar[urutan]);
		}if(judul_kategori.getText().toString().equals(database.judul[9])){
			Button lanjut=(Button)findViewById(R.id.sholat_detail_btn_berikutnya);
			judul_kategori.setText(database.judul[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan[urutan]);
			arab.setText(database.arab[urutan]);
			cara_baca.setText(database.cara_baca[urutan]);
			arti.setText(database.arti[urutan]);
			gambar.setImageResource(database.gambar[urutan]);
			lanjut.setVisibility(View.GONE);
		}
		mp.start();
	}
	public void sebelumnya(View v){
		urutan--;
		if(judul_kategori.getText().toString().equals(database.judul[1])){
			Button sebelum=(Button)findViewById(R.id.sholat_detail_btn_sebelumnya);
			judul_kategori.setText(database.judul[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan[urutan]);
			arab.setText(database.arab[urutan]);
			cara_baca.setText(database.cara_baca[urutan]);
			arti.setText(database.arti[urutan]);
			gambar.setImageResource(database.gambar[urutan]);
			sebelum.setVisibility(View.GONE);
		}
		else{
			Button lanjut=(Button)findViewById(R.id.sholat_detail_btn_berikutnya);
			judul_kategori.setText(database.judul[urutan]);
			keterangan.setText("Keterangan :\n"+database.keterangan[urutan]);
			arab.setText(database.arab[urutan]);
			cara_baca.setText(database.cara_baca[urutan]);
			arti.setText(database.arti[urutan]);
			gambar.setImageResource(database.gambar[urutan]);
			lanjut.setVisibility(View.VISIBLE);
		}
		mp.start();
	}
}
