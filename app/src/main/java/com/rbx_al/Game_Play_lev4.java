package com.rbx_al;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

public class Game_Play_lev4 extends Activity {

	private Button jawab1, jawab2, jawab3, jawab4;
	private View wadah_soal;
	private TextView txWaktu, txPertanyaan, txkategori, Pancingan;
	private Penghitung_lev4 timer = new Penghitung_lev4(20000, 1000);
	private keputusan_lev4 keputusannya = new keputusan_lev4(2500, 1000);
	int urut = 0, nilai_benar = 0, nilai_salah = 0, benar=0, tidak_terjawab=0;
	public Database_Game isi_game = new Database_Game();
	public MediaPlayer ganti_soal, btn_benar, btn_salah, backsound;
	public ImageView image;

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_game__play_lev4);
		pertanyaan1_lev4();font();
		ganti_soal = MediaPlayer.create(getBaseContext(), R.raw.game_ganti_soal);
		btn_benar = MediaPlayer.create(getBaseContext(), R.raw.game_btn_benar);
		btn_salah = MediaPlayer.create(getBaseContext(), R.raw.game_btn_salah);
		timer.start();TextView txNilai = (TextView) findViewById(R.id.game_4nilaiNilai);
		TextView tdk_terjwb = (TextView) findViewById(R.id.game_4_tidak_terjawab);
		txNilai.setVisibility(View.GONE);
		ganti_soal.start();tdk_terjwb.setVisibility(View.GONE);
		backsound = MediaPlayer.create(this, R.raw.backsound_game_play);
		backsound.setVolume(50, 50);
		backsound.start();
		jawab1 = (Button) findViewById(R.id.game_4jawab1);
		jawab2 = (Button) findViewById(R.id.game_4jawab2);
		jawab3 = (Button) findViewById(R.id.game_4jawab3);
		jawab4 = (Button) findViewById(R.id.game_4jawab4);
		txWaktu = (TextView) findViewById(R.id.game_4wadah_waktu);
		txkategori = (TextView) findViewById(R.id.game_4_judul_kategori);
		txPertanyaan = (TextView) findViewById(R.id.game_4txSoal);
		wadah_soal = findViewById(R.id.game_4wadah_Soal);
		image=(ImageView)findViewById(R.id.game_4gbrSoal);
		Pancingan = (TextView) findViewById(R.id.game_4pancingan);
		Pancingan.setAnimation(AnimationUtils.loadAnimation(getBaseContext(),
				R.anim.animasi_btn_benar));
		animasi_komponen_lev4();
		backsound.setOnCompletionListener(new OnCompletionListener() {
			public void onCompletion(MediaPlayer mp) {
				backsound.start();
			}
		});
	}

	public void jawaban_lev4(View v) {
		if (v == findViewById(R.id.game_4jawab1)) {
			if (Arrays.asList(isi_game.jawaban_benar_lev4).contains(jawab1.getText().toString())) {
				jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.game_4jawab2)){
			if (Arrays.asList(isi_game.jawaban_benar_lev4).contains(jawab2.getText().toString())) {
				jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.game_4jawab3)){
			if (Arrays.asList(isi_game.jawaban_benar_lev4).contains(jawab3.getText().toString())) {
				jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}else if(v==findViewById(R.id.game_4jawab4)){
			if (Arrays.asList(isi_game.jawaban_benar_lev4).contains(jawab4.getText().toString())) {
				jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_benar));
				nilai_benar += 10;btn_benar.start();benar++;
			} else {
				jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this, R.anim.animasi_btn_salah));
				btn_salah.start();nilai_salah++;
			}
		}
		NonAktifkan_Tombol_lev4();timer.cancel();
		keputusannya.start();backsound.pause();
	}

	public void animasi_komponen_lev4() {
		jawab1 = (Button) findViewById(R.id.game_4jawab1);
		jawab2 = (Button) findViewById(R.id.game_4jawab2);
		jawab3 = (Button) findViewById(R.id.game_4jawab3);
		jawab4 = (Button) findViewById(R.id.game_4jawab4);
		txWaktu = (TextView) findViewById(R.id.game_4wadah_waktu);
		wadah_soal = findViewById(R.id.game_4wadah_Soal);
		wadah_soal.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_soal));
		txWaktu.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_soal));
		jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_btn_kiri));
		jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_btn_kiri));
		jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_btn_kanan));
		jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
				R.anim.animasi_btn_kanan));
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@SuppressLint("NewApi")
	public class Penghitung_lev4 extends CountDownTimer {
		public Penghitung_lev4(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}
		public void onFinish() {
			timer.cancel();ganti_pertanyaan_lev4();timer.start();
			tidak_terjawab++;
		}
		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@SuppressLint("NewApi")
		@Override
		public void onTick(long millisUntilFinished) {
			txWaktu = (TextView) findViewById(R.id.game_4wadah_waktu);
			long milis = millisUntilFinished;
			String hms = String.format(
					"%02d",
					TimeUnit.MILLISECONDS.toSeconds(milis)
							- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
									.toMinutes(milis)));
			txWaktu.setText(hms);
		}
	}

	public void pertanyaan1_lev4() {
		txkategori = (TextView) findViewById(R.id.game_4_judul_kategori);
		txPertanyaan = (TextView) findViewById(R.id.game_4txSoal);
		jawab1 = (Button) findViewById(R.id.game_4jawab1);
		jawab2 = (Button) findViewById(R.id.game_4jawab2);
		jawab3 = (Button) findViewById(R.id.game_4jawab3);
		jawab4 = (Button) findViewById(R.id.game_4jawab4);
		image=(ImageView)findViewById(R.id.game_4gbrSoal);
		txkategori.setText(isi_game.kategori_lev4[urut]);
		txPertanyaan.setText(isi_game.pertanyaan_lev4[urut]);
		jawab1.setText(isi_game.opsi_lev4[urut]);
		jawab2.setText(isi_game.opsi_lev4[1]);
		jawab3.setText(isi_game.opsi_lev4[2]);
		jawab4.setText(isi_game.opsi_lev4[3]);
		image.setBackgroundResource(isi_game.gbr_pertanyaan_lev4[urut]);
	}

	public void ganti_pertanyaan_lev4() {
		if (txPertanyaan.getText().toString().equals(isi_game.pertanyaan_lev4[urut])) {
		}
		urut++;
		if (urut < isi_game.pertanyaan_lev4.length) {
			txkategori.setText(isi_game.kategori_lev4[urut]);
			txPertanyaan.setText(isi_game.pertanyaan_lev4[urut]);
			jawab1.setText(isi_game.opsi_lev4[(urut * 4)]);
			jawab2.setText(isi_game.opsi_lev4[(urut * 4) + 1]);
			jawab3.setText(isi_game.opsi_lev4[(urut * 4) + 2]);
			jawab4.setText(isi_game.opsi_lev4[(urut * 4) + 3]);
			image.setBackgroundResource(isi_game.gbr_pertanyaan_lev4[urut]);
			wadah_soal.setAnimation(AnimationUtils.loadAnimation(
					Game_Play_lev4.this, R.anim.animasi_soal));
			txWaktu.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
					R.anim.animasi_soal));
			jawab1.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
					R.anim.animasi_btn_kiri));
			jawab2.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
					R.anim.animasi_btn_kiri));
			jawab3.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
					R.anim.animasi_btn_kanan));
			jawab4.setAnimation(AnimationUtils.loadAnimation(Game_Play_lev4.this,
					R.anim.animasi_btn_kanan));
			Aktifkan_Tombol_lev4();
			timer.start();
			ganti_soal.start();
			TextView txNilai = (TextView) findViewById(R.id.game_4nilaiNilai),txNilaiBenar = (TextView) findViewById(R.id.game_4_tx_Benar),
					txSalah = (TextView) findViewById(R.id.game_4tx_Salah);
			txNilai.setText(""+nilai_benar);txNilaiBenar.setText("Benar : "+benar);
			txSalah.setText("Salah : "+nilai_salah);
			backsound.setVolume(50, 50);backsound.start();
		} else {
			keputusannya.cancel();
			timer.cancel();
			startActivity(new Intent(Game_Play_lev4.this, Game_Nilai_lev4.class));
			overridePendingTransition(R.animator.animation1, R.animator.animation2);
			data_nilai_lev4();
			if (backsound.isPlaying()) {
				backsound.pause();
			}
		}
	}

	public class keputusan_lev4 extends CountDownTimer {
		public keputusan_lev4(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
		}
		public void onFinish() {
			timer.cancel();
			ganti_pertanyaan_lev4();
		}
		public void onTick(long millisUntilFinished) {
		}
	}

	public void NonAktifkan_Tombol_lev4() {
		jawab1.setClickable(false);
		jawab2.setClickable(false);
		jawab3.setClickable(false);
		jawab4.setClickable(false);
	}

	public void Aktifkan_Tombol_lev4() {
		jawab1.setClickable(true);
		jawab2.setClickable(true);
		jawab3.setClickable(true);
		jawab4.setClickable(true);
	}
	public void onBackPressed(){
		AlertDialog.Builder ab = new AlertDialog.Builder(this);
		timer.cancel();
		ab.setCancelable(false).setTitle("Akhiri Permainan ?")
		.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				backsound.pause();
				timer.cancel();
				startActivity(new Intent(Game_Play_lev4.this, Game_Level.class));
			}
			}).setNegativeButton("Tidak",new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog,int which) {
					dialog.cancel();
					timer.start();}
				}).show();
	}
	public void font(){
		jawab1 = (Button) findViewById(R.id.game_4jawab1);jawab2 = (Button) findViewById(R.id.game_4jawab2);
		jawab3 = (Button) findViewById(R.id.game_4jawab3);jawab4 = (Button) findViewById(R.id.game_4jawab4);
		txWaktu = (TextView) findViewById(R.id.game_4wadah_waktu);txkategori = (TextView) findViewById(R.id.game_4_judul_kategori);
		txPertanyaan = (TextView) findViewById(R.id.game_4txSoal);
		TextView txNilaiBenar = (TextView) findViewById(R.id.game_4_tx_Benar),
				txSalah = (TextView) findViewById(R.id.game_4tx_Salah);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		jawab1.setTypeface(face);jawab2.setTypeface(face);jawab3.setTypeface(face);jawab4.setTypeface(face);
		txWaktu.setTypeface(face);txPertanyaan.setTypeface(face);txWaktu.setTypeface(face);
		txkategori.setTypeface(face);txNilaiBenar.setTypeface(face);txSalah.setTypeface(face);
	}
	public void data_nilai_lev4(){
		TextView txNilai = (TextView) findViewById(R.id.game_4nilaiNilai),
				 txSalah = (TextView) findViewById(R.id.game_4tx_Salah),
			     txBenar = (TextView) findViewById(R.id.game_4_tx_Benar),
			     tdk_terjwb = (TextView) findViewById(R.id.game_4_tidak_terjawab);
		txBenar.setText("Benar : "+benar);
		txSalah.setText("Salah : "+nilai_salah);
		txNilai.setText(""+nilai_benar);
		tdk_terjwb.setText("Tidak Terjawab : "+tidak_terjawab);
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		Editor edit=sp.edit();
		edit.putString("level4", txNilai.getText().toString());
		edit.putString("salah_level4", txSalah.getText().toString());
		edit.putString("tdk_terjawab", tdk_terjwb.getText().toString());
		edit.putString("benar_level4", txBenar.getText().toString());
		edit.commit();
	}
}
