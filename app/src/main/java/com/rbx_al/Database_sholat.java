package com.rbx_al;

public class Database_sholat {
	//Bacaan Sholat Subuh
	String[] judul_subuh={
			"Membaca Niat Sholat Subuh",
			"Membaca Takbiratul Ihram",
			"Membaca Do'a Iftitah",
			"Membaca Al-Fatikhah",
			"Membaca Surat Pendek",
			"Membaca Ruku",
			"Membaca I'tidal",
			"Bacaan Sujud",
			"Bacaan Duduk Diantara 2 Sujud",
			"Membaca Do'a Qunut",
			"Membaca Tasyahud Akhir",
			"Membaca Salam"
	};
	
	int[] audio_subuh={
			R.raw.sholat_niat,
			R.raw.takbir,
			R.raw.sholat_iftitah,
			R.raw.sholat_fatihah,
			R.raw.sholat_al_ikhlas,
			R.raw.sholat_rukuk,
			R.raw.sholat_iktidal,
			R.raw.sujud,
			R.raw.sholat_duduk2sujud,
			R.raw.qunut,
			R.raw.tsyhud_akhir,
			R.raw.sholat_salam
	};
	
	String[] bacaan_subuh={
			"اُصَلِّيْ فَرْضَ الصُّبْحِ رَكْعَتَيْنِ مُسْتَقْبِلاْلقِبْلَةِ اَدَاءً لِلَّهِ تَعَالَىَ",
			"الله اكبر",
			"إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمَوَاتِ وَالأَرْضَ حَنِيْفًا مُسْلِمًا وَمَا أَنَا مِنَ المُشْرِكِيْن . إِنَّ صَلاَتِي وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ العَالَمِيْن لاَ شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ المُسْلِمِيًن",
			"بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّحِيمِ\nالْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ\nالرَّ حْمَنِ الرَّحِيمِ\nمَالِكِ يَوْمِ الدِّينِ\nإِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ\nاهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ\nصِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ",
			"إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ\nفَصَلِّ لِرَبِّكَ وَانْحَ\nإِنَّ شَانِئَكَ هُوَ الأبْتَرُ",
			"سُبْحَانَ رَبِّيَ الْعَظِيْمِ وَبِحَمْدِهِ",
			"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ",
			"سُبْحَانَ رَبِّيَ اْلأَعْلَى وَبِحَمْدِهِ",
			"رَبِ ّاِغْفِرْلِيِ وَارْحَمْنِيْ وَارْفَعْنِيْ وَاجْبُرْنِيْ وَارْزُقْنِيْ وَاهْدِنِيْ وَعَاِفِنيْ وَاعْفُ عَنِّيْ",
			"اَللّهُمَّ اهْدِنِىْ فِيْمَنْ هَدَيْتَ\nوَعَافِنِى فِيْمَنْ عَافَيْتَ\nوَتَوَلَّنِىْ فِيْمَنْ تَوَلَّيْتَ\nوَبَارِكْ لِىْ فِيْمَا اَعْطَيْتَ\nوَقِنِيْ شَرَّمَا قََضَيْتَ\nفَاِ نَّكَ تَقْضِىْ وَلاَ يُقْضَى عَلَيْكَ\nوَاِ نَّهُ لاَ يَذِلُّ مَنْ وَالَيْتَ\nوَلاَ يَعِزُّ مَنْ عَادَيْتَ\nتَبَارَكْتَ رَبَّنَا وَتَعَالَيْتَ\nفَلَكَ الْحَمْدُ عَلَى مَا قَضَيْتَ\nوَاَسْتَغْفِرُكَ وَاَتُوْبُ اِلَيْكَ\nوَصَلَّى اللهُ عَلَى سَيِّدَنَا مُحَمَّدٍ النَّبِيِّ اْلاُمِّيِّ وَعَلَى آلِهِ وَصَحْبِهِ وَسَلَّمَ",
			"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد وعلى آلِ  مُحَمَّد كَمَا صَلَّبْتَ عَلَى  إِبْرَاهِيْمَ وَعَلَى آلِ   إِبْرَاهِيْمَ وَبَارِكْ عَلَى  مُحَمَّدٍ وَعَلَى آلِ   مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى   إِبْرَاهِيْمَ وَعَلَى آلِ  إِبْرَاهِيْمَ فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد",
			"اَلسَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ الله"
	};
	String[] cara_baca_subuh={
		"Usholli Fardlosh shubhi rok'ataini mustaqbilal qiblati adaa-an lillaahi ta'aala.",
		"Allaahu akbar.",
		"Innii wajjahtu wajhiya lilladzii fatharas samaawaati wal-ardha, haniifam muslimaw wamaa ana minal musyrikiin. Inna shalaati wanusukii wamahyaaya wamamaatii lillaahi rabbil ‘alaamiina. Laa syariika lahu wabidzaalika umirtu wa ana minal muslimiina.",
		"bismillaahirrahmaanirrahiim\nalhamdulillaahirabbil’aalamiin\narrahmaanirrahiim\nmaalikiyawmiddiin\niyyaakana’buduwa-iyyaakanasta’iin\n" +
		"ihdinaashshiraathalmustaqiim\nshiraathalladziinaan’amta’alayhim ghayrilmaghdhuubi’alayhim walaadhdhaalliin",
		"Innaa a'thainaakal kautsar\nFashalli lirabbika waanhar\nInna syaani-aka huwal abtar",
		"Subhaana rabbiyal 'azhiimi wabihamdihi",
		"Sami'allaahu liman hamidah, rabbanaa walakal hamdu.",
		"Subhaana rabbiyal a'laa wabihamdih",
		"Rabbighfirlii warhamnii warfa'nii wajburnii warzuqnii wahdinii wa 'aafinii wa'fu 'annii",
		"Allah hummah dinii fiiman hadait.\nWa'aa finii fiiman 'aafait.\nWatawallanii fiiman tawal-laiit.\nWabaariklii fiimaa a'thait.\n" +
		"Waqinii syarramaa qadhait.\nFainnaka taqdhii walaa yuqdha 'alaik.\nWainnahu laayadzilu man walait.\nWalaa ya'izzu man 'aadait.\n" +
		"Tabaa rakta rabbanaa wata'aalait.\nFalakalhamdu 'alaa maaqadhait.\nAstaghfiruka wa'atuubu ilaik.\nWasallallahu 'ala Sayyidina Muhammadin nabiyyil ummiyyi. Wa'alaa aalihi washahbihi Wasallam.",
		"Attahiyyaatul mubaarakaatush shalawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma shalli ‘alaa Muhammad wa 'alaa aali Muhammad, kamaa shallaita 'alaa Ibraahim, wa 'alaa aali Ibraahim. Wabaarik ‘alaa Muhammad, wa 'alaa aali Muhammad, kamaa baarakta 'alaa Ibraahim, wa 'alaa aali Ibraahim. Fil 'aalamiina innaka hamiidum majiid.",
		"Assalaamu 'alaikum warahmatullaah."
	};
	String[] arti_subuh={
		"Aku niat melakukan shalat fardu subuh 2 rakaat, sambil menghadap qiblat, saat ini, karena Allah ta'ala.",
		"Allah Maha Besar.",
		"Sesungguhnya aku menghadapkan mukaku kepada Dzat yang menciptakan langit dan bumi dengan keadaan lurus dan berserah diri, dan bukannya aku termasuk dalam golongan musyrik. Sesungguhnya sembahyangku, ibadatku, hidupku dan matiku hanya untuk Allah semesta alam. Tiada sekutu bagi-Nya, karena itu aku rela diperintah dan aku ini adalah golongan orang Islam.",
		"Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.\nSegala puji bagi Allah, Tuhan semesta alam.\nYang Maha Pemurah lagi Maha Penyayang\n" +
		"Yang menguasai di Hari Pembalasan\nHanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan.\nTunjukilah kami jalan yang lurus,\n" +
		"(yaitu) Jalan orang-orang yang telah Engkau beri ni’mat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.",
		"Sesungguhnya Kami telah memberikan kepadamu, nikmat yang banyak.\nMaka dirikanlah shalat, karena Rabb-mu; dan berkorbanlah.\nSesungguhnya orang-orang yang membenci kamu, dialah yang terputus.",
		"Maha Suci Tuhan Yang Maha Besar lagi Maha Terpuji.",
		"Allah mendengar akan sesiapa yang memuji-Nya. Hai Tuhan kami, kepada Engkaulah segala pujian.",
		"Maha Suci Tuhan Yang Maha Tinggi lagi Maha Terpuji",
		"Ya Allah ! ampunilah dosaku, belas kasihanilah aku, dan angkatlah darjatku dan cukuplah segala kekuranganku dan berilah rezeki kepadaku, dan berilah aku petunjuk dan sejahterakanlah aku dan berilah keampunan padaku.",
		"Ya Allah tunjukkanlah akan daku sebagaiman mereka yang telah Engkau tunjukkan\nDan berilah kesihatan kepadaku sebagaimana mereka yang Engkau telah berikan kesihatan\n" +
		"Dan peliharalah daku sebagaimana orang yang telah Engkau peliharakan\nDan berilah keberkatan bagiku pada apa-apa yang telah Engkau kurniakan\n" +
		"Dan selamatkan aku dari bahaya kejahatan yang Engkau telah tentukan\nMaka sesungguhnya Engkaulah yang menghukum dan bukan kena hukum\n" +
		"Maka sesungguhnya tidak hina orang yang Engkau pimpin\nDan tidak mulia orang yang Engkau memusuhinya\nMaha Suci Engkau wahai Tuhan kami dan Maha tinggi Engkau\n" +
		"Maha bagi Engkau segala pujian di atas yang Engkau hukumkan\nKu memohon ampun dari Engkau dan aku bertaubat kepada Engkau\n(Dan semoga Allah) mencurahkan rahmat dan sejahtera ke atas junjungan kami Nabi Muhammad, keluarga dan sahabatnya.",
		"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Aku bersaksi bahwa tiada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah. Ya Allah! Limpahkanlah rahmat kepada Nabi Muhammad. “ Sebagimana pernah Engkau beri rahmat kepada Nabi Ibrahim dan keluarganya. Dan limpahilah berkah atas Nabi Muhammad beserta para keluarganya. Sebagaimana Engkau memberi berkah kepada Nabi Ibrahim dan keluarganya.",
		"Keselamatan dan rahmat buat Anda sekalian."
	};
	int[] gambar_subuh={
		R.drawable.sholat1_niat,
		R.drawable.sholat2_takbir,
		R.drawable.sholat3_baca,
		R.drawable.sholat3_baca,
		R.drawable.sholat3_baca,
		R.drawable.sholat4_ruku,
		R.drawable.sholat5_iktidal,
		R.drawable.sholat6_sujud,
		R.drawable.sholat7_duduk,
		R.drawable.sholat1_niat,
		R.drawable.sholat9_akhir,
		R.drawable.sholat10_salam,
	};
	
	//Bacaan Sholat Dhuhur
	String[] judul_dhuhur={
			"Membaca Niat Sholat Dhuhur.",
			"Membaca Takbiratul Ihram.",
			"Membaca Do'a Iftitah.",
			"Membaca Al-Fatikhah.",
			"Membaca Surat Pendek.",
			"Membaca Ruku.",
			"Membaca I'tidal.",
			"Bacaan Sujud.",
			"Bacaan Duduk Diantara 2 Sujud.",
			"Membaca Tasyahud Awal.",
			"Membaca Tasyahud Akhir.",
			"Membaca Salam."
	};
	
	int[] audio_dhuhur={
			R.raw.sholat_niat,
			R.raw.takbir,
			R.raw.sholat_iftitah,
			R.raw.sholat_fatihah,
			R.raw.sholat_al_ikhlas,
			R.raw.sholat_rukuk,
			R.raw.sholat_iktidal,
			R.raw.sujud,
			R.raw.sholat_duduk2sujud,
			R.raw.tsyhud_awal,
			R.raw.tsyhud_akhir,
			R.raw.sholat_salam
	};
	
	String[] bacaan_dhuhur={
			"اُصَلّى فَرْضَ الظُّهْرِاَرْبَعَ رَكَعَاتٍ مُسْتَقْبِلَ الْقِبْلَةِ اَدَاءً مَأْمُوْمًا ِللهِ تَعَالَى",
			"الله اكبر",
			"إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمَوَاتِ وَالأَرْضَ حَنِيْفًا مُسْلِمًا وَمَا أَنَا مِنَ المُشْرِكِيْن . إِنَّ صَلاَتِي وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ العَالَمِيْن لاَ شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ المُسْلِمِيًن",
			"بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّحِيمِ\nالْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ\nالرَّ حْمَنِ الرَّحِيمِ\nمَالِكِ يَوْمِ الدِّينِ\nإِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ\nاهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ\nصِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ",
			"إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ\nفَصَلِّ لِرَبِّكَ وَانْحَ\nإِنَّ شَانِئَكَ هُوَ الأبْتَرُ",
			"سُبْحَانَ رَبِّيَ الْعَظِيْمِ وَبِحَمْدِهِ",
			"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ",
			"سُبْحَانَ رَبِّيَ اْلأَعْلَى وَبِحَمْدِهِ",
			"رَبِ ّاِغْفِرْلِيِ وَارْحَمْنِيْ وَارْفَعْنِيْ وَاجْبُرْنِيْ وَارْزُقْنِيْ وَاهْدِنِيْ وَعَاِفِنيْ وَاعْفُ عَنِّيْ",
			"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد",
			"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد وعلى آلِ  مُحَمَّد كَمَا صَلَّبْتَ عَلَى  إِبْرَاهِيْمَ وَعَلَى آلِ   إِبْرَاهِيْمَ وَبَارِكْ عَلَى  مُحَمَّدٍ وَعَلَى آلِ   مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى   إِبْرَاهِيْمَ وَعَلَى آلِ  إِبْرَاهِيْمَ فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد",
			"اَلسَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ الله"
	};
	String[] cara_baca_dhuhur={
		"Usholli Fardlo dhuhri arba'a roka'atim mustaqbilal qiblati adaa-an lillaahi ta'aala.",
		"Allaahu akbar.",
		"Innii wajjahtu wajhiya lilladzii fatharas samaawaati wal-ardha, haniifam muslimaw wamaa ana minal musyrikiin. Inna shalaati wanusukii wamahyaaya wamamaatii lillaahi rabbil ‘alaamiina. Laa syariika lahu wabidzaalika umirtu wa ana minal muslimiina.",
		"bismillaahirrahmaanirrahiim\nalhamdulillaahirabbil’aalamiin\narrahmaanirrahiim\nmaalikiyawmiddiin\niyyaakana’buduwa-iyyaakanasta’iin\n" +
				"ihdinaashshiraathalmustaqiim\nshiraathalladziinaan’amta’alayhim ghayrilmaghdhuubi’alayhim walaadhdhaalliin",
				"Innaa a'thainaakal kautsar\nFashalli lirabbika waanhar\nInna syaani-aka huwal abtar",
		"Subhaana rabbiyal 'azhiimi wabihamdihi",
		"Sami'allaahu liman hamidah, rabbanaa walakal hamdu.",
		"Subhaana rabbiyal a'laa wabihamdih",
		"Rabbighfirlii warhamnii warfa'nii wajburnii warzuqnii wahdinii wa 'aafinii wa'fu 'annii",
		"Attahiyyaatul mubaarakaatush sholawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma sholli ‘alaa   Muhammad.",
		"Attahiyyaatul mubaarakaatush shalawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma shalli ‘alaa Muhammad wa 'alaa aali Muhammad, kamaa shallaita 'alaa Ibraahim, wa 'alaa aali Ibraahim. Wabaarik ‘alaa Muhammad, wa 'alaa aali Muhammad, kamaa baarakta 'alaa Ibraahim, wa 'alaa aali Ibraahim. Fil 'aalamiina innaka hamiidum majiid.",
		"Assalaamu 'alaikum warahmatullaah."
	};
	String[] arti_dhuhur={
		"Aku niat melakukan shalat fardu dhuhur 4 rakaat, sambil menghadap qiblat, saat ini, karena Allah ta'ala.",
		"Allah Maha Besar.",
		"Sesungguhnya aku menghadapkan mukaku kepada Dzat yang menciptakan langit dan bumi dengan keadaan lurus dan berserah diri, dan bukannya aku termasuk dalam golongan musyrik. Sesungguhnya sembahyangku, ibadatku, hidupku dan matiku hanya untuk Allah semesta alam. Tiada sekutu bagi-Nya, karena itu aku rela diperintah dan aku ini adalah golongan orang Islam.",
		"Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.\nSegala puji bagi Allah, Tuhan semesta alam.\nYang Maha Pemurah lagi Maha Penyayang\n" +
				"Yang menguasai di Hari Pembalasan\nHanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan.\nTunjukilah kami jalan yang lurus,\n" +
				"(yaitu) Jalan orang-orang yang telah Engkau beri ni’mat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.",
				"Sesungguhnya Kami telah memberikan kepadamu, nikmat yang banyak.\nMaka dirikanlah shalat, karena Rabb-mu; dan berkorbanlah.\nSesungguhnya orang-orang yang membenci kamu, dialah yang terputus.",
		"Maha Suci Tuhan Yang Maha Besar lagi Maha Terpuji.",
		"Allah mendengar akan sesiapa yang memuji-Nya. Hai Tuhan kami, kepada Engkaulah segala pujian.",
		"Maha Suci Tuhan Yang Maha Tinggi lagi Maha Terpuji",
		"Ya Allah ! ampunilah dosaku, belas kasihanilah aku, dan angkatlah darjatku dan cukuplah segala kekuranganku dan berilah rezeki kepadaku, dan berilah aku petunjuk dan sejahterakanlah aku dan berilah keampunan padaku.",
		"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Ya Allah aku bersumpah dan berjanji bahwa tiada ada Tuhan yang berhak disembah kecuali Engkau ya Allah, dan aku bersumpah dan berjanji sesungguhnya Nabi Muhammad adalah utusan-Mu Ya Allah. Ya Allah, limpahkan shalawat-Mu kepada Nabi Muhammad.",
		"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Aku bersaksi bahwa tiada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah. Ya Allah! Limpahkanlah rahmat kepada Nabi Muhammad. “ Sebagimana pernah Engkau beri rahmat kepada Nabi Ibrahim dan keluarganya. Dan limpahilah berkah atas Nabi Muhammad beserta para keluarganya. Sebagaimana Engkau memberi berkah kepada Nabi Ibrahim dan keluarganya.",
		"Keselamatan dan rahmat buat Anda sekalian."
	};
	int[] gambar_dhuhur={
		R.drawable.sholat1_niat,
		R.drawable.sholat2_takbir,
		R.drawable.sholat3_baca,
		R.drawable.sholat3_baca,
		R.drawable.sholat3_baca,
		R.drawable.sholat4_ruku,
		R.drawable.sholat5_iktidal,
		R.drawable.sholat6_sujud,
		R.drawable.sholat7_duduk,
		R.drawable.sholat8_awal,
		R.drawable.sholat9_akhir,
		R.drawable.sholat10_salam,
	};
	
	//Bacaan Sholat Azhar
		String[] judul_azhar={
				"Membaca Niat Sholat Azhar.   ",
				"Membaca Takbiratul Ihram.   ",
				"Membaca Do'a Iftitah.   ",
				"Membaca Al-Fatikhah.   ",
				"Membaca Surat Pendek.   ",
				"Membaca Ruku.   ",
				"Membaca I'tidal.   ",
				"Bacaan Sujud.   ",
				"Bacaan Duduk Diantara 2 Sujud.   ",
				"Membaca Tasyahud Awal.   ",
				"Membaca Tasyahud Akhir.   ",
				"Membaca Salam.   "
		};
		
		int[] audio_azhar={
				R.raw.sholat_niat,
				R.raw.takbir,
				R.raw.sholat_iftitah,
				R.raw.sholat_fatihah,
				R.raw.sholat_al_ikhlas,
				R.raw.sholat_rukuk,
				R.raw.sholat_iktidal,
				R.raw.sujud,
				R.raw.sholat_duduk2sujud,
				R.raw.tsyhud_awal,
				R.raw.tsyhud_akhir,
				R.raw.sholat_salam
		};
		
		String[] bacaan_azhar={
				"اُصَلّى فَرْضَ الْعَصْرِاَرْبَعَ رَكَعَاتٍ مُسْتَقْبِلَ الْقِبْلَةِ اَدَاءً مَأْمُوْمًا ِللهِ تَعَالَى",
				"الله اكبر",
				"إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمَوَاتِ وَالأَرْضَ حَنِيْفًا مُسْلِمًا وَمَا أَنَا مِنَ المُشْرِكِيْن . إِنَّ صَلاَتِي وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ العَالَمِيْن لاَ شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ المُسْلِمِيًن",
				"بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّحِيمِ\nالْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ\nالرَّ حْمَنِ الرَّحِيمِ\nمَالِكِ يَوْمِ الدِّينِ\nإِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ\nاهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ\nصِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ",
				"إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ\nفَصَلِّ لِرَبِّكَ وَانْحَ\nإِنَّ شَانِئَكَ هُوَ الأبْتَرُ",
				"سُبْحَانَ رَبِّيَ الْعَظِيْمِ وَبِحَمْدِهِ",
				"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ",
				"سُبْحَانَ رَبِّيَ اْلأَعْلَى وَبِحَمْدِهِ",
				"رَبِ ّاِغْفِرْلِيِ وَارْحَمْنِيْ وَارْفَعْنِيْ وَاجْبُرْنِيْ وَارْزُقْنِيْ وَاهْدِنِيْ وَعَاِفِنيْ وَاعْفُ عَنِّيْ",
				"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد",
				"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد وعلى آلِ  مُحَمَّد كَمَا صَلَّبْتَ عَلَى  إِبْرَاهِيْمَ وَعَلَى آلِ   إِبْرَاهِيْمَ وَبَارِكْ عَلَى  مُحَمَّدٍ وَعَلَى آلِ   مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى   إِبْرَاهِيْمَ وَعَلَى آلِ  إِبْرَاهِيْمَ فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد",
				"اَلسَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ الله"
		};
		String[] cara_baca_azhar={
			"Usholli Fardlol azhri roka'atim mustaqbilal qiblati adaa-an lillaahi ta'aala.",
			"Allaahu akbar.",
			"Innii wajjahtu wajhiya lilladzii fatharas samaawaati wal-ardha, haniifam muslimaw wamaa ana minal musyrikiin. Inna shalaati wanusukii wamahyaaya wamamaatii lillaahi rabbil ‘alaamiina. Laa syariika lahu wabidzaalika umirtu wa ana minal muslimiina.",
			"bismillaahirrahmaanirrahiim\nalhamdulillaahirabbil’aalamiin\narrahmaanirrahiim\nmaalikiyawmiddiin\niyyaakana’buduwa-iyyaakanasta’iin\n" +
					"ihdinaashshiraathalmustaqiim\nshiraathalladziinaan’amta’alayhim ghayrilmaghdhuubi’alayhim walaadhdhaalliin",
					"Innaa a'thainaakal kautsar\nFashalli lirabbika waanhar\nInna syaani-aka huwal abtar",
			"Subhaana rabbiyal 'azhiimi wabihamdihi",
			"Sami'allaahu liman hamidah, rabbanaa walakal hamdu.",
			"Subhaana rabbiyal a'laa wabihamdih",
			"Rabbighfirlii warhamnii warfa'nii wajburnii warzuqnii wahdinii wa 'aafinii wa'fu 'annii",
			"Attahiyyaatul mubaarakaatush sholawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma sholli ‘alaa   Muhammad.",
			"Attahiyyaatul mubaarakaatush shalawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma shalli ‘alaa Muhammad wa 'alaa aali Muhammad, kamaa shallaita 'alaa Ibraahim, wa 'alaa aali Ibraahim. Wabaarik ‘alaa Muhammad, wa 'alaa aali Muhammad, kamaa baarakta 'alaa Ibraahim, wa 'alaa aali Ibraahim. Fil 'aalamiina innaka hamiidum majiid.",
			"Assalaamu 'alaikum warahmatullaah."
		};
		String[] arti_azhar={
			"Aku niat melakukan shalat fardu azhar 4 rakaat, sambil menghadap qiblat, saat ini, karena Allah ta'ala.",
			"Allah Maha Besar.",
			"Sesungguhnya aku menghadapkan mukaku kepada Dzat yang menciptakan langit dan bumi dengan keadaan lurus dan berserah diri, dan bukannya aku termasuk dalam golongan musyrik. Sesungguhnya sembahyangku, ibadatku, hidupku dan matiku hanya untuk Allah semesta alam. Tiada sekutu bagi-Nya, karena itu aku rela diperintah dan aku ini adalah golongan orang Islam.",
			"Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.\nSegala puji bagi Allah, Tuhan semesta alam.\nYang Maha Pemurah lagi Maha Penyayang\n" +
					"Yang menguasai di Hari Pembalasan\nHanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan.\nTunjukilah kami jalan yang lurus,\n" +
					"(yaitu) Jalan orang-orang yang telah Engkau beri ni’mat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.",
					"Sesungguhnya Kami telah memberikan kepadamu, nikmat yang banyak.\nMaka dirikanlah shalat, karena Rabb-mu; dan berkorbanlah.\nSesungguhnya orang-orang yang membenci kamu, dialah yang terputus.",
			"Maha Suci Tuhan Yang Maha Besar lagi Maha Terpuji.",
			"Allah mendengar akan sesiapa yang memuji-Nya. Hai Tuhan kami, kepada Engkaulah segala pujian.",
			"Maha Suci Tuhan Yang Maha Tinggi lagi Maha Terpuji",
			"Ya Allah ! ampunilah dosaku, belas kasihanilah aku, dan angkatlah darjatku dan cukuplah segala kekuranganku dan berilah rezeki kepadaku, dan berilah aku petunjuk dan sejahterakanlah aku dan berilah keampunan padaku.",
			"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Ya Allah aku bersumpah dan berjanji bahwa tiada ada Tuhan yang berhak disembah kecuali Engkau ya Allah, dan aku bersumpah dan berjanji sesungguhnya Nabi Muhammad adalah utusan-Mu Ya Allah. Ya Allah, limpahkan shalawat-Mu kepada Nabi Muhammad.",
			"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Aku bersaksi bahwa tiada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah. Ya Allah! Limpahkanlah rahmat kepada Nabi Muhammad. “ Sebagimana pernah Engkau beri rahmat kepada Nabi Ibrahim dan keluarganya. Dan limpahilah berkah atas Nabi Muhammad beserta para keluarganya. Sebagaimana Engkau memberi berkah kepada Nabi Ibrahim dan keluarganya.",
			"Keselamatan dan rahmat buat Anda sekalian."
		};
		int[] gambar_azhar={
			R.drawable.sholat1_niat,
			R.drawable.sholat2_takbir,
			R.drawable.sholat3_baca,
			R.drawable.sholat3_baca,
			R.drawable.sholat3_baca,
			R.drawable.sholat4_ruku,
			R.drawable.sholat5_iktidal,
			R.drawable.sholat6_sujud,
			R.drawable.sholat7_duduk,
			R.drawable.sholat8_awal,
			R.drawable.sholat9_akhir,
			R.drawable.sholat10_salam,
		};
	
		//Bacaan Sholat Maghrib
				String[] judul_maghrib={
						"Membaca Niat Sholat Maghrib. ",
						"Membaca Takbiratul Ihram. ",
						"Membaca Do'a Iftitah. ",
						"Membaca Al-Fatikhah. ",
						"Membaca Surat Pendek. ",
						"Membaca Ruku. ",
						"Membaca I'tidal. ",
						"Bacaan Sujud. ",
						"Bacaan Duduk Diantara 2 Sujud. ",
						"Membaca Tasyahud Awal. ",
						"Membaca Tasyahud Akhir. ",
						"Membaca Salam. "
				};
				
				int[] audio_maghrib={
						R.raw.sholat_niat,
						R.raw.takbir,
						R.raw.sholat_iftitah,
						R.raw.sholat_fatihah,
						R.raw.sholat_al_ikhlas,
						R.raw.sholat_rukuk,
						R.raw.sholat_iktidal,
						R.raw.sujud,
						R.raw.sholat_duduk2sujud,
						R.raw.tsyhud_awal,
						R.raw.tsyhud_akhir,
						R.raw.sholat_salam
				};
				
				String[] bacaan_maghrib={
						"اُصَلّى فَرْضَ الْمَغْرِبِ ثَلاَثَ رَكَعَاتٍ مُسْتَقْبِلَ الْقِبْلَةِ اَدَاءً مَأْمُوْمًا ِللهِ تَعَالَى",
						"الله اكبر",
						"إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمَوَاتِ وَالأَرْضَ حَنِيْفًا مُسْلِمًا وَمَا أَنَا مِنَ المُشْرِكِيْن . إِنَّ صَلاَتِي وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ العَالَمِيْن لاَ شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ المُسْلِمِيًن",
						"بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّحِيمِ\nالْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ\nالرَّ حْمَنِ الرَّحِيمِ\nمَالِكِ يَوْمِ الدِّينِ\nإِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ\nاهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ\nصِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ",
						"إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ\nفَصَلِّ لِرَبِّكَ وَانْحَ\nإِنَّ شَانِئَكَ هُوَ الأبْتَرُ",
						"سُبْحَانَ رَبِّيَ الْعَظِيْمِ وَبِحَمْدِهِ",
						"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ",
						"سُبْحَانَ رَبِّيَ اْلأَعْلَى وَبِحَمْدِهِ",
						"رَبِ ّاِغْفِرْلِيِ وَارْحَمْنِيْ وَارْفَعْنِيْ وَاجْبُرْنِيْ وَارْزُقْنِيْ وَاهْدِنِيْ وَعَاِفِنيْ وَاعْفُ عَنِّيْ",
						"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد",
						"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد وعلى آلِ  مُحَمَّد كَمَا صَلَّبْتَ عَلَى  إِبْرَاهِيْمَ وَعَلَى آلِ   إِبْرَاهِيْمَ وَبَارِكْ عَلَى  مُحَمَّدٍ وَعَلَى آلِ   مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى   إِبْرَاهِيْمَ وَعَلَى آلِ  إِبْرَاهِيْمَ فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد",
						"اَلسَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ الله"
				};
				String[] cara_baca_maghrib={
					"Usholli Fardlol maghribi roka'atim mustaqbilal qiblati adaa-an lillaahi ta'aala.",
					"Allaahu akbar.",
					"Innii wajjahtu wajhiya lilladzii fatharas samaawaati wal-ardha, haniifam muslimaw wamaa ana minal musyrikiin. Inna shalaati wanusukii wamahyaaya wamamaatii lillaahi rabbil ‘alaamiina. Laa syariika lahu wabidzaalika umirtu wa ana minal muslimiina.",
					"bismillaahirrahmaanirrahiim\nalhamdulillaahirabbil’aalamiin\narrahmaanirrahiim\nmaalikiyawmiddiin\niyyaakana’buduwa-iyyaakanasta’iin\n" +
							"ihdinaashshiraathalmustaqiim\nshiraathalladziinaan’amta’alayhim ghayrilmaghdhuubi’alayhim walaadhdhaalliin",
							"Innaa a'thainaakal kautsar\nFashalli lirabbika waanhar\nInna syaani-aka huwal abtar",
					"Subhaana rabbiyal 'azhiimi wabihamdihi",
					"Sami'allaahu liman hamidah, rabbanaa walakal hamdu.",
					"Subhaana rabbiyal a'laa wabihamdih",
					"Rabbighfirlii warhamnii warfa'nii wajburnii warzuqnii wahdinii wa 'aafinii wa'fu 'annii",
					"Attahiyyaatul mubaarakaatush sholawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma sholli ‘alaa   Muhammad.",
					"Attahiyyaatul mubaarakaatush shalawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma shalli ‘alaa Muhammad wa 'alaa aali Muhammad, kamaa shallaita 'alaa Ibraahim, wa 'alaa aali Ibraahim. Wabaarik ‘alaa Muhammad, wa 'alaa aali Muhammad, kamaa baarakta 'alaa Ibraahim, wa 'alaa aali Ibraahim. Fil 'aalamiina innaka hamiidum majiid.",
					"Assalaamu 'alaikum warahmatullaah."
				};
				String[] arti_maghrib={
					"Aku niat melakukan shalat fardu maghrib 3 rakaat, sambil menghadap qiblat, saat ini, karena Allah ta'ala.",
					"Allah Maha Besar.",
					"Sesungguhnya aku menghadapkan mukaku kepada Dzat yang menciptakan langit dan bumi dengan keadaan lurus dan berserah diri, dan bukannya aku termasuk dalam golongan musyrik. Sesungguhnya sembahyangku, ibadatku, hidupku dan matiku hanya untuk Allah semesta alam. Tiada sekutu bagi-Nya, karena itu aku rela diperintah dan aku ini adalah golongan orang Islam.",
					"Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.\nSegala puji bagi Allah, Tuhan semesta alam.\nYang Maha Pemurah lagi Maha Penyayang\n" +
							"Yang menguasai di Hari Pembalasan\nHanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan.\nTunjukilah kami jalan yang lurus,\n" +
							"(yaitu) Jalan orang-orang yang telah Engkau beri ni’mat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.",
							"Sesungguhnya Kami telah memberikan kepadamu, nikmat yang banyak.\nMaka dirikanlah shalat, karena Rabb-mu; dan berkorbanlah.\nSesungguhnya orang-orang yang membenci kamu, dialah yang terputus.",
					"Maha Suci Tuhan Yang Maha Besar lagi Maha Terpuji.",
					"Allah mendengar akan sesiapa yang memuji-Nya. Hai Tuhan kami, kepada Engkaulah segala pujian.",
					"Maha Suci Tuhan Yang Maha Tinggi lagi Maha Terpuji",
					"Ya Allah ! ampunilah dosaku, belas kasihanilah aku, dan angkatlah darjatku dan cukuplah segala kekuranganku dan berilah rezeki kepadaku, dan berilah aku petunjuk dan sejahterakanlah aku dan berilah keampunan padaku.",
					"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Ya Allah aku bersumpah dan berjanji bahwa tiada ada Tuhan yang berhak disembah kecuali Engkau ya Allah, dan aku bersumpah dan berjanji sesungguhnya Nabi Muhammad adalah utusan-Mu Ya Allah. Ya Allah, limpahkan shalawat-Mu kepada Nabi Muhammad.",
					"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Aku bersaksi bahwa tiada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah. Ya Allah! Limpahkanlah rahmat kepada Nabi Muhammad. “ Sebagimana pernah Engkau beri rahmat kepada Nabi Ibrahim dan keluarganya. Dan limpahilah berkah atas Nabi Muhammad beserta para keluarganya. Sebagaimana Engkau memberi berkah kepada Nabi Ibrahim dan keluarganya.",
					"Keselamatan dan rahmat buat Anda sekalian."
				};
				int[] gambar_maghrib={
					R.drawable.sholat1_niat,
					R.drawable.sholat2_takbir,
					R.drawable.sholat3_baca,
					R.drawable.sholat3_baca,
					R.drawable.sholat3_baca,
					R.drawable.sholat4_ruku,
					R.drawable.sholat5_iktidal,
					R.drawable.sholat6_sujud,
					R.drawable.sholat7_duduk,
					R.drawable.sholat8_awal,
					R.drawable.sholat9_akhir,
					R.drawable.sholat10_salam,
				};
				
				//Bacaan Sholat Isya'
				String[] judul_isya={
						"Membaca Niat Sholat Isya'.  ",
						"Membaca Takbiratul Ihram.  ",
						"Membaca Do'a Iftitah.  ",
						"Membaca Al-Fatikhah.  ",
						"Membaca Surat Pendek.  ",
						"Membaca Ruku.  ",
						"Membaca I'tidal.  ",
						"Bacaan Sujud.  ",
						"Bacaan Duduk Diantara 2 Sujud.  ",
						"Membaca Tasyahud Awal.  ",
						"Membaca Tasyahud Akhir.  ",
						"Membaca Salam.  "
				};
				
				int[] audio_isya={
						R.raw.sholat_niat,
						R.raw.takbir,
						R.raw.sholat_iftitah,
						R.raw.sholat_fatihah,
						R.raw.sholat_al_ikhlas,
						R.raw.sholat_rukuk,
						R.raw.sholat_iktidal,
						R.raw.sujud,
						R.raw.sholat_duduk2sujud,
						R.raw.tsyhud_awal,
						R.raw.tsyhud_akhir,
						R.raw.sholat_salam
				};
				
				String[] bacaan_isya={
						"اُصَلّى فَرْضَ الْعِشَاءِ اَرْبَعَ رَكَعَاتٍ مُسْتَقْبِلَ الْقِبْلَةِ اَدَاءً مَأْمُوْمًا ِللهِ تَعَالَى",
						"الله اكبر",
						"إِنِّي وَجَّهْتُ وَجْهِيَ لِلَّذِيْ فَطَرَ السَّمَوَاتِ وَالأَرْضَ حَنِيْفًا مُسْلِمًا وَمَا أَنَا مِنَ المُشْرِكِيْن . إِنَّ صَلاَتِي وَنُسُكِيْ وَمَحْيَايَ وَمَمَاتِي لِلَّهِ رَبِّ العَالَمِيْن لاَ شَرِيْكَ لَهُ وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ المُسْلِمِيًن",
						"بِسْمِ اللَّهِ الرَّ حْمَنِ الرَّحِيمِ\nالْحَمْدُ لِلَّهِ رَبِّ الْعَالَمِينَ\nالرَّ حْمَنِ الرَّحِيمِ\nمَالِكِ يَوْمِ الدِّينِ\nإِيَّاكَ نَعْبُدُ وَإِيَّاكَ نَسْتَعِينُ\nاهْدِنَا الصِّرَاطَ الْمُسْتَقِيمَ\nصِرَاطَ الَّذِينَ أَنْعَمْتَ عَلَيْهِمْ غَيْرِ الْمَغْضُوبِ عَلَيْهِمْ وَلَا الضَّالِّينَ",
						"إِنَّا أَعْطَيْنَاكَ الْكَوْثَرَ\nفَصَلِّ لِرَبِّكَ وَانْحَ\nإِنَّ شَانِئَكَ هُوَ الأبْتَرُ",
						"سُبْحَانَ رَبِّيَ الْعَظِيْمِ وَبِحَمْدِهِ",
						"سَمِعَ اللهُ لِمَنْ حَمِدَه رَبَّنَا وَلَكَ الْحَمْدُ",
						"سُبْحَانَ رَبِّيَ اْلأَعْلَى وَبِحَمْدِهِ",
						"رَبِ ّاِغْفِرْلِيِ وَارْحَمْنِيْ وَارْفَعْنِيْ وَاجْبُرْنِيْ وَارْزُقْنِيْ وَاهْدِنِيْ وَعَاِفِنيْ وَاعْفُ عَنِّيْ",
						"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد",
						"التَّحِيَّاتُ الْمُبَارَكَاتُ الصَّلَوَاتُ الطَّيِّبَاتُ ِللهِ ، السَّلاَمُ عَلَيْكَ أَيُّهَا النَّبِيُّ وَرَحْمَةُ اللهِ وَبَرَكاَتُهُ السَّلاَمُ عَلَيْنَا وَعَلَى عِبَادِ اللهِ الصَّالِحِيْنَ . أَشْهَدُ أَنْ لاَ إِلَهَ إِلاَّ الله وَأَشْهَدُ أَنَّ مُحَمَّدًا رَسُوْلُ الله. اللَّهُمَّ صَلِّ عَلَى  مُحَمَّد وعلى آلِ  مُحَمَّد كَمَا صَلَّبْتَ عَلَى  إِبْرَاهِيْمَ وَعَلَى آلِ   إِبْرَاهِيْمَ وَبَارِكْ عَلَى  مُحَمَّدٍ وَعَلَى آلِ   مُحَمَّدٍ كَمَا بَارَكْتَ عَلَى   إِبْرَاهِيْمَ وَعَلَى آلِ  إِبْرَاهِيْمَ فِيْ الْعَالَمِيْنَ إِنَّكَ حَمِيْدٌ مَجِيْد",
						"اَلسَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ الله"
				};
				String[] cara_baca_isya={
					"Usholli Fardlol isya'i roka'atim mustaqbilal qiblati adaa-an lillaahi ta'aala.",
					"Allaahu akbar.",
					"Innii wajjahtu wajhiya lilladzii fatharas samaawaati wal-ardha, haniifam muslimaw wamaa ana minal musyrikiin. Inna shalaati wanusukii wamahyaaya wamamaatii lillaahi rabbil ‘alaamiina. Laa syariika lahu wabidzaalika umirtu wa ana minal muslimiina.",
					"bismillaahirrahmaanirrahiim\nalhamdulillaahirabbil’aalamiin\narrahmaanirrahiim\nmaalikiyawmiddiin\niyyaakana’buduwa-iyyaakanasta’iin\n" +
							"ihdinaashshiraathalmustaqiim\nshiraathalladziinaan’amta’alayhim ghayrilmaghdhuubi’alayhim walaadhdhaalliin",
							"Innaa a'thainaakal kautsar\nFashalli lirabbika waanhar\nInna syaani-aka huwal abtar",
					"Subhaana rabbiyal 'azhiimi wabihamdihi",
					"Sami'allaahu liman hamidah, rabbanaa walakal hamdu.",
					"Subhaana rabbiyal a'laa wabihamdih",
					"Rabbighfirlii warhamnii warfa'nii wajburnii warzuqnii wahdinii wa 'aafinii wa'fu 'annii",
					"Attahiyyaatul mubaarakaatush sholawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma sholli ‘alaa   Muhammad.",
					"Attahiyyaatul mubaarakaatush shalawaatuth thayyibatul lillaah, Assalaamu’alaika ayyuhan nabiyyu warahmatullaahi wabarakaatuh, Assalaamu’alainaa wa’alaa ‘ibaadillaahish shaalihiin. Asyhadu allaa ilaaha illallaah, Waasyhadu anna Muhammadar rasuulullaah. Allahhumma shalli ‘alaa Muhammad wa 'alaa aali Muhammad, kamaa shallaita 'alaa Ibraahim, wa 'alaa aali Ibraahim. Wabaarik ‘alaa Muhammad, wa 'alaa aali Muhammad, kamaa baarakta 'alaa Ibraahim, wa 'alaa aali Ibraahim. Fil 'aalamiina innaka hamiidum majiid.",
					"Assalaamu 'alaikum warahmatullaah."
				};
				String[] arti_isya={
					"Aku niat melakukan shalat fardu isya' 4 rakaat, sambil menghadap qiblat, saat ini, karena Allah ta'ala.",
					"Allah Maha Besar.",
					"Sesungguhnya aku menghadapkan mukaku kepada Dzat yang menciptakan langit dan bumi dengan keadaan lurus dan berserah diri, dan bukannya aku termasuk dalam golongan musyrik. Sesungguhnya sembahyangku, ibadatku, hidupku dan matiku hanya untuk Allah semesta alam. Tiada sekutu bagi-Nya, karena itu aku rela diperintah dan aku ini adalah golongan orang Islam.",
					"Dengan menyebut nama Allah Yang Maha Pemurah lagi Maha Penyayang.\nSegala puji bagi Allah, Tuhan semesta alam.\nYang Maha Pemurah lagi Maha Penyayang\n" +
							"Yang menguasai di Hari Pembalasan\nHanya Engkaulah yang kami sembah, dan hanya kepada Engkaulah kami meminta pertolongan.\nTunjukilah kami jalan yang lurus,\n" +
							"(yaitu) Jalan orang-orang yang telah Engkau beri ni’mat kepada mereka; bukan (jalan) mereka yang dimurkai dan bukan (pula jalan) mereka yang sesat.",
							"Sesungguhnya Kami telah memberikan kepadamu, nikmat yang banyak.\nMaka dirikanlah shalat, karena Rabb-mu; dan berkorbanlah.\nSesungguhnya orang-orang yang membenci kamu, dialah yang terputus.",
					"Maha Suci Tuhan Yang Maha Besar lagi Maha Terpuji.",
					"Allah mendengar akan sesiapa yang memuji-Nya. Hai Tuhan kami, kepada Engkaulah segala pujian.",
					"Maha Suci Tuhan Yang Maha Tinggi lagi Maha Terpuji",
					"Ya Allah ! ampunilah dosaku, belas kasihanilah aku, dan angkatlah darjatku dan cukuplah segala kekuranganku dan berilah rezeki kepadaku, dan berilah aku petunjuk dan sejahterakanlah aku dan berilah keampunan padaku.",
					"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Ya Allah aku bersumpah dan berjanji bahwa tiada ada Tuhan yang berhak disembah kecuali Engkau ya Allah, dan aku bersumpah dan berjanji sesungguhnya Nabi Muhammad adalah utusan-Mu Ya Allah. Ya Allah, limpahkan shalawat-Mu kepada Nabi Muhammad.",
					"Segala kehormatan, keberkahan, kebahagiaan dan kebaikan bagi Allah, salam, rahmat, dan berkahNya kupanjatkan kepadamu wahai Nabi (Muhammad). Salam keselamatan semoga tetap untuk kami seluruh hamba yang shaleh-shaleh. Aku bersaksi bahwa tiada Tuhan melainkan Allah. Dan aku bersaksi bahwa Nabi Muhammad adalah utusan Allah. Ya Allah! Limpahkanlah rahmat kepada Nabi Muhammad. “ Sebagimana pernah Engkau beri rahmat kepada Nabi Ibrahim dan keluarganya. Dan limpahilah berkah atas Nabi Muhammad beserta para keluarganya. Sebagaimana Engkau memberi berkah kepada Nabi Ibrahim dan keluarganya.",
					"Keselamatan dan rahmat buat Anda sekalian."
				};
				int[] gambar_isya={
					R.drawable.sholat1_niat,
					R.drawable.sholat2_takbir,
					R.drawable.sholat3_baca,
					R.drawable.sholat3_baca,
					R.drawable.sholat3_baca,
					R.drawable.sholat4_ruku,
					R.drawable.sholat5_iktidal,
					R.drawable.sholat6_sujud,
					R.drawable.sholat7_duduk,
					R.drawable.sholat8_awal,
					R.drawable.sholat9_akhir,
					R.drawable.sholat10_salam,
				};
}
