package com.rbx_al;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

public class Nabi_Home extends Activity {

	private TextView judul, nabi1,nabi2,nabi3,nabi4,nabi5;
	public Database_nabi database=new Database_nabi();
    private MediaPlayer mp,efek;
	public int urutan=0;
	public Button berikut,sebelum; 
	private View list1,list2,list3,list4,list5;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_nabi_home);
		mp=MediaPlayer.create(this, R.raw.klik);
		berikut=(Button)findViewById(R.id.nabi_home_btn_berikutnya);
		efek=MediaPlayer.create(this, R.raw.effect_power_up);
		efek.start();
		sebelum=(Button)findViewById(R.id.Nabi_home_btn_sebelumnya);
		sebelum.setVisibility(View.GONE);
		nabi1=(TextView)findViewById(R.id.nabi_home_tx_nabi1);
		nabi2=(TextView)findViewById(R.id.nabi_home_tx_nabi2);
		nabi3=(TextView)findViewById(R.id.nabi_home_tx_nabi3);
		nabi4=(TextView)findViewById(R.id.nabi_home_tx_nabi4);
		nabi5=(TextView)findViewById(R.id.nabi_home_tx_nabi5);
		font();muncul_pertama();animasi();
	}
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}
	public void onResume(){
		super.onResume();
		animasi();efek.start();
	}
	public void font(){
		judul=(TextView)findViewById(R.id.judul_kisah);
		nabi1=(TextView)findViewById(R.id.nabi_home_tx_nabi1);
		nabi2=(TextView)findViewById(R.id.nabi_home_tx_nabi2);
		nabi3=(TextView)findViewById(R.id.nabi_home_tx_nabi3);
		nabi4=(TextView)findViewById(R.id.nabi_home_tx_nabi4);
		nabi5=(TextView)findViewById(R.id.nabi_home_tx_nabi5);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);nabi1.setTypeface(face2);nabi2.setTypeface(face2);
		nabi3.setTypeface(face2);nabi4.setTypeface(face2);nabi5.setTypeface(face2);
	}
	public void muncul_pertama(){
		nabi1.setText(database.nabi1[urutan]);
		nabi2.setText(database.nabi2[urutan]);
		nabi3.setText(database.nabi3[urutan]);
		nabi4.setText(database.nabi4[urutan]);
		nabi5.setText(database.nabi5[urutan]);
	}
	
	public void Button_klik(View v){
		if(v==findViewById(R.id.nabi_home_btn_kembali)){
			finish();mp.start();
		}else if(v==findViewById(R.id.nabi_home_btn_berikutnya)){
			if(nabi1.getText().toString().equals(database.nabi1[3])){
				berikut.setVisibility(View.GONE);
			}
			urutan++;
			if(urutan<database.nabi1.length){
				nabi1.setText(database.nabi1[urutan]);
				nabi2.setText(database.nabi2[urutan]);
				nabi3.setText(database.nabi3[urutan]);
				nabi4.setText(database.nabi4[urutan]);
				nabi5.setText(database.nabi5[urutan]);
			}
			sebelum.setVisibility(View.VISIBLE);mp.start();animasi();efek.start();
		}else if(v==findViewById(R.id.Nabi_home_btn_sebelumnya)){
			if(nabi1.getText().toString().equals(database.nabi1[1])){
				sebelum.setVisibility(View.GONE);
			}
			urutan--;
			if(urutan<database.nabi1.length){
				nabi1.setText(database.nabi1[urutan]);
				nabi2.setText(database.nabi2[urutan]);
				nabi3.setText(database.nabi3[urutan]);
				nabi4.setText(database.nabi4[urutan]);
				nabi5.setText(database.nabi5[urutan]);
			}
			berikut.setVisibility(View.VISIBLE);mp.start();animasi();efek.start();
		}
	}
	public void btn_tujuan(View v){
		Intent i=new Intent(Nabi_Home.this, Nabi_adam.class);
		if(v==findViewById(R.id.nabi_home_btn_klik1)){
			if(nabi1.getText().toString().equals(database.nabi1[0])){
				i.putExtra("judul", database.nabi1[0]);
				i.putExtra("cerita", database.cerita[0]);
			}else if(nabi1.getText().toString().equals(database.nabi1[1])){
				i.putExtra("judul", database.nabi1[1]);
				i.putExtra("cerita", database.cerita[5]);
			}else if(nabi1.getText().toString().equals(database.nabi1[2])){
				i.putExtra("judul", database.nabi1[2]);
				i.putExtra("cerita", database.cerita[10]);
			}else if(nabi1.getText().toString().equals(database.nabi1[3])){
				i.putExtra("judul", database.nabi1[3]);
				i.putExtra("cerita", database.cerita[15]);
			}else if(nabi1.getText().toString().equals(database.nabi1[4])){
				i.putExtra("judul", database.nabi1[4]);
				i.putExtra("cerita", database.cerita[20]);
			}
		}
		if(v==findViewById(R.id.nabi_home_btn_klik2)){
			if(nabi2.getText().toString().equals(database.nabi2[0])){
				i.putExtra("judul", database.nabi2[0]);
				i.putExtra("cerita", database.cerita[1]);
			}else if(nabi2.getText().toString().equals(database.nabi2[1])){
				i.putExtra("judul", database.nabi2[1]);
				i.putExtra("cerita", database.cerita[6]);
			}else if(nabi2.getText().toString().equals(database.nabi2[2])){
				i.putExtra("judul", database.nabi2[2]);
				i.putExtra("cerita", database.cerita[11]);
			}else if(nabi2.getText().toString().equals(database.nabi2[3])){
				i.putExtra("judul", database.nabi2[3]);
				i.putExtra("cerita", database.cerita[16]);
			}else if(nabi2.getText().toString().equals(database.nabi2[4])){
				i.putExtra("judul", database.nabi2[4]);
				i.putExtra("cerita", database.cerita[21]);
			}
		}
		if(v==findViewById(R.id.nabi_home_btn_klik3)){
			if(nabi3.getText().toString().equals(database.nabi3[0])){
				i.putExtra("judul", database.nabi3[0]);
				i.putExtra("cerita", database.cerita[2]);
			}else if(nabi3.getText().toString().equals(database.nabi3[1])){
				i.putExtra("judul", database.nabi3[1]);
				i.putExtra("cerita", database.cerita[7]);
			}else if(nabi3.getText().toString().equals(database.nabi3[2])){
				i.putExtra("judul", database.nabi3[2]);
				i.putExtra("cerita", database.cerita[12]);
			}else if(nabi3.getText().toString().equals(database.nabi3[3])){
				i.putExtra("judul", database.nabi3[3]);
				i.putExtra("cerita", database.cerita[17]);
			}else if(nabi3.getText().toString().equals(database.nabi3[4])){
				i.putExtra("judul", database.nabi3[4]);
				i.putExtra("cerita", database.cerita[22]);
			}
		}
		if(v==findViewById(R.id.nabi_home_btn_klik4)){
			if(nabi4.getText().toString().equals(database.nabi4[0])){
				i.putExtra("judul", database.nabi4[0]);
				i.putExtra("cerita", database.cerita[3]);
			}else if(nabi4.getText().toString().equals(database.nabi4[1])){
				i.putExtra("judul", database.nabi4[1]);
				i.putExtra("cerita", database.cerita[8]);
			}else if(nabi4.getText().toString().equals(database.nabi4[2])){
				i.putExtra("judul", database.nabi4[2]);
				i.putExtra("cerita", database.cerita[13]);
			}else if(nabi4.getText().toString().equals(database.nabi4[3])){
				i.putExtra("judul", database.nabi4[3]);
				i.putExtra("cerita", database.cerita[18]);
			}else if(nabi4.getText().toString().equals(database.nabi4[4])){
				i.putExtra("judul", database.nabi4[4]);
				i.putExtra("cerita", database.cerita[23]);
			}
		}
		if(v==findViewById(R.id.nabi_home_btn_klik5)){
			if(nabi5.getText().toString().equals(database.nabi5[0])){
				i.putExtra("judul", database.nabi5[0]);
				i.putExtra("cerita", database.cerita[4]);
			}else if(nabi5.getText().toString().equals(database.nabi5[1])){
				i.putExtra("judul", database.nabi5[1]);
				i.putExtra("cerita", database.cerita[9]);
			}else if(nabi5.getText().toString().equals(database.nabi5[2])){
				i.putExtra("judul", database.nabi5[2]);
				i.putExtra("cerita", database.cerita[14]);
			}else if(nabi5.getText().toString().equals(database.nabi5[3])){
				i.putExtra("judul", database.nabi5[3]);
				i.putExtra("cerita", database.cerita[19]);
			}else if(nabi5.getText().toString().equals(database.nabi5[4])){
				i.putExtra("judul", database.nabi5[4]);
				i.putExtra("cerita", database.cerita[24]);
			}
		}
		startActivity(i);mp.start();
	}
	public void animasi(){
		list1=findViewById(R.id.nabi_home_wadah1);list2=findViewById(R.id.nabi_home_wadah2);
		list3=findViewById(R.id.nabi_home_wadah3);list4=findViewById(R.id.nabi_home_wadah4);
		list5=findViewById(R.id.nabi_home_wadah5);
		list1.setAnimation(AnimationUtils.loadAnimation(Nabi_Home.this, R.anim.nabi_list1));
		list2.setAnimation(AnimationUtils.loadAnimation(Nabi_Home.this, R.anim.nabi_list2));
		list3.setAnimation(AnimationUtils.loadAnimation(Nabi_Home.this, R.anim.nabi_list3));
		list4.setAnimation(AnimationUtils.loadAnimation(Nabi_Home.this, R.anim.nabi_list4));
		list5.setAnimation(AnimationUtils.loadAnimation(Nabi_Home.this, R.anim.nabi_list5));
	}
}
