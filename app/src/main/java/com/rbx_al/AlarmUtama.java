package com.rbx_al;

import java.util.Calendar;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

public class AlarmUtama extends Activity {
	
	private MediaPlayer mp,efek;
	private TextView jdl;
	private CheckBox isak,subuh,dhuhur,azhar,maghrib;
	TimePicker myTimePicker1,myTimePicker2,myTimePicker3,myTimePicker4,myTimePicker5;
	TimePickerDialog timePickerDialog1,timePickerDialog2,timePickerDialog3,timePickerDialog4,timePickerDialog5;
	final static int RQS_1 = 1;
	Calendar calendar1,calNow1,calSet1,calendar2,calNow2,calSet2,calendar3,calNow3,calSet3,
			 calendar4,calNow4,calSet4,calendar5,calNow5,calSet5;
	AlarmManager am1,am2,am3,am4,am5;
	PendingIntent pending1,pending2,pending3,pending4,pending5 ;
	private View list1,list2,list3,list4,list5;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_alarm_utama);
		font();analisa_sholat();animasi();
		mp=MediaPlayer.create(this, R.raw.klik);
		isak=(CheckBox)findViewById(R.id.chkIsyak);
		subuh=(CheckBox)findViewById(R.id.chkSubuh);
		dhuhur=(CheckBox)findViewById(R.id.chkDhuhur);
		azhar=(CheckBox)findViewById(R.id.chkAshar);
		maghrib=(CheckBox)findViewById(R.id.chkMaghrib);
		efek=MediaPlayer.create(this, R.raw.effect_power_up);
		efek.start();
	}
	
	private void openTimePickerDialog1(final boolean is24r) {
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		final Editor edit=sp.edit();
	      calendar1 = Calendar.getInstance();
	      timePickerDialog1 = new TimePickerDialog(AlarmUtama.this,onTimeSetListener1, 
	    		  calendar1.get(Calendar.HOUR_OF_DAY),calendar1.get(Calendar.MINUTE), true);
	      timePickerDialog1.setTitle("Atur Waktu Pengingat");
	      timePickerDialog1.setCanceledOnTouchOutside(true);
	      timePickerDialog1.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				isak.setChecked(false);
				edit.putString("pilih_isak", "1");
				edit.commit();analisa_sholat();
			}
		});
	      timePickerDialog1.show();
	   }
	private void openTimePickerDialog2(final boolean is24r) {
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		final Editor edit=sp.edit();
	      calendar2 = Calendar.getInstance();
	      timePickerDialog2 = new TimePickerDialog(AlarmUtama.this,onTimeSetListener2, 
	    		  calendar2.get(Calendar.HOUR_OF_DAY),calendar2.get(Calendar.MINUTE), true);
	      timePickerDialog2.setTitle("Atur Waktu Pengingat");
	      timePickerDialog2.setCanceledOnTouchOutside(true);
	      timePickerDialog2.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				subuh.setChecked(false);
				edit.putString("pilih_subuh", "1");
				edit.commit();analisa_sholat();
			}
		});
	      timePickerDialog2.show();
	   }
	private void openTimePickerDialog3(final boolean is24r) {
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		final Editor edit=sp.edit();
	      calendar3 = Calendar.getInstance();
	      timePickerDialog3 = new TimePickerDialog(AlarmUtama.this,onTimeSetListener3, 
	    		  calendar3.get(Calendar.HOUR_OF_DAY),calendar3.get(Calendar.MINUTE), true);
	      timePickerDialog3.setTitle("Atur Waktu Pengingat");
	      timePickerDialog3.setCanceledOnTouchOutside(true);
	      timePickerDialog3.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				dhuhur.setChecked(false);
				edit.putString("pilih_dhuhur", "1");
				edit.commit();analisa_sholat();
			}
		});
	      timePickerDialog3.show();
	   }
	private void openTimePickerDialog4(final boolean is24r) {
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		final Editor edit=sp.edit();
	      calendar4 = Calendar.getInstance();
	      timePickerDialog4 = new TimePickerDialog(AlarmUtama.this,onTimeSetListener4, 
	    		  calendar4.get(Calendar.HOUR_OF_DAY),calendar4.get(Calendar.MINUTE), true);
	      timePickerDialog4.setTitle("Atur Waktu Pengingat");
	      timePickerDialog4.setCanceledOnTouchOutside(true);
	      timePickerDialog4.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				azhar.setChecked(false);
				edit.putString("pilih_azhar", "1");
				edit.commit();analisa_sholat();
			}
		});
	      timePickerDialog4.show();
	   }
	private void openTimePickerDialog5(final boolean is24r) {
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		final Editor edit=sp.edit();
	      calendar5 = Calendar.getInstance();
	      timePickerDialog5 = new TimePickerDialog(AlarmUtama.this,onTimeSetListener5, 
	    		  calendar5.get(Calendar.HOUR_OF_DAY),calendar5.get(Calendar.MINUTE), true);
	      timePickerDialog5.setTitle("Atur Waktu Pengingat");
	      timePickerDialog5.setCanceledOnTouchOutside(true);
	      timePickerDialog5.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				maghrib.setChecked(false);
				edit.putString("pilih_maghrib", "1");
				edit.commit();analisa_sholat();
			}
		});
	      timePickerDialog5.show();
	   }
	
	OnTimeSetListener onTimeSetListener1 = new OnTimeSetListener() {
	      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
	    	  SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
	  		final Editor edit=sp.edit();
	         calNow1 = Calendar.getInstance();
	         calSet1 = (Calendar) calNow1.clone();
	         calSet1.set(Calendar.HOUR_OF_DAY, hourOfDay);
	         calSet1.set(Calendar.MINUTE, minute);
	         calSet1.set(Calendar.SECOND, 0);
	         calSet1.set(Calendar.MILLISECOND, 0);
	         if (calSet1.compareTo(calNow1) <= 0) {
	            // Today Set time passed, count to tomorrow
	            calSet1.add(Calendar.DATE, 1);
	            Log.i("hasil", " =<0");
	         } else if (calSet1.compareTo(calNow1) > 0) {
	            Log.i("hasil", " > 0");
	         } else {
	            Log.i("hasil", " else ");
	         }
	         setAlarm1(calSet1);
	         edit.putString("pilih_isak", "0");
	         edit.commit();
	         analisa_sholat();
	      }
	   };
	   OnTimeSetListener onTimeSetListener2 = new OnTimeSetListener() {
		      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		    	  SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
		         calNow2 = Calendar.getInstance();
		         calSet2 = (Calendar) calNow2.clone();
		         calSet2.set(Calendar.HOUR_OF_DAY, hourOfDay);
		         calSet2.set(Calendar.MINUTE, minute);
		         calSet2.set(Calendar.SECOND, 0);
		         calSet2.set(Calendar.MILLISECOND, 0);
		         if (calSet2.compareTo(calNow2) <= 0) {
		            // Today Set time passed, count to tomorrow
		            calSet2.add(Calendar.DATE, 1);
		            Log.i("hasil", " =<0");
		         } else if (calSet2.compareTo(calNow2) > 0) {
		            Log.i("hasil", " > 0");
		         } else {
		            Log.i("hasil", " else ");
		         }
		         setAlarm2(calSet2);
		         edit.putString("pilih_subuh", "0");
		         edit.commit();analisa_sholat();
		      }
		   };
		   OnTimeSetListener onTimeSetListener3 = new OnTimeSetListener() {
			      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			    	  SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
						final Editor edit=sp.edit();
			         calNow3 = Calendar.getInstance();
			         calSet3 = (Calendar) calNow3.clone();
			         calSet3.set(Calendar.HOUR_OF_DAY, hourOfDay);
			         calSet3.set(Calendar.MINUTE, minute);
			         calSet3.set(Calendar.SECOND, 0);
			         calSet3.set(Calendar.MILLISECOND, 0);
			         if (calSet3.compareTo(calNow3) <= 0) {
			            // Today Set time passed, count to tomorrow
			            calSet3.add(Calendar.DATE, 1);
			            Log.i("hasil", " =<0");
			         } else if (calSet3.compareTo(calNow3) > 0) {
			            Log.i("hasil", " > 0");
			         } else {
			            Log.i("hasil", " else ");
			         }
			         setAlarm3(calSet3);
			         edit.putString("pilih_dhuhur", "0");
			         edit.commit();analisa_sholat();
			      }
			   };
			   OnTimeSetListener onTimeSetListener4 = new OnTimeSetListener() {
				      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
				    	  SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
							final Editor edit=sp.edit();
				         calNow4 = Calendar.getInstance();
				         calSet4 = (Calendar) calNow4.clone();
				         calSet4.set(Calendar.HOUR_OF_DAY, hourOfDay);
				         calSet4.set(Calendar.MINUTE, minute);
				         calSet4.set(Calendar.SECOND, 0);
				         calSet4.set(Calendar.MILLISECOND, 0);
				         if (calSet4.compareTo(calNow4) <= 0) {
				            // Today Set time passed, count to tomorrow
				            calSet4.add(Calendar.DATE, 1);
				            Log.i("hasil", " =<0");
				         } else if (calSet4.compareTo(calNow4) > 0) {
				            Log.i("hasil", " > 0");
				         } else {
				            Log.i("hasil", " else ");
				         }
				         setAlarm4(calSet4);
				         edit.putString("pilih_azhar", "0");
				         edit.commit();analisa_sholat();
				      }
				   };
				   OnTimeSetListener onTimeSetListener5 = new OnTimeSetListener() {
					      public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
					    	  SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
								final Editor edit=sp.edit();
					         calNow5 = Calendar.getInstance();
					         calSet5 = (Calendar) calNow5.clone();
					         calSet5.set(Calendar.HOUR_OF_DAY, hourOfDay);
					         calSet5.set(Calendar.MINUTE, minute);
					         calSet5.set(Calendar.SECOND, 0);
					         calSet5.set(Calendar.MILLISECOND, 0);
					         if (calSet5.compareTo(calNow5) <= 0) {
					            // Today Set time passed, count to tomorrow
					            calSet5.add(Calendar.DATE, 1);
					            Log.i("hasil", " =<0");
					         } else if (calSet5.compareTo(calNow5) > 0) {
					            Log.i("hasil", " > 0");
					         } else {
					            Log.i("hasil", " else ");
					         }
					         setAlarm5(calSet5);
					         edit.putString("pilih_maghrib", "0");
					         edit.commit();analisa_sholat();
					      }
					   };
	   
	   private void setAlarm1(Calendar targetCal) {
		      try{
		      Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima.class);
		      pending1=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			  am1 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
		      am1.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
		            pending1);
		      }catch(NumberFormatException e){
		    	  Log.i("AlarmManager", "Number format exception");
		      }
		   }
	   private void setAlarm2(Calendar targetCal) {
		      try{
		      Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Subuh.class);
		      pending2=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			  am2 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
		      am2.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
		            pending2);
		      }catch(NumberFormatException e){
		    	  Log.i("AlarmManager", "Number format exception");
		      }
		   }
	   private void setAlarm3(Calendar targetCal) {
		      try{
		      Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Dhuhur.class);
		      pending3=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			  am3 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
		      am3.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
		            pending3);
		      }catch(NumberFormatException e){
		    	  Log.i("AlarmManager", "Number format exception");
		      }
		   }
	   private void setAlarm4(Calendar targetCal) {
		      try{
		      Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Azhar.class);
		      pending4=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			  am4 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
		      am4.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
		            pending4);
		      }catch(NumberFormatException e){
		    	  Log.i("AlarmManager", "Number format exception");
		      }
		   }
	   private void setAlarm5(Calendar targetCal) {
		      try{
		      Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Maghrib.class);
		      pending5=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
			  am5 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
		      am5.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
		            pending5);
		      }catch(NumberFormatException e){
		    	  Log.i("AlarmManager", "Number format exception");
		      }
		   }
	
	public void analisa_sholat(){
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		isak=(CheckBox)findViewById(R.id.chkIsyak);
		subuh=(CheckBox)findViewById(R.id.chkSubuh);
		dhuhur=(CheckBox)findViewById(R.id.chkDhuhur);
		azhar=(CheckBox)findViewById(R.id.chkAshar);
		maghrib=(CheckBox)findViewById(R.id.chkMaghrib);
		String ambil_isak=sp.getString("pilih_isak", ""),
			   ambil_subuh=sp.getString("pilih_subuh", ""),
			   ambil_dhuhur=sp.getString("pilih_dhuhur", ""),
			   ambil_azhar=sp.getString("pilih_azhar", ""),
			   ambil_maghrib=sp.getString("pilih_maghrib", "");
		int nol=0,satu=1;
		boolean pilihan=false;
		int bg = 0;
		if(ambil_isak.isEmpty()||Integer.parseInt(ambil_isak)>nol){
			bg=R.drawable.alarm_list_file;
			pilihan=false;
		}else if(Integer.parseInt(ambil_isak)<satu){
			bg=R.drawable.alarm_list_aktif;
			pilihan=true;
		}
		isak.setChecked(pilihan);
		isak.setBackgroundResource(bg);
		
		if(ambil_subuh.isEmpty()||Integer.parseInt(ambil_subuh)>0){
			pilihan=false;
			bg=R.drawable.alarm_list_file;;
		}else if(Integer.parseInt(ambil_subuh)<1){
			bg=R.drawable.alarm_list_aktif;
			pilihan=true;
		}
		subuh.setChecked(pilihan);
		subuh.setBackgroundResource(bg);
		
		if(ambil_dhuhur.isEmpty()||Integer.parseInt(ambil_dhuhur)>0){
			bg=R.drawable.alarm_list_file;
			pilihan=false;
		}else if(Integer.parseInt(ambil_dhuhur)<1){
			bg=R.drawable.alarm_list_aktif;
			pilihan=true;
		}
		dhuhur.setChecked(pilihan);
		dhuhur.setBackgroundResource(bg);
		
		if(ambil_azhar.isEmpty()||Integer.parseInt(ambil_azhar)>0){
			bg=R.drawable.alarm_list_file;
			pilihan=false;
		}else if(Integer.parseInt(ambil_azhar)<1){
			bg=R.drawable.alarm_list_aktif;
			pilihan=true;
		}
		azhar.setChecked(pilihan);
		azhar.setBackgroundResource(bg);
		if(ambil_maghrib.isEmpty()||Integer.parseInt(ambil_maghrib)>0){
			bg=R.drawable.alarm_list_file;
			pilihan=false;
		}else if(Integer.parseInt(ambil_maghrib)<1){
			bg=R.drawable.alarm_list_aktif;
			pilihan=true;
		}
		maghrib.setBackgroundResource(bg);
		maghrib.setChecked(pilihan);
		
	}
	
	public void check1(View v){
		if(isak.isChecked()){
				openTimePickerDialog1(false);
		}else if(isak.isChecked()==false){
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setCancelable(false).setTitle("Batalkan Pengingat ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima.class);
					pending1=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
					am1 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
					am1.cancel(pending1);
					SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
					edit.putString("pilih_isak", "1");
					edit.commit();analisa_sholat();}
				}).setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						dialog.cancel();
						analisa_sholat();}}).show();
		}
	}
	public void check2(View v){
		if(subuh.isChecked()){
			openTimePickerDialog2(false);
		}else if(subuh.isChecked()==false){
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setCancelable(false).setTitle("Batalkan Pengingat ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Subuh.class);
					pending2=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
					am2 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
					am2.cancel(pending2);
					SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
					edit.putString("pilih_subuh", "1");
					edit.commit();analisa_sholat();}
				}).setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						dialog.cancel();
						analisa_sholat();}}).show();
		}
	}
	public void check3(View v){
		if(dhuhur.isChecked()){
			openTimePickerDialog3(false);
		}else if(dhuhur.isChecked()==false){
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setCancelable(false).setTitle("Batalkan Pengingat ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Dhuhur.class);
					pending3=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
					am3 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
					am3.cancel(pending3);
					SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
					edit.putString("pilih_dhuhur", "1");
					edit.commit();analisa_sholat();}
				}).setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						dialog.cancel();
						analisa_sholat();}}).show();
		}
	}
	public void check4(View v){
		if(azhar.isChecked()){
			openTimePickerDialog4(false);
		}else if(azhar.isChecked()==false){
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setCancelable(false).setTitle("Batalkan Pengingat ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Azhar.class);
				    pending4=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
					am4 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
					am4.cancel(pending4);
					SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
					edit.putString("pilih_azhar", "1");
					edit.commit();analisa_sholat();}
				}).setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						dialog.cancel();
						analisa_sholat();}}).show();
		}
	}
	public void check5(View v){
		if(maghrib.isChecked()){
			openTimePickerDialog5(false);
		}else if(maghrib.isChecked()==false){
			AlertDialog.Builder ab = new AlertDialog.Builder(this);
			ab.setCancelable(false).setTitle("Batalkan Pengingat ?")
			.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					Intent intent = new Intent(AlarmUtama.this, Alarm_Penerima_Maghrib.class);
				    pending5=PendingIntent.getActivity(AlarmUtama.this, 2, intent, PendingIntent.FLAG_CANCEL_CURRENT);
					am5 = (android.app.AlarmManager)getSystemService(ALARM_SERVICE);
					am5.cancel(pending5);
					SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
					final Editor edit=sp.edit();
					edit.putString("pilih_maghrib", "1");
					edit.commit();analisa_sholat();}
				}).setNegativeButton("Tidak",
						new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int which) {
						dialog.cancel();
						analisa_sholat();}}).show();
		}
	}
	public void simpan(View v){
		finish();
		mp.start();
	}
	
	public void onBackPressed(){
		mp.start();
		finish();
	}
	public void font(){
		jdl=(TextView)findViewById(R.id.judul_alarm);
		isak=(CheckBox)findViewById(R.id.chkIsyak);
		subuh=(CheckBox)findViewById(R.id.chkSubuh);
		dhuhur=(CheckBox)findViewById(R.id.chkDhuhur);
		azhar=(CheckBox)findViewById(R.id.chkAshar);
		maghrib=(CheckBox)findViewById(R.id.chkMaghrib);
		Button kembali=(Button)findViewById(R.id.alarm_btn_kembali);
		Typeface ganti=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				 ganti2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		jdl.setTypeface(ganti);isak.setTypeface(ganti2);subuh.setTypeface(ganti2);
		dhuhur.setTypeface(ganti2);azhar.setTypeface(ganti2);maghrib.setTypeface(ganti2);
		kembali.setTypeface(ganti2);
	}

	public void animasi(){
		list1=findViewById(R.id.chkIsyak);list2=findViewById(R.id.chkSubuh);
		list3=findViewById(R.id.chkDhuhur);list4=findViewById(R.id.chkAshar);
		list5=findViewById(R.id.chkMaghrib);
		list1.setAnimation(AnimationUtils.loadAnimation(AlarmUtama.this, R.anim.alarm_list1));
		list2.setAnimation(AnimationUtils.loadAnimation(AlarmUtama.this, R.anim.alarm_list2));
		list3.setAnimation(AnimationUtils.loadAnimation(AlarmUtama.this, R.anim.alarm_list3));
		list4.setAnimation(AnimationUtils.loadAnimation(AlarmUtama.this, R.anim.alarm_list4));
		list5.setAnimation(AnimationUtils.loadAnimation(AlarmUtama.this, R.anim.alarm_list5));
	}
}
