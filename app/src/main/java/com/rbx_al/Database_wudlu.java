package com.rbx_al;

public class Database_wudlu {
String[] keterangan={
		"Membasuh kedua telapak tangan sebanyak 3 kali",
		"Mengambil air dengan tangan kanan kemudian memasukkannya ke dalam mulut untuk berkumur-kumur dengan tangan kiri sebanyak 3 kali.",
		"Membaaca Niat Berwudlu",
		"Membasuh seluruh wajah dan menyela-nyelai jenggot sebanyak 3 kali.",
		"Membasuh tangan kanan hingga siku bersamaan dengan menyela-nyelai jemari sebanyak 3 kali.dilanjutkan dengan yang kiri.",
		"Membasuh tangan kiri hingga siku bersamaan dengan menyela-nyelai jemari sebanyak 3 kali.",
		"Menyapu seluruh kepala dengan cara mengusap dari depan ditarik ke belakang, lalu ditarik lagi ke depan, dilakukan sebanyak 1 kali.",
		"Menyapu bagian luar dan dalam telinga sebanyak 1 kali.",
		"Membasuh kaki kanan hingga mata kaki bersamaan dengan menyela-nyelai jemari sebanyak 3 kali kemudian dilanjutkan dengan kaki kiri.",
		"Membaca Do'a Sesudah Berwudlu"
	},judul={
		"Membasuh telapak tangan",
		"Berkumur-kumur",
		"Membaca Niat Wudlu",
		"Membasuh Wajah",
		"Membasuh Tangan Kanan",
		"Membasuh Tangan Kiri",
		"Membasuh Kepala",
		"Membasuh Telinga",
		"Membasuh Kaki",
		"Membaca Do'a Sesudah Wudlu"
	},
	arab={
		"بِسْمِ اللَّهِ الرَّحْمنِ الرَّحيمِ\nاَلْحَمْدُ ِللهِ الَّذِي جَعَلَ اْلمَاءَ طَهُوْرًا",
		"اَللَّهُمَّ اَسْـقِـنِى مِنْ حَوْضِ نَبِيِّكَ مُحَمَّدٍ صَلَّى اللهُ عَلَيْهِ وَسَلَّمَ كَأْسًا لاَ أَظْمَأُ بَعْدَهاَ أَبَدًا",
		"نَوَيْتُ الْوُضُوْءَ لِرَفْعِ الْحَدَثِ اْلاَصْغَرِ فَرْضًا ِللهِ تَعَالَى",
		"اَللَّهُمَّ بَيِّضْ وَجْهِى يَوْمَ تَبْيَضُّ وُجُوْهٌ وَتَسْوَدُّ وُجُوْهٌ",
		"اَللَّهُمَّ اَعْطِنِى كِتاَبِى بِيَمِيْنِى وَحَاسِبْنِى حِسَاباً يَسِيْرًا",
		"اَللَّهُمَّ لاَ تُعْطِنِى كِتاَبِى مِنْ يَساَرِىْ وَ لاَ مِنْ وَرَاءِ ظَهْرِىْ",
		"اَللَّهُمَّ حَرِّمْ شَعْرِيْ وَبَشَرِيْ مِنَ النَّارِ وَاَظِلَّنِي تَحْتَ عَرْشِكَ يَوْمَ لاَ ظِلَّ اِلاَّ ظِلُّكَ",
		"اَللَّهُمَّ اجْعَلْنِي مِنَ الَّذِيْنَ يَسْتَمِعُوْنَ اْلقَوْلَ فَيَتَّبِعُوْنَ أَحْسَنَهُ",
		"اَللَّهُمَّ ثَبِّتْ قَدَمَّي عَلَى الصِّرَاطِ يَوْمَ تَزِلُّ فِيْهِ اْلاَقْدَامُ",
		"-"
	},
	cara_baca={
		"Bismillahirrahmanirrahim\nAllhamdulillahi ladzii ja’alal maa’a thohuuraa.",
		"Allaahumma asqinii min haudli Nabiyyika Muhammadin shallallaahu ’alaihi wasallama ka’san azhma’u ba’dahu abadan.",
		"Nawaitul Wudhuu A Liraf'il Hadatsil Ashghari Fardhal Lillaahi Ta'aalaa",
		"Allahumma Bayyadl Waj'hi Yauma Tab'yadlu Wujuuhu Wataswaddu Wujuuhu",
		"Allaahumma a’thinii kitaabii biyaminii waahaasibnii hisaabaan yasiiraa.",
		"Allaahumma laatu’thini kitaabiii miyyasaarii wa laa min waraa-i dlahri",
		"Allaahumma harrim sya’rii Wabasyari Minannaari wazhillanii tahta ’arsika yauma laazhilla illaa zhilluka.",
		"Allahummaj'alni Minalladzina Yastami'unal Qoula Fayattabi'una Akhsanah.",
		"Allahumma Tsabbat Qodamma 'Alasshoroti Yauma Tazillu Fiihil Aq'daamu",
		"Asysy hadu an laa illaaha illallaah wah dahulaa syarikalah waasyhadu anna muhammadan ’abduhuu wa rarasuuluhuallaahum maj ’alnii minat tawaabiina waj’alnii minal mutathahhitiina waj:alnii min ’ibaadikash shaalihiin."
	},
	arti={
		"Dengan nama Allah yang Maha Pemurah Lagi Maha Penyayang. Segala Puji bagi Allah yang menjadikan air itu suci.",
		"Ya Allah, curahkan segelas air dari telaga Nabimu Muhammad SAW yang tidak akan kehausan setelah itu selama-lamanya.",
		"Saya niat berwudhu untuk menghilangkan hadas kecil fardu karena Allah Ta'ala",
		"Ya Allah! beri cahaya di wajahku pada hari bercahaya.",
		"Ya Allah! berikanlah kepadaku kitabku dari sebelah kanan dan hitunglah amalanku dengan perhitungan yang mudah.",
		"Ya Allah! aku berlindung denganMu dari menerima kitab amalanku dari sebelah kiri atau dari sebelah belakang.",
		"Ya Allah, haramkan rambutku dan kulitku dari neraka dan lindungilah aku dari ArsyMu pada hari tidak ada perlindungan kecuali perlindunganMu.",
		"Ya Allah, jadikanlah aku termasuk orang-orang yang mendengarkan kata dan mengikuti sesuatu yang terbaik.",
		"Ya Allah, mantapkan kedua kakiku di atas titian (shirothol mustaqim) pada hari dimana banyak kaki-kaki yang tergelincir.",
		"Saya bersaksi bahwa tiada Tuhan selain Allah Satu-satu-Nya,tiada sekutu bagi-Nya Dan Saya bersaksi bahwa Muhammad adalah hamba-Nya dan utusan-Nya.Yaa Allah,jadikanlah saya termasuk orang-orang yang bertaubat dan jadikanlah aku termasuk orang-orang yang suci dan jadikanlah saya termasuk orang-orang yang shalih."
	};
	int[] gambar={
		R.drawable.wd1,
		R.drawable.wd2,
		R.drawable.wd0,
		R.drawable.wd3,
		R.drawable.wd4,
		R.drawable.wd4,
		R.drawable.wd5,
		R.drawable.wd6,
		R.drawable.wd7,
		R.drawable.wd8
	};
}
