package com.rbx_al;

public class Database_nabi {
	String[] nabi1 = {
			"1. Nabi Adam A.S",
			"6. Nabi Ibrahim A.S",
    		"11. Nabi Yusuf A.S",
    		"16. Nabi Zulkifli A.S",
    		"21. Nabi Yunus A.S",
    		};
	String[] nabi2 = {
			"2. Nabi Idris A.S",
    		"7. Nabi Luth A.S",
    		"12. Nabi Ayub A.S",
    		"17. Nabi Daud A.S",
    		"22. Nabi Zakariya A.S",
    		};
	String[] nabi3 = {
			"3. Nabi Nuh A.S",
			"8. Nabi Ismail A.S",
    		"13. Nabi Suaeb A.S",
    		"18. Nabi Sulaiman A.S",
    		"23. Nabi Yahya A.S"
    		};
	String[] nabi4 = {
			"4. Nabi Hud A.S",
    		"9. Nabi Ishaq A.S",
    		"14. Nabi Musa A.S",
    		"19. Nabi Ilyas A.S",
    		"24. Nabi Isa A.S",
    		};
	String[] nabi5 = {
			"5. Nabi Sholeh A.S",
			"10. Nabi Ya'kub A.S",
    		"15. Nabi Harun A.S",
    		"20. Nabi Ilyasa A.S",
    		"25. Muhammad S.A.W"
    		};
	String[] cerita={
			"Diyakini sebagai Manusia pertama yang menginjakkan kakinya dibumi, sebagai pasangan Nabi Adam yaitu Hawa yang diciptakan dari tulang rusuk kiri Nabi Adam.Mereka diturunkan kebumi oleh Allah SWT karena telah berbuat kesalahan akibat godaan iblis (Syeitan).\n" +
			" Nabi Adam dan Hawa dikaruniai 2 pasang putra dan putri yang bernama Qabil dan Iklima, kemudian Habil dan Labuda.Qabil yang artinya bersifat Kasar, sedangkan Habil yang artinya bersifat Lembut. Kedua sifat inilah yang akhirnya menjadi Sifat-sifat dasar manusia sampai saat ini.",
			
			"Nabi Idris diyakini sebagai Nabi pertama yang menulis dengan Pena, Masyarakat terdahulu mempercayai pula bahwa dia dibawa ke Surga tanpa mengalami kematian. Peristiwa itu terjadi ketika Nabi Idris berusia 82 tahun.",
			
			"Nabi Nuh menyebarkan ajaran Islam untuk menyembah Allah SWT, namun masyarakat menolak dan menganggapnya gila. Nabi Nuh kemudian diberikan peringatan oleh Allah SWT bahwa akan ada banjir besar yang akan melanda daerahnya. Oleh karena itu Nabi Nuh diperintahkan untuk membuat sebuah kapal, \n" +
					"Namun masyarakat sekitar tetap tidak mendengarkan peringatan yang disampaikan oleh Nabi Nuh, sehingga mereka semua hanyut dalam banjir tersebut.",
			
			"Nabi Hud tergolong dalam kaum ada yang terhormat, kehidupan mereka serba maju dan berkecukupan. Namun sayangnya mereka selalu berfoya-foya dan tenggelam dalam kehidupan Fana.Nabi Hud mengingatkan kepada mereka untuk bersyukur dan selalu memohon kepada Allah SWT, \n" +
			"namun mereka menolak dan akhirnya murka Allah SWT datang dengan menurunkan Azab berupa Badai Gurun selama 7 hari 7 malam, Bagi kaum yang mendengarkan himbauan Nabi Hud selamat dengan berpindah ke Kota Hadramaut.",
					
			"Mukjizat Nabi Shalih yang paling terkenal adalah Unta betina yang keluar dari batu, setelah Nabi Shalih memukulkan Telapak Tangannya. Nabi Shalih meminta agar penduduk setempat untuk tidak mengganggu Unta tersebut dan susunya boleh diperah untuk memenuhi kebutuhan penduduk miskin, namun kaum yang tidak menyukainya \n" +
					"berusaha untuk membunuh Unta tersebut, pada akhirnya mereka dijatuhi Azab Petir dan Gempa.",
			
			"Nabi Ibrahim dikenal sebagai Bapak para Nabi, dia sangat dihormati oleh pemeluk 3 Agama yaitu:\n- Islam\n- Kristen\n- Yahudi \nNabi Ibrahim-lah yang membangun Ka'bah dikota Mekkah, keyakinannya yang kuat terhadap Islam dimulai dari pencariannya akan Tuhan. \n" +
			"Dia sangat tidak tidak menerima orang-orang disekitarnya yang menyembah berhala, sampai akhirnya Nabi Ibrahim dibakar hidup-hidup.. namun Allah SWT menurunkan Mukjizatnya dengan menyelamatkan Nabi Ibrahim dari kobaran api tersebut.",
			
			"Perjuangan Nabi Luth adalah menyeru kaum Sodom untuk kembali ke jalan yang benar yaitu meninggalkan Homoseksual, kemudian menyembah Allah SWT. Pada akhirnya Allah SWT berfirman agar Nabi Luth segera meninggalkan pemukimannya dan kemudian Allah SWT menurunkan Azab yang pedih kepada kaum tersebut.",
			
			"Nabi Ismail dan keluarganya merupakan orang-orang yang terdahulu melaksanakan Ibadah Haji, Suatu saat Nabi Ismail haus dan Ibunya bolak-balik dari Bukit Safa-Marwah untuk mencari air, hingga akhirnya keluarlah sebuah mata air yang saat ini dikenal sebagai Air Zamzam. \n" +
			"Dalam perjalanan menuju tempat penyembelihan, Nabi Ismail digoda oleh Syeitan agar membatalkan niatnya. namun Nabi Ismail tidak goyah dan melempar Syeitan tersebut dengan Batu, yang saat ini menjadi Ritula Ibadah Haji yaitu Lempar Jumrah.Seperti yang kita ketahui, saat akan disembelih \n" +
			"jasad Nabi Ismail digantikan oleh seekor Kambing, yang akhirnya menjadi cikal bakal Ibadah Idul Adha.",
			
			"Nabi Ishaq lebih banyak menemani Bapaknya yaitu Nabi Ibrahim dalam berdakwah dan menyebarkan ajaran Islam.",
			
			"Nabi Ya'kub adalah Kakek Moyang para Rasul sebelum masa Nabi Muhammad SAW. Sikap dan cara berpikirnya tentu berpengaruh kepada para Rasul keturunannya serta kaum Yahudi dan kemudian Nasrani penegak panji keesaan Allah SWT sebelum era Nabi Muhammad SAW.",
			
			"Nabi Yusuf dikisahkan dalam riwayatnya sebagai seorang pria yang sangat tampan dan sangat piawai dalam memimpin negaranya, sejak kecil dia mendapat mimpi yang tidak biasa.. dan ketika besar Nabi Yusuf dapat mentakwilkan mimpinya tersebut sehingga dia sangat dihormati oleh masyarakat sekitarnya.",
			
			"Keluarga Nabi Ayub dikenal sangat kaya raya dan sangat Dermawan, namun kesejahteraan ini tidak membuatnya sombong, ini yang mendorong iblis (Syeitan) untuk menggodanya.Allah SWT pun menentang iblis sekiranya dia dapat meruntuhkan Iman Nabi Ayub.\n" +
			"Ujian itu pun tiba, seluruh harta yang dimiliki Nabi Ayub terbakar, setelah itu Nabi Ayub pun terserang penyakit kulit hingga 80 tahun lamanya. namun dia dan istrinya yang Setia, Rahmah, tetap bertawakkal kepada Allah SWT.Sampai akhirnya Allah SWT berfirman \n" +
			"agar Nabi Ayub menapakkan kedua kakinya di tanah, kemudian dari tanah tersebut keluarlah air yang dapat menyembuhkan penyakit Nabi Ayub yang dideritanya selama 80 tahun.",
			
			"Nabi Syuaib menyebarkan ajaran Islam didaerah Madyan, namun masyarakat Madyan menolak ajaran tersebut hingga akhirnya Allah SWT menurunkan Azab berupa Petir dan Kilat yang menghanguskan mereka.",
			
			"Kisah pertarungan Nabi Musa melawan Fir'aun merupakan salah satu kisah yang paling tersohor, dikisahkan bahwa Fir'aun merasa terancam dengan keberadaan Nabi Musa yang menyebarkan ajaran Islam untuk mengesahkan Allah SWT. Mereka bertarung dan akhirnya Nabi Musa memenangkannya dengan bantuan tongkanya, \n" +
			"kemudian Nabi Musa dan kaumnya dikejar oleh para pengikut Fir'aun. Namun mereka berhasil lolos dengan bantuan tongkat Nabi Musa yang dapat membelah lautan. Nabi Musa akhirnya mendapat Mukjizat Kitab Taurat yang dikenal dengan perjanjian lama yang berisi ajaran pokok 10 Perintah Allah SWT.",
			
			"Nabi Harun disebut sebagai partner setia Nabi Musa, dia adalah sosok yang cakap berdakwah, pandai berdiplomasi, dan penuh perhatian. Nabi Harun selalu mendampingi Nabi Musa dalam berdakwah, hingga suatu saat Nabi Musa memutuskan untuk beruziah dan menitipkan pembinaan umatnya kepada Nabi Harun.\n " +
			"Nabi Harun juga sempat berjuang untuk memberantas penyembahan berhala yang dipimpin oleh Samiri salah seorang tukang sihir kerajaan Fir'aun.",
			
			"Sejarah menyebutkan bahwa Nabi Dzulkifli adalah putra Nabi Ayub, dikisahkan pula bahwa dia mewarisi sifat sabar ayahnya yaitu Nabi Ayub.\n " +
			"Suatu saat beliau ditunjuk untuk menjadi Raja setelah dapat memenuhi persyaratan yang diminta, yaitu calon pengganti haruslah seorang yang sanggup berpuasa di siang hari, beribadah di malam hari, dan bukanlah seorang yang pemarah.",
			
			"Figur Nabi Daud memuncak disaat dia berhasil membunuh Jalut, Pemimpin kaum pemberontak di Palestina. Nabi Daud kemudian diangkat menjadi Raja dan berlaku sangat adil, Di masanya.. kerajaan tumbuh kuat dan masyarakat menjadi makmur.\n" +
			"Suatu saat Nabi Daud melarang para Nelayan untuk melaut, namun peringatan tersebut dilanggar, sehingga terjadi bencana Gempa yang menewaskan seluruh penduduk.",
			
			"Salah satu keahlian Nabi Sulaiman yang paling menonjol adalah kemampuannya yang dapat berkomunikasi dengan Binatang. Dia juga merupakan Raja yang sangat bijaksana, kekuasaannya bahkan mencakup sampai Bangsa Jin.",
			
			"Nabi Ilyas tinggal di lembah sungai Yordan yang dimana penduduknya menyembah berhala, Nabi Ilyas menyerukan kepada mereka untuk meninggalkan berhala.Namun mereka tidak mendengarkannya, bahkan mereka menantang Tuhan yang disembah Nabi Ilyas menurunkan bencana, \n" +
			"dan akhirnya kekeringan melanda daerah tersebut.. setelah beberapa tahun kemudian, Nabi Ilyas dapat meyakinkan kaum tersebut untuk menyembah Allah SWT.",
			
			"Nabi Ilyasa merupakan kerabat dekat dari Nabi Ilyas, setelah Nabi Ilyas meninggal dunia beliau melanjutkan perjuangan Nabi Ilyas untuk menghalau penyembahan berhala yang kembali merebak di lembah sungai Yordan. Namun kaum tersebut tidak mau mendengarkan, \n" +
			"sehingga bencana kekeringan kembali melanda mereka.",
			
			"Nabi Yunus berusaha menyebarkan ajaran Allah SWT, namun dia tidak mendapat sambutan baik dari masyarakat. Dalam perjalannya menjauhi daerah tersebut karena khawatir akan dibunuh. Kapal yang dia tumpangi diguncang Topan dan diputuskan bahwa Nabi Yunus akan dikorbankan untuk ditenggelamkan ke laut demi keselamatan penumpang lainnya.. \n" +
			"namun Mukjizat Allah SWT datang, Nabi Yunus dimakan oleh seekor Ikan Paus dan ditemukan masih hidup di perut Ikan Paus tersebut, Nabi Yunus pun melanjutkan perjalanannya dalam menyebarkan ajaran Islam.",
			
			"Nabi Zakaria dan istrinya yaitu Isya membaktikan dirinya untuk menjaga Baitul Maqdis - rumah Ibadah peninggalan Nabi Sulaiman di Yerusalem. Nabi Zakaria dikaruniai keturunan oleh Allah SWT disaat beliau berusia sudah cukup uzur yaitu 100 tahun, dan Anak tersebut adalah Nabi Yahya.",
			
			"Nabi Yahya mengajarkan bahwa kebenaran harus ditegakkan dengan resiko apapun, pada riwayatnya dicontohkan saat dia bersikeras dan melarang pernikahan antara seorang Paman dengan Keponakannya sendiri.",
			
			"Nabi ISA adalah putra dari Maryam yang dilahirkan tanpa memiliki Suami, hal ini yang menimbulkan kontroversi dan hujatan bertubi-tubi kepada Maryam. Secara ajaib Nabi ISA yang saat itu masih bayi tiba-tiba berbicara dan menjelaskan apa yang sebenarnya terjadi.. \n" +
			"bahwa penciptaan dirinya diawali dari kedatangan malaikat Jibril kepada Ibunya. Nabi ISA juga memperlihatkan banyak Mukjizat lainnya ketika dia tumbuh dewasa, diantaranya dengan membentuk seekor burung hidup dari tanah liat, menghidupkan orang mati, menyembuhkan kebutaan, \n" +
			"dan mendatangkan makanan yang semula tidak ada.Penyelamatan Nabi ISA dari penyaliban juga merupakan salah satu bentuk Mukjizat yang diberikan oleh Allah SWT.",
			
			"Nabi Muhammad SAW adalah Rasul terakhir dan sekaligus sebagai penutup Rasul-Rasul sebelumnya, Beliaulah yang menyempurnakan ajaran-ajaran Islam. Mukjizat yang diberikan oleh Allah SWT kepadanya sangatlah banyak, salah satunya yang paling besar adalah Al-Qur'an, \n" +
			"yang menjadi pedoman utama kehidupan manusia. Selain itu ada pula peristiwa Isra Mi'raj yang membawanya bertemu dengan Allah SWT."
	};
}
