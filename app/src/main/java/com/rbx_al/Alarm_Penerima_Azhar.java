package com.rbx_al;

import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.Vibrator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DigitalClock;
import android.widget.TextView;

@SuppressWarnings("deprecation")
@SuppressLint("NewApi")
public class Alarm_Penerima_Azhar extends Activity {

	private MediaPlayer mMediaPlayer;
	private PowerManager.WakeLock mWakeLock;
	private TextView judul;
	private DigitalClock clock;
	private Button berhenti;
	Vibrator getar1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		mWakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "My Wake Log");
		mWakeLock.acquire();
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON, 
				WindowManager.LayoutParams.FLAG_FULLSCREEN |
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
				WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		setContentView(R.layout.layout_alarm__penerima__azhar);
		font();nonaktif();
		getar1=(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		long[] pattern={250, 500};
		getar1.vibrate(pattern,0);
		berhenti = (Button) findViewById(R.id.alarm_penerima_btn_hentikan_azhar);
		berhenti.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mMediaPlayer.stop();
				System.exit(0);
				getar1=(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				getar1.cancel();
			}
		});
		playSound(this, getAlarmUri());
	}
	private void playSound(Context context, Uri alert){
		mMediaPlayer = MediaPlayer.create(this, R.raw.adzan);
		mMediaPlayer.start();
		
	}
	public void onBackPressed(){
		mMediaPlayer.stop();
		finishAffinity();
		getar1=(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		getar1.cancel();
	}
	private Uri getAlarmUri(){
		Uri alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		if(alert == null){
			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			if(alert == null){
				alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
			}
		}return alert;
	}
	protected void onStop(){
		super.onStop();
		getar1=(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		getar1.cancel();
		mWakeLock.release();
	}
	public void font(){
		judul=(TextView)findViewById(R.id.alarm_penerima_tx_sholat_azhar);
		berhenti=(Button)findViewById(R.id.alarm_penerima_btn_hentikan_azhar);
		clock=(DigitalClock)findViewById(R.id.clock_azhar);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);berhenti.setTypeface(face);clock.setTypeface(face);
	}
	public void nonaktif(){
		SharedPreferences sp=getSharedPreferences("tampung",MODE_PRIVATE);
		Editor edit=sp.edit();
		edit.putString("pilih_azhar", "1");
		edit.commit();
	}
}
