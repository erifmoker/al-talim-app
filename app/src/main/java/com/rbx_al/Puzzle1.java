package com.rbx_al;

import java.util.ArrayList;
import java.util.Collections;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AbsoluteLayout;
import android.widget.Button;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class Puzzle1 extends Activity {

	private TextView hitungPindah,pernyataan;
	private Button[] komponen;
	private static final Integer[] benar=new Integer[]{3,2,1,6,5,4,0,8,7};
	private ArrayList<Integer> arena=new ArrayList<Integer>();
	private Boolean salah_pindah=false;
	MediaPlayer klik_salah,win;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_puzzle1);
		komponen=findkomponen();
		klik_salah=MediaPlayer.create(getBaseContext(), R.raw.kembali);
		win=MediaPlayer.create(getBaseContext(), R.raw.game_win);
		gambar();
		for(int i=0; i<9; i++){
			this.arena.add(i);
		}
		Collections.shuffle(this.arena);//random arena array
		
		fill_grid();
		
		hitungPindah=(TextView)findViewById(R.id.MoveCounter);
		pernyataan=(TextView)findViewById(R.id.FeedbackText);
		
		for (int i = 1; i < 9; i++) {
			komponen[i].setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					makeMove((Button) v);
				}
			});
		}
		hitungPindah.setText("0");
		pernyataan.setText("pernyataan");
	}

	public Button[] findkomponen(){
		Button[] b=new Button[9];
		
		b[0]=(Button)findViewById(R.id.Button00);
		b[1]=(Button)findViewById(R.id.Button01);
		b[2]=(Button)findViewById(R.id.Button02);
		b[3]=(Button)findViewById(R.id.Button03);
		b[4]=(Button)findViewById(R.id.Button04);
		b[5]=(Button)findViewById(R.id.Button05);
		b[6]=(Button)findViewById(R.id.Button06);
		b[7]=(Button)findViewById(R.id.Button07);
		b[8]=(Button)findViewById(R.id.Button08);
		return b;
	}
	
	public void makeMove(final Button b){
		salah_pindah=true;
		int b_text,b_pos,zuk_pos;
		b_text=Integer.parseInt((String) b.getText());
		b_pos=find_pos(b_text);
		zuk_pos=find_pos(0);
		switch (zuk_pos) {
		case (0):
			if(b_pos==1||b_pos==3)
			salah_pindah=false;
			break;
		case (1):
			if(b_pos==0||b_pos==2||b_pos==4)
			salah_pindah=false;
			break;
		case (2):
			if(b_pos==1||b_pos==5)
			salah_pindah=false;
			break;
		case (3):
			if(b_pos==0||b_pos==4||b_pos==6)
			salah_pindah=false;
			break;
		case (4):
			if(b_pos==1||b_pos==3||b_pos==5||b_pos==7)
			salah_pindah=false;
			break;
		case (5):
			if(b_pos==2||b_pos==4||b_pos==8)
			salah_pindah=false;
			break;
		case (6):
			if(b_pos==3||b_pos==7)
			salah_pindah=false;
			break;
		case (7):
			if(b_pos==4||b_pos==6||b_pos==8)
			salah_pindah=false;
			break;
		case (8):
			if(b_pos==5||b_pos==7)
			salah_pindah=false;
			break;
		}
		
		if(salah_pindah==true){
			pernyataan.setText("Salah Pindah");
			klik_salah.start();
			return;
		}
		pernyataan.setText("Lanjutkan");
		arena.remove(b_pos);
		arena.add(b_pos,0);
		arena.remove(zuk_pos);
		arena.add(zuk_pos,b_text);
		
		fill_grid();
		hitungPindah.setText(Integer.toString(Integer.parseInt((String) hitungPindah.getText())+1));
		if(Integer.parseInt(hitungPindah.getText().toString())>150){
			SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
			Editor edit=sp.edit();
			edit.putString("puzzle", "1");
			edit.commit();
			startActivity(new Intent(Puzzle1.this,Puzzle_kalah.class));
		}
		for (int i = 0; i < 9; i++) {
			if(arena.get(i)!=benar[i]){
				return;
			}
		}
		pernyataan.setText("Kamu Memenagkan Permainan");
		SharedPreferences sp=getSharedPreferences("tampung", MODE_PRIVATE);
		Editor edit=sp.edit();
		if(Integer.parseInt(hitungPindah.getText().toString())>120){
			edit.putString("nilai_puzzle1", "70");
		}else if(Integer.parseInt(hitungPindah.getText().toString())>100){
			edit.putString("nilai_puzzle1", "80");
		}else if(Integer.parseInt(hitungPindah.getText().toString())>80){
			edit.putString("nilai_puzzle1", "90");
		}else{
			edit.putString("nilai_puzzle1", "100");
		}
		edit.putString("puzzle", "1");
		edit.commit();
		win.start();
		startActivity(new Intent(Puzzle1.this,Puzzle1_nilai.class));
	}
	
	public void fill_grid(){
		for (int i = 0; i < 9; i++) {
			int text=arena.get(i);
			
			AbsoluteLayout.LayoutParams absParams=
					(AbsoluteLayout.LayoutParams)komponen[text].getLayoutParams();
			switch (i) {
			case (0):
				absParams.x=50;
				absParams.y=5;
				komponen[text].setLayoutParams(absParams);
				break;
			case (1):
				absParams.x=170;
				absParams.y=5;
				komponen[text].setLayoutParams(absParams);
				break;
			case (2):
				absParams.x=290;
				absParams.y=5;
				komponen[text].setLayoutParams(absParams);
				break;
			case (3):
				absParams.x=50;
				absParams.y=125;
				komponen[text].setLayoutParams(absParams);
				break;
			case (4):
				absParams.x=170;
				absParams.y=125;
				komponen[text].setLayoutParams(absParams);
				break;
			case (5):
				absParams.x=290;
				absParams.y=125;
				komponen[text].setLayoutParams(absParams);
				break;
			case (6):
				absParams.x=50;
				absParams.y=245;
				komponen[text].setLayoutParams(absParams);
				break;
			case (7):
				absParams.x=170;
				absParams.y=245;
				komponen[text].setLayoutParams(absParams);
				break;
			case (8):
				absParams.x=290;
				absParams.y=245;
				komponen[text].setLayoutParams(absParams);
				break;
			}
		}
	}
	
	public void gambar(){
		Button btn1=(Button)findViewById(R.id.Button01);
		Button btn2=(Button)findViewById(R.id.Button02);
		Button btn3=(Button)findViewById(R.id.Button03);
		Button btn4=(Button)findViewById(R.id.Button04);
		Button btn5=(Button)findViewById(R.id.Button05);
		Button btn6=(Button)findViewById(R.id.Button06);
		Button btn7=(Button)findViewById(R.id.Button07);
		Button btn8=(Button)findViewById(R.id.Button08);
		Database_Puzzle ambil=new Database_Puzzle();
		btn1.setBackgroundResource(ambil.gambar[0]);
		btn2.setBackgroundResource(ambil.gambar[1]);
		btn3.setBackgroundResource(ambil.gambar[2]);
		btn4.setBackgroundResource(ambil.gambar[3]);
		btn5.setBackgroundResource(ambil.gambar[4]);
		btn6.setBackgroundResource(ambil.gambar[5]);
		btn7.setBackgroundResource(ambil.gambar[6]);
		btn8.setBackgroundResource(ambil.gambar[7]);
	}
	
	public int find_pos(int element){
		int i=0;
		for ( i = 0; i < 9; i++) {
			if(arena.get(i)==element){
				break;
			}
		}
		return i;	
	}
}
