package com.rbx_al;

import android.os.Bundle;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.Window;
import android.widget.TextView;

public class Nabi_adam extends Activity {

	private TextView judul,isi;
	private String ambil_judul,ambil_isi;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_nabi_adam);
		font();
		judul=(TextView)findViewById(R.id.nabi_detail_judul);
		isi=(TextView)findViewById(R.id.nabi_detail_isi);
		Bundle extras=getIntent().getExtras();
		if(extras!=null){
			ambil_judul=extras.getString("judul");
			ambil_isi=extras.getString("cerita");
		}
		judul.setText(ambil_judul);
		isi.setText(ambil_isi);
	}
	public void font(){
		judul=(TextView)findViewById(R.id.nabi_detail_judul);
		isi=(TextView)findViewById(R.id.nabi_detail_isi);
		Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf"),
				face2=Typeface.createFromAsset(getAssets(), "font/smart_kid.otf");
		judul.setTypeface(face);isi.setTypeface(face2);
	}
}
