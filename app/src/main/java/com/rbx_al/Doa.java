package com.rbx_al;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class Doa extends Activity {

	MediaPlayer mp;
	private ListView ls_doa;
	private Database ambil=new Database();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_doa);
		font();
		mp=MediaPlayer.create(this, R.raw.klik);
		ls_doa = (ListView) findViewById(R.id.listdoa);
	
	    List<HashMap<String, String>> tampung=new ArrayList<HashMap<String, String>>();
		for(int i=0;i<ambil.doa.length;i++){
			HashMap<String, String> hm=new HashMap<String, String>();
			hm.put("gambarnya", Integer.toString(ambil.gambar[i]));
			hm.put("judulnya", ambil.doa[i]);
			tampung.add(hm);
		}
		String[] dari={"gambarnya","judulnya"};
		int[] ke={R.id.gbr,R.id.nama_doa};
		final SimpleAdapter adapter=new SimpleAdapter(getBaseContext(), tampung, R.layout.model_list, dari, ke);
		ls_doa.setAdapter(adapter);
		ls_doa.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int posisi, long arg3) {
				for(int i=0;i<ambil.doa.length;i++){
					if(ambil.doa[posisi].equals(ambil.doa[i])){
						Intent pindah=new Intent(Doa.this, Isi_doa.class);
						mp.start();
						pindah.putExtra("judule", ambil.doa[i]);
						pindah.putExtra("gambare", ambil.gambar_besar[i]);
						pindah.putExtra("arabe", ambil.arab[i]);
						pindah.putExtra("audione", ambil.audio[i]);
						pindah.putExtra("bacane", ambil.baca[i]);
						pindah.putExtra("artine", ambil.arti[i]);
						startActivity(pindah);
						overridePendingTransition(R.animator.animation1, R.animator.animation2);
					}
				}
			}
		});
		
	}
	public void onBackPressed(){
		mp.start();
		super.onBackPressed();
	}

public void Home(View v){
finish();
}
public void font(){
	TextView tx=(TextView)findViewById(R.id.judul_doa);
	Typeface face=Typeface.createFromAsset(getAssets(), "font/buka_puasa.ttf");
	tx.setTypeface(face);
}

}
